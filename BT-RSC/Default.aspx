﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BT_RSC.AdminPanel.Default" StylesheetTheme="NewTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="CMS" TagName="TopHeader" src="~/UserControl/TopHeader.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Rental Security System</title>
</head>
<body>
    <form id="form1" runat="server">
    <center>
        <div class="heaer-Outer">
            <div class="header-outer-inner">
                <div class="LogoInner">
                    <div class="Logo">
                        <h1><span style="color:#007acc">RENTAL SECURITY</span><span style="color:#e70516">   SYSTEM V1.0</h1></span>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="body-Outer">
            <div class="Inner-Body">
                <div class="login-Body-Inner">
                    <div class="Inner-body">
                        <div class="Part1">
                            <img src="images/admin/User.jpg" />
                        </div>
                        <div class="Input-Field">
                            <div class="Heading-outer">
                                <h2>
                                    User Login</h2>
                                <br />
                            </div>
                            <div class="Inner-set">
                                <div class="Outer-sub-heading">
                                    Username:
                                </div>
                                <div class="Textbox-Outer" style="text-align: left;">
                                    <asp:TextBox ID="txtLogin" runat="server" CssClass="textbox" Width="190px" Height="8px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtLogin"
                                        ValidationGroup="aa" CssClass="ValidationError"></asp:RequiredFieldValidator>
                                </div>
                                <div class="Outer-sub-heading">
                                    Password:
                                </div>
                                <div class="Textbox-Outer" style="text-align: left;">
                                    <asp:TextBox ID="txtPasswd" runat="server" CssClass="textbox" Height="22px" size="25" TextMode="password"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="txtPasswd"
                                        ValidationGroup="aa" CssClass="ValidationError"></asp:RequiredFieldValidator>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Forget_Password.aspx">Forget Password?</asp:HyperLink>
                                <div class="Outer-sub-heading">
                                    
                                </div>
                                <div class="Textbox-Outer" style="text-align: left;">
                                    <asp:ImageButton ImageUrl="~/images/admin/btn-login.jpg" ID="ImageButton1" runat="server"
                                        ValidationGroup="aa" OnClick="LoginBtn_Click" />
                                    &nbsp;
                                </div>
                                <div class="Textbox-Outer" style="margin-left: 20px;">
                                    <asp:Label ID="lblError" runat="server" CssClass="ValidationError"></asp:Label>
                                </div>
                            </div>
                            <div class="Text-Div" style="float: left; margin-left: -180px;">
                                Use a valid Username and password to gain access to your account</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="FooterOuterClass">
            Copyright 2014@Bahria Town, All rights reserved.
        </div>
    </center>
    </form>
</body>
</html>
