﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using RSC;

namespace BT_RSC.UserControl
{
    public partial class TabControl : System.Web.UI.UserControl
    {
        Query ObjQ = new Query(HostSettings.CS);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity == null || HttpContext.Current.User.Identity.Name == "")
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                HidLoginID.Value = HttpContext.Current.User.Identity.Name;
                TU001 Objuser = ObjQ.QTU001.Get(HidLoginID.Value);
                if (Objuser == null)
                {
                    return;
                }
                else
                {
                    //MenuGenerator ObjMenu = new MenuGenerator();
                    // lblMenuControl.Text = ObjMenu.HTML(Objuser.UserLevelID);
       //             LoadMenu(Objuser.UserLevelID);
                }
            }
        }
       
    }
}