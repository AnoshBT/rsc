﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RSC;
using System.Web.Security;

namespace BT_RSC.AdminPanel
{
    public partial class Default : System.Web.UI.Page
    {
        Query ObjQ = new Query(HostSettings.CS);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }
        protected void LoginBtn_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                lblError.Text = "";
                TUL002 ObjCheckUser = ObjQ.QTUL002.Get(txtLogin.Text);
                if (ObjCheckUser == null)
                {
                    lblError.Text = "Username not found";
                    return;
                }
                //string password = Security.EncryptText(txtPasswd.Text);
                string password = txtPasswd.Text;
                if (ObjCheckUser.Password != password)
                {
                    lblError.Text = "Incorrect Password ";
                    return;
                }
                //if (ObjCheckUser.isactive != "Y")
                //{
                //    lblError.Text = "Sorry your Account is not active please contact to administrator";
                //    return;
                //}
                //Response.Write("UserName and Password Valid");
                ObjCheckUser.LastLoginTime = DateTime.Now;
                ObjQ.QTUL002.Update(ObjCheckUser, ObjCheckUser.UserID);
                FormAuth(ObjCheckUser);
                TU001 user = ObjQ.QTU001.Get(txtLogin.Text);
                if (user.IsBlock == true.ToString())
                {
                    lblError.Text = "Your Account has been blocked. Please Contact Administrator";
                    return;
                }
                else
                {
                    Response.Redirect("Country.aspx", false);
                }

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
        private void FormAuth(TUL002 objUser)
        {
            try
            {
                // =========== START FORMSAUTHENTICATION =================
                // use namespace of System.Web.Security;
                FormsAuthentication.Initialize();
                // Create a new ticket used for authentication
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1, // Ticket version
                   objUser.UserID, // Username associated with ticket - from database
                    DateTime.Now, // Date/time issued
                    // Date/time to expire
                    DateTime.Now.AddMinutes(Session.Timeout), // Date/time to expire
                    false, // "true" for a persistent user cookie, if "false" then cookie is deleted as user login successfully, so when user close the page then he has to login again to go to that page
                    //UserData,
                    FormsAuthentication.FormsCookiePath);// Path cookie valid for

                // Encrypt the cookie using the machine key for secure transport
                string hash = FormsAuthentication.Encrypt(ticket);
                HttpCookie cookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName, // Name of auth cookie
                    hash); // Hashed ticket

                // Set the cookie's expiration time to the tickets expiration time
                cookie.Expires = ticket.Expiration;
                // Add the cookie to the list for outgoing response
                cookie.Expires = ticket.Expiration;
                Response.Cookies.Add(cookie);

                // ========== END FORMSAUTHENTICATION ====================
                try
                {
                    ObjQ.QTUS003.SignIn(Request, objUser.UserID);

                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    Response.Write(ex.Message);
                }

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Response.Redirect("Message.aspx?Message= " + ex.Message + "!");
            }
        }
    }
}