﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RSC;
using System.Web.Security;
namespace BT_RSC
{
    public partial class Master : System.Web.UI.MasterPage
    {
        Query ObjQ = new Query(HostSettings.CS);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.User.Identity == null || HttpContext.Current.User.Identity.Name == "")
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                HidLoginID.Value = HttpContext.Current.User.Identity.Name;
                LoadUserName(HidLoginID.Value);
            }
        }
        protected void LoadUserName(string UserName)
        {
            try
            {
                TU001 Objuser = ObjQ.QTU001.Get(HidLoginID.Value);
                if (Objuser == null)
                {
                    return;
                }
                else
                {
                    lblWelcome.Text = Objuser.FirstName + " " + Objuser.LastName;
                }
            }
            catch (Exception ex)
            {

                //    //lblError.Text = ex.Message;
            }
        }
    }
}