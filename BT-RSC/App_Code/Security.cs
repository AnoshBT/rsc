﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Cryptography;

/// <summary>
/// Summary description for Security
/// </summary>
public class Security
{
    static public string EncryptText(string strText)
    {
        return Encrypt(strText, "&%#@?,:*");
    }

    static public string Encrypt(string strText, string strEncrKey)
    {

        byte[] byKey = { };

        // byte[] IV = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF};
        byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            byte[] inputByteArray = System.Text.Encoding.UTF8.GetBytes(strText);

            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }

    }

    static public string Decrypt(string strText, string sDecrKey)
    {
        byte[] byKey = { };
        //byte[] IV = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF};
        byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };

        byte[] inputByteArray = new byte[strText.Length + 1];

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(strText);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);


            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;

            return encoding.GetString(ms.ToArray());

        }
        catch (Exception ex)
        {
            return ex.Message;
        }


    }

    static public string DecryptText(string strText)
    {
        return Decrypt(strText, "&%#@?,:*");
    }
    //    string str= EncryptText(DateTime.Now.AddMonths(3).ToShortDateString());
    //    string str = DecryptText("bNRnMPgxPGehjMNzE2+5WA==");
}