/* 
Source code originally extracted from Yahoo!
Few modificaion by npguy@hotmail.com, npguy.homepage.com
Added heavy comments and documentation.
*/   
   
//Constants.
SEP_PADDING = 5
HANDLE_PADDING = 7

var yToolbars = new Array();  // Array of all toolbars.

// Initialize everything when the document is ready
var YInitialized = false;
function document.onreadystatechange() {
//  if (YInitialized) return;
  YInitialized = true;

  var i, s, curr;

  // Find all the toolbars and initialize them.
  // Now starting to explore the entire body of deditor.htm
  for (i=0; i<document.body.all.length; i++) {
    //Get the current object and assign to curr
    curr=document.body.all[i];    
    //Now we will look in every div tag class value which is the child node of <div class="yToolBar"> 
    if (curr.className == "yToolbar") {
      //See InitTB for more! if return false then the current classname is invalid or not contain
      //in our CSS.
      if (! InitTB(curr)) {
        alert("Toolbar: " + curr.id + " failed to initialize. Status: false");
      }
      //Get the next child node of <div class="YToolBar"> parent nodes
      yToolbars[yToolbars.length] = curr;
      
    }
  }

  //Lay out the page, set handler.
  DoLayout();
  window.onresize = DoLayout;

  mytext.document.open()
  //Intial HTML 
  mytext.document.write("<BODY MONOSPACE STYLE=\"font:10pt arial,sans-serif\"></body>");
  mytext.document.close()
  mytext.document.designMode="On"
  //setTimeout("mytext.focus()",0)

}

// Initialize a toolbar button
// Its preety easy to understand.
function InitBtn(btn) {
  btn.onmouseover = BtnMouseOver;
  btn.onmouseout = BtnMouseOut;
  btn.onmousedown = BtnMouseDown;
  btn.onmouseup = BtnMouseUp;
  btn.ondragstart = YCancelEvent;
  btn.onselectstart = YCancelEvent;
  btn.onselect = YCancelEvent;
  btn.YUSERONCLICK = btn.onclick;
  btn.onclick = YCancelEvent;
  btn.YINITIALIZED = true;
  return true;
}

//Initialize a toolbar. 
function InitTB(y) {
  // Set initial size of toolbar to that of the handle
  y.TBWidth = 0;
    
  // Populate the toolbar with its contents
  if (! PopulateTB(y)) return false;
  
  // Set the toolbar width and put in the handle
  y.style.posWidth = y.TBWidth;
  
  return true;
}


// Hander that simply cancels an event
function YCancelEvent() {
  event.returnValue=false;
  event.cancelBubble=true;
  return false;
}

// Toolbar button onmouseover handler
function BtnMouseOver() {
  if (event.srcElement.tagName != "IMG") return false;
  var image = event.srcElement;
  var element = image.parentElement;
  
  // Change button look based on current state of image.
  if (image.className == "Ico") element.className = "BtnMouseOverUp";
  else if (image.className == "IcoDown") element.className = "BtnMouseOverDown";

  event.cancelBubble = true;
}

// Toolbar button onmouseout handler
function BtnMouseOut() {
  if (event.srcElement.tagName != "IMG") {
    event.cancelBubble = true;
    return false;
  }

  var image = event.srcElement;
  var element = image.parentElement;
  yRaisedElement = null;
  
  element.className = "Btn";
  image.className = "Ico";

  event.cancelBubble = true;
}

// Toolbar button onmousedown handler
function BtnMouseDown() {
  if (event.srcElement.tagName != "IMG") {
    event.cancelBubble = true;
    event.returnValue=false;
    return false;
  }

  var image = event.srcElement;
  var element = image.parentElement;

  element.className = "BtnMouseOverDown";
  image.className = "IcoDown";

  event.cancelBubble = true;
  event.returnValue=false;
  return false;
}

// Toolbar button onmouseup handler
function BtnMouseUp() {
  if (event.srcElement.tagName != "IMG") {
    event.cancelBubble = true;
    return false;
  }

  var image = event.srcElement;
  var element = image.parentElement;

  if (element.YUSERONCLICK) eval(element.YUSERONCLICK + "anonymous()");

  element.className = "BtnMouseOverUp";
  image.className = "Ico";

  event.cancelBubble = true;
  return false;
}

// Populate a toolbar with the elements within it
function PopulateTB(y) {
  var i, elements, element;

  // Iterate through all the top-level elements in the toolbar
  elements = y.children;
  for (i=0; i<elements.length; i++) {
    element = elements[i];
    if (element.tagName == "SCRIPT" || element.tagName == "!") continue;
    
    switch (element.className) {
    case "Btn":
      if (element.YINITIALIZED == null) {
	if (! InitBtn(element)) {
	  alert("Problem initializing:" + element.id);
	  return false;
	}
      }
      
      element.style.posLeft = y.TBWidth;
      y.TBWidth += element.offsetWidth + 1;
      break;
      
    case "TBGen":
      element.style.posLeft = y.TBWidth;
      y.TBWidth += element.offsetWidth + 1;
      break;
      
    case "TBSep":
      element.style.posLeft = y.TBWidth + 2;
      y.TBWidth += SEP_PADDING;
      break;
      
    case "TBHandle":
      element.style.posLeft = 2;
      y.TBWidth += element.offsetWidth + HANDLE_PADDING;
      break;
      
    default:
      alert("Invalid class: " + element.className + " on Element: " + element.id + " <" + element.tagName + ">");
      return false;
    }
  }

  y.TBWidth += 1;
  return true;
}

function DebugObject(obj) {
  var msg = "";
  for (var i in TB) {
    ans=prompt(i+"="+TB[i]+"\n");
    if (! ans) break;
  }
}

// Lay out the docked toolbars
function LayoutTBs() {
  NumTBs = yToolbars.length;

  // If no toolbars we're outta here
  if (NumTBs == 0) return;

  //Get the total size of a TBline.
  var i;
  var ScrWid = (document.body.offsetWidth) - 6;
  var TotalLen = ScrWid;
  for (i = 0 ; i < NumTBs ; i++) {
    TB = yToolbars[i];
    if (TB.TBWidth > TotalLen) TotalLen = TB.TBWidth;
  }

  var PrevTB;
  var LastStart = 0;
  var RelTop = 0;
  var LastWid, CurrWid;

  //Set up the first toolbar.
  var TB = yToolbars[0];
  TB.style.posTop = 0;
  TB.style.posLeft = 0;

  //Lay out the other toolbars.
  var Start = TB.TBWidth;
  for (i = 1 ; i < yToolbars.length ; i++) {
    PrevTB = TB;
    TB = yToolbars[i];
    CurrWid = TB.TBWidth;

    if ((Start + CurrWid) > ScrWid) { 
      //TB needs to go on next line.
      Start = 0;
      LastWid = TotalLen - LastStart;
    } 
    else { 
      //Ok on this line.
      LastWid = PrevTB.TBWidth;
      //RelTop -= TB.style.posHeight;
      RelTop -= TB.offsetHeight;
    }
      
    //Set TB position and LastTB width.
    TB.style.posTop = RelTop;
    TB.style.posLeft = Start;
    PrevTB.style.width = LastWid;

    //Increment counters.
    LastStart = Start;
    Start += CurrWid;
  } 

  //Set width of last toolbar.
  TB.style.width = TotalLen - LastStart;
  
  //Move everything after the toolbars up the appropriate amount.
  i--;
  TB = yToolbars[i];
  var TBInd = TB.sourceIndex;
  var A = TB.document.all;
  var item;
  for (i in A) {
    item = A.item(i);
    if (! item) continue;
    if (! item.style) continue;
    if (item.sourceIndex <= TBInd) continue;
    if (item.style.position == "absolute") continue;
    item.style.posTop = RelTop;
  }
}

//Lays out the page.
function DoLayout() {
  LayoutTBs();
}

// Check if toolbar is being used when in text mode
function isRTextMode() {
  if (! bTextMode) return true;
  alert("Please uncheck the \"View HTML source\" checkbox.");
  mytext.focus();
  return false;
}

//Formats text in mytext.
function RunCom(what,opt) {
  if (!isRTextMode()) return;
  
  if (opt=="removeFormat") {
    what=opt;
    opt=null;
  }

  if (opt==null) mytext.document.execCommand(what);
  else mytext.document.execCommand(what,"",opt);
  
  pureText = false;
  mytext.focus();
}

//Switches between text and html mode.
function setMode(newMode) {
  bTextMode = newMode;
  var cont;
  if (bTextMode) {
    cleanHtml();
    cleanHtml();

    cont=mytext.document.body.innerHTML;
    mytext.document.body.innerText=cont;
  } else {
    cont=mytext.document.body.innerText;
    mytext.document.body.innerHTML=cont;
  }
  
  mytext.focus();
}

//Finds and returns an element.
function getEl(sTag,start) {
 //Copy the selected character to "start" string while start!=NULL && tagName doesn't have 'A' 
  while ((start!=null) && (start.tagName!=sTag)) start = start.parentElement;
  return start;
}

function createLink() {
  //Is View Source is Checked!
  if (!isRTextMode()) return;
  
  
  var isA = getEl("A",mytext.document.selection.createRange().parentElement());
  
  var str=prompt("Enter URL :", isA ? isA.href : "http:\/\/");
  
  //if str selection type is None!(If the user didn't block the string) then
  // get the string and add the <A HREF and paste it.  
  if ((str!=null) && (str!="http://")) {
    if (mytext.document.selection.type=="None") {    	
      var sel=mytext.document.selection.createRange();
      sel.pasteHTML("<A HREF=\""+str+"\">"+str+"</A> ");
      sel.select();
    }
    else 
    //If user had selected/blocked the string  just pass this command.
    RunCom("CreateLink",str);
  }
  else 
  //If nothing entered just Focust in our IFRAME.
  mytext.focus();
}

//Sets the text color.
function foreColor() {
  if (! isRTextMode()) return;
  //var arr = showModalDialog("page.htm", "", "font-family:Verdana; font-size:12; dialogWidth:30em; dialogHeight:35em");
  alert("in TODO List");  
}

//Sets the background color.
function backColor() {
  if (!isRTextMode()) return;
  alert("in TODO List");  
}

function cleanHtml() {
  var fonts = mytext.document.body.all.tags("FONT");
  var curr;
  for (var i = fonts.length - 1; i >= 0; i--) {
    curr = fonts[i];
    if (curr.style.backgroundColor == "#ffffff") curr.outerHTML = curr.innerHTML;
  }
}

function getPureHtml() {
  var str = "";
  var paras = mytext.document.body.all.tags("P");
  if (paras.length > 0) {
    for (var i=paras.length-1; i >= 0; i--) str = paras[i].innerHTML + "\n" + str;
  } else {
    str = mytext.document.body.innerHTML;
  }
  return str;
}

// Local Variables:
// c-basic-offset: 2
// End:
