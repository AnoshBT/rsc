using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QProperty_Servants:Query
	{

		public const string TABLE_NAME = "Property_Servants";
		public const string P_Id = "Id";
		public const string P_Id_Person = "Id_Person";
		public const string P_Staying = "Staying";
		public const string P_Id_Property = "Id_Property";
		public const string P_Id_House = "Id_House";
		public const string P_Id_HousePortions = "Id_HousePortions";
		public const string P_Id_Street = "Id_Street";
		public const string P_Id_Block = "Id_Block";
		public const string P_Id_Phase = "Id_Phase";
		public const string SelectColumns=@"Id,Id_Person,Staying,Id_Property,Id_House,Id_HousePortions,Id_Street,Id_Block,Id_Phase";
		public const string QSelect=@"Select * From Property_Servants  order by  Id";
		public const string QSelectById=@"Select * From Property_Servants 
								 where Id = @Id  order by  Id";
		public const string QInsert=@"Insert Into  Property_Servants (Id_Person,Staying,Id_Property,Id_House,Id_HousePortions,Id_Street,Id_Block,Id_Phase) 
								 values(@Id_Person,@Staying,@Id_Property,@Id_House,@Id_HousePortions,@Id_Street,@Id_Block,@Id_Phase)";
		public const string QUpdateById=@"Update Property_Servants Set Id_Person=@Id_Person,Staying=@Staying,Id_Property=@Id_Property,Id_House=@Id_House,
						Id_HousePortions=@Id_HousePortions,Id_Street=@Id_Street,Id_Block=@Id_Block,Id_Phase=@Id_Phase 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Property_Servants 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Property_Servants 
								 where Id = @Id";

		public QProperty_Servants(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QProperty_Servants(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QProperty_Servants(IDbConnection Connection):base(Connection)
		{
		}
		public QProperty_Servants(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Property_Servants Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Property_Servants(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Property_Servants obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Property_Servants obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Property_Servants obj) 
		{
			try
			{
				AddParameter(P_Id_Person,obj.Id_Person,true,false);
				AddParameter(P_Staying,obj.Staying,true,true);
				AddParameter(P_Id_Property,obj.Id_Property,true,true);
				AddParameter(P_Id_House,obj.Id_House,true,true);
				AddParameter(P_Id_HousePortions,obj.Id_HousePortions,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Id_Person,obj[P_Id_Person],true,false);
				AddParameter(P_Staying,obj[P_Staying],true,true);
				AddParameter(P_Id_Property,obj[P_Id_Property],true,true);
				AddParameter(P_Id_House,obj[P_Id_House],true,true);
				AddParameter(P_Id_HousePortions,obj[P_Id_HousePortions],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Property_Servants obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Person,obj.Id_Person,true,false);
				AddParameter(P_Staying,obj.Staying,true,true);
				AddParameter(P_Id_Property,obj.Id_Property,true,true);
				AddParameter(P_Id_House,obj.Id_House,true,true);
				AddParameter(P_Id_HousePortions,obj.Id_HousePortions,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Person,obj[P_Id_Person],true,false);
				AddParameter(P_Staying,obj[P_Staying],true,true);
				AddParameter(P_Id_Property,obj[P_Id_Property],true,true);
				AddParameter(P_Id_House,obj[P_Id_House],true,true);
				AddParameter(P_Id_HousePortions,obj[P_Id_HousePortions],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Block=@"Select * From Property_Servants 
								 where Id_Block = @Id_Block  order by  Id";
		public const string QDeleteById_Block=@"Delete From Property_Servants 
								 where Id_Block = @Id_Block";
		virtual	public DataTable GetById_Block(string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Block( string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_HousePortions=@"Select * From Property_Servants 
								 where Id_HousePortions = @Id_HousePortions  order by  Id";
		public const string QDeleteById_HousePortions=@"Delete From Property_Servants 
								 where Id_HousePortions = @Id_HousePortions";
		virtual	public DataTable GetById_HousePortions(string Id_HousePortions ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_HousePortions;
				AddParameter(P_Id_HousePortions,Id_HousePortions,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_HousePortions( string Id_HousePortions ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_HousePortions;
				AddParameter(P_Id_HousePortions,Id_HousePortions,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_House=@"Select * From Property_Servants 
								 where Id_House = @Id_House  order by  Id";
		public const string QDeleteById_House=@"Delete From Property_Servants 
								 where Id_House = @Id_House";
		virtual	public DataTable GetById_House(string Id_House ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_House;
				AddParameter(P_Id_House,Id_House,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_House( string Id_House ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_House;
				AddParameter(P_Id_House,Id_House,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Person=@"Select * From Property_Servants 
								 where Id_Person = @Id_Person  order by  Id";
		public const string QDeleteById_Person=@"Delete From Property_Servants 
								 where Id_Person = @Id_Person";
		virtual	public DataTable GetById_Person(string Id_Person ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Person;
				AddParameter(P_Id_Person,Id_Person,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Person( string Id_Person ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Person;
				AddParameter(P_Id_Person,Id_Person,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		
		public const string QSelectById_Phase=@"Select * From Property_Servants 
								 where Id_Phase = @Id_Phase  order by  Id";
		public const string QDeleteById_Phase=@"Delete From Property_Servants 
								 where Id_Phase = @Id_Phase";
		virtual	public DataTable GetById_Phase(string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Phase( string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Street=@"Select * From Property_Servants 
								 where Id_Street = @Id_Street  order by  Id";
		public const string QDeleteById_Street=@"Delete From Property_Servants 
								 where Id_Street = @Id_Street";
		virtual	public DataTable GetById_Street(string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Street( string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Property_Servants
	{
		public Property_Servants()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Id_Person=string.Empty;
		public string Id_Person
		{
			get
			{
				 return _Id_Person;
			}
			set
			{
				  _Id_Person = value;
			}
		}
		protected  string _Staying=string.Empty;
		public string Staying
		{
			get
			{
				 return _Staying;
			}
			set
			{
				  _Staying = value;
			}
		}
		protected  string _Id_Property=string.Empty;
		public string Id_Property
		{
			get
			{
				 return _Id_Property;
			}
			set
			{
				  _Id_Property = value;
			}
		}
		protected  string _Id_House=string.Empty;
		public string Id_House
		{
			get
			{
				 return _Id_House;
			}
			set
			{
				  _Id_House = value;
			}
		}
		protected  string _Id_HousePortions=string.Empty;
		public string Id_HousePortions
		{
			get
			{
				 return _Id_HousePortions;
			}
			set
			{
				  _Id_HousePortions = value;
			}
		}
		protected  string _Id_Street=string.Empty;
		public string Id_Street
		{
			get
			{
				 return _Id_Street;
			}
			set
			{
				  _Id_Street = value;
			}
		}
		protected  string _Id_Block=string.Empty;
		public string Id_Block
		{
			get
			{
				 return _Id_Block;
			}
			set
			{
				  _Id_Block = value;
			}
		}
		protected  string _Id_Phase=string.Empty;
		public string Id_Phase
		{
			get
			{
				 return _Id_Phase;
			}
			set
			{
				  _Id_Phase = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Property_Servants(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id))
					Id = obj[QProperty_Servants.P_Id].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_Person))
					Id_Person = obj[QProperty_Servants.P_Id_Person].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Staying))
					Staying = obj[QProperty_Servants.P_Staying].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_Property))
					Id_Property = obj[QProperty_Servants.P_Id_Property].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_House))
					Id_House = obj[QProperty_Servants.P_Id_House].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_HousePortions))
					Id_HousePortions = obj[QProperty_Servants.P_Id_HousePortions].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_Street))
					Id_Street = obj[QProperty_Servants.P_Id_Street].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_Block))
					Id_Block = obj[QProperty_Servants.P_Id_Block].ToString();
				if(obj.Table.Columns.Contains(QProperty_Servants.P_Id_Phase))
					Id_Phase = obj[QProperty_Servants.P_Id_Phase].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}