using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QNOC:Query
	{

		public const string TABLE_NAME = "NOC";
		public const string P_Id = "Id";
		public const string P_Id_Residential_property = "Id_Residential_property";
		public const string P_Id_Commercial_property = "Id_Commercial_property";
		public const string P_Case_Number = "Case_Number";
		public const string P_Move_In_BTL = "Move_In_BTL";
		public const string P_Move_Out_BTL = "Move_Out_BTL";
		public const string P_Reason_for_Shifting = "Reason_for_Shifting";
		public const string P_Rent_Agreement = "Rent_Agreement";
		public const string P_Police_Verification_Rpt_Tenant = "Police_Verification_Rpt_Tenant";
		public const string P_Last_Paid_Utility_Bills_Owner = "Last_Paid_Utility_Bills_Owner";
		public const string P_NOC_Owner_Tenant = "NOC_Owner_Tenant";
		public const string P_Property_Dealer_VCard = "Property_Dealer_VCard";
		public const string P_Authority_Letter_Owner = "Authority_Letter_Owner";
		public const string P_Id_Owner = "Id_Owner";
		public const string P_Id_Tenant = "Id_Tenant";
		public const string P_Id_Power_of_Attorney = "Id_Power_of_Attorney";
		public const string P_Id_Guarantor_1 = "Id_Guarantor_1";
		public const string P_Id_Guarantor_2 = "Id_Guarantor_2";
		public const string P_Doc_Date = "Doc_Date";
		public const string P_Id_Doc_Preparey_By = "Id_Doc_Preparey_By";
		public const string P_Id_Doc_Approved_By = "Id_Doc_Approved_By";
		public const string P_Sugestions = "Sugestions";
		public const string P_Remarks = "Remarks";
		public const string P_ID_Card = "ID_Card";
		public const string P_Ownership_Proof = "Ownership_Proof";
		public const string P_Id_Plaza_Number = "Id_Plaza_Number";
		public const string P_Id_House_Number = "Id_House_Number";
		public const string P_Id_House_Portion = "Id_House_Portion";
		public const string P_Id_Shop_Number = "Id_Shop_Number";
		public const string P_Id_Street = "Id_Street";
		public const string P_Id_Block = "Id_Block";
		public const string P_Id_Phase = "Id_Phase";
		public const string P_IP = "IP";
		public const string SelectColumns=@"Id,Id_Residential_property,Id_Commercial_property,Case_Number,Move_In_BTL,Move_Out_BTL,Reason_for_Shifting,Rent_Agreement,Police_Verification_Rpt_Tenant,
						Last_Paid_Utility_Bills_Owner,NOC_Owner_Tenant,Property_Dealer_VCard,Authority_Letter_Owner,Id_Owner,Id_Tenant,Id_Power_of_Attorney,Id_Guarantor_1,
						Id_Guarantor_2,Doc_Date,Id_Doc_Preparey_By,Id_Doc_Approved_By,Sugestions,Remarks,ID_Card,Ownership_Proof,
						Id_Plaza_Number,Id_House_Number,Id_House_Portion,Id_Shop_Number,Id_Street,Id_Block,Id_Phase,IP";
		public const string QSelect=@"Select * From NOC  order by  Case_Number";
		public const string QSelectById=@"Select * From NOC 
								 where Id = @Id  order by  Case_Number";
		public const string QInsert=@"Insert Into  NOC (Id_Residential_property,Id_Commercial_property,Case_Number,Move_In_BTL,Move_Out_BTL,Reason_for_Shifting,Rent_Agreement,Police_Verification_Rpt_Tenant,
						Last_Paid_Utility_Bills_Owner,NOC_Owner_Tenant,Property_Dealer_VCard,Authority_Letter_Owner,Id_Owner,Id_Tenant,Id_Power_of_Attorney,Id_Guarantor_1,
						Id_Guarantor_2,Doc_Date,Id_Doc_Preparey_By,Id_Doc_Approved_By,Sugestions,Remarks,ID_Card,Ownership_Proof,
						Id_Plaza_Number,Id_House_Number,Id_House_Portion,Id_Shop_Number,Id_Street,Id_Block,Id_Phase,IP) 
								 values(@Id_Residential_property,@Id_Commercial_property,@Case_Number,@Move_In_BTL,@Move_Out_BTL,@Reason_for_Shifting,@Rent_Agreement,@Police_Verification_Rpt_Tenant,
						@Last_Paid_Utility_Bills_Owner,@NOC_Owner_Tenant,@Property_Dealer_VCard,@Authority_Letter_Owner,@Id_Owner,@Id_Tenant,@Id_Power_of_Attorney,@Id_Guarantor_1,
						@Id_Guarantor_2,@Doc_Date,@Id_Doc_Preparey_By,@Id_Doc_Approved_By,@Sugestions,@Remarks,@ID_Card,@Ownership_Proof,
						@Id_Plaza_Number,@Id_House_Number,@Id_House_Portion,@Id_Shop_Number,@Id_Street,@Id_Block,@Id_Phase,@IP)";
		public const string QUpdateById=@"Update NOC Set Id_Residential_property=@Id_Residential_property,Id_Commercial_property=@Id_Commercial_property,Case_Number=@Case_Number,Move_In_BTL=@Move_In_BTL,
						Move_Out_BTL=@Move_Out_BTL,Reason_for_Shifting=@Reason_for_Shifting,Rent_Agreement=@Rent_Agreement,Police_Verification_Rpt_Tenant=@Police_Verification_Rpt_Tenant,
						Last_Paid_Utility_Bills_Owner=@Last_Paid_Utility_Bills_Owner,NOC_Owner_Tenant=@NOC_Owner_Tenant,Property_Dealer_VCard=@Property_Dealer_VCard,Authority_Letter_Owner=@Authority_Letter_Owner,
						Id_Owner=@Id_Owner,Id_Tenant=@Id_Tenant,Id_Power_of_Attorney=@Id_Power_of_Attorney,Id_Guarantor_1=@Id_Guarantor_1,
						Id_Guarantor_2=@Id_Guarantor_2,Doc_Date=@Doc_Date,Id_Doc_Preparey_By=@Id_Doc_Preparey_By,Id_Doc_Approved_By=@Id_Doc_Approved_By,
						Sugestions=@Sugestions,Remarks=@Remarks,ID_Card=@ID_Card,Ownership_Proof=@Ownership_Proof,
						Id_Plaza_Number=@Id_Plaza_Number,Id_House_Number=@Id_House_Number,Id_House_Portion=@Id_House_Portion,Id_Shop_Number=@Id_Shop_Number,
						Id_Street=@Id_Street,Id_Block=@Id_Block,Id_Phase=@Id_Phase,IP=@IP 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From NOC 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From NOC 
								 where Id = @Id";

		public QNOC(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QNOC(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QNOC(IDbConnection Connection):base(Connection)
		{
		}
		public QNOC(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public NOC Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new NOC(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(NOC obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(NOC obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(NOC obj) 
		{
			try
			{
				AddParameter(P_Id_Residential_property,obj.Id_Residential_property,true,true);
				AddParameter(P_Id_Commercial_property,obj.Id_Commercial_property,true,true);
				AddParameter(P_Case_Number,obj.Case_Number,true,true);
				AddParameter(P_Move_In_BTL,obj.Move_In_BTL,true,false);
				AddParameter(P_Move_Out_BTL,obj.Move_Out_BTL,true,false);
				AddParameter(P_Reason_for_Shifting,obj.Reason_for_Shifting,true,true);
				AddParameter(P_Rent_Agreement,obj.Rent_Agreement,true,true);
				AddParameter(P_Police_Verification_Rpt_Tenant,obj.Police_Verification_Rpt_Tenant,true,true);
				AddParameter(P_Last_Paid_Utility_Bills_Owner,obj.Last_Paid_Utility_Bills_Owner,true,true);
				AddParameter(P_NOC_Owner_Tenant,obj.NOC_Owner_Tenant,true,true);
				AddParameter(P_Property_Dealer_VCard,obj.Property_Dealer_VCard,true,true);
				AddParameter(P_Authority_Letter_Owner,obj.Authority_Letter_Owner,true,true);
				AddParameter(P_Id_Owner,obj.Id_Owner,true,false);
				AddParameter(P_Id_Tenant,obj.Id_Tenant,true,true);
				AddParameter(P_Id_Power_of_Attorney,obj.Id_Power_of_Attorney,true,true);
				AddParameter(P_Id_Guarantor_1,obj.Id_Guarantor_1,true,true);
				AddParameter(P_Id_Guarantor_2,obj.Id_Guarantor_2,true,true);
				AddParameter(P_Doc_Date,obj.Doc_Date,true,false);
				AddParameter(P_Id_Doc_Preparey_By,obj.Id_Doc_Preparey_By,true,false);
				AddParameter(P_Id_Doc_Approved_By,obj.Id_Doc_Approved_By,true,true);
				AddParameter(P_Sugestions,obj.Sugestions,true,true);
				AddParameter(P_Remarks,obj.Remarks,true,true);
				AddParameter(P_ID_Card,obj.ID_Card,true,true);
				AddParameter(P_Ownership_Proof,obj.Ownership_Proof,true,true);
				AddParameter(P_Id_Plaza_Number,obj.Id_Plaza_Number,true,true);
				AddParameter(P_Id_House_Number,obj.Id_House_Number,true,true);
				AddParameter(P_Id_House_Portion,obj.Id_House_Portion,true,true);
				AddParameter(P_Id_Shop_Number,obj.Id_Shop_Number,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_IP,obj.IP,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Id_Residential_property,obj[P_Id_Residential_property],true,true);
				AddParameter(P_Id_Commercial_property,obj[P_Id_Commercial_property],true,true);
				AddParameter(P_Case_Number,obj[P_Case_Number],true,true);
				AddParameter(P_Move_In_BTL,obj[P_Move_In_BTL],true,false);
				AddParameter(P_Move_Out_BTL,obj[P_Move_Out_BTL],true,false);
				AddParameter(P_Reason_for_Shifting,obj[P_Reason_for_Shifting],true,true);
				AddParameter(P_Rent_Agreement,obj[P_Rent_Agreement],true,true);
				AddParameter(P_Police_Verification_Rpt_Tenant,obj[P_Police_Verification_Rpt_Tenant],true,true);
				AddParameter(P_Last_Paid_Utility_Bills_Owner,obj[P_Last_Paid_Utility_Bills_Owner],true,true);
				AddParameter(P_NOC_Owner_Tenant,obj[P_NOC_Owner_Tenant],true,true);
				AddParameter(P_Property_Dealer_VCard,obj[P_Property_Dealer_VCard],true,true);
				AddParameter(P_Authority_Letter_Owner,obj[P_Authority_Letter_Owner],true,true);
				AddParameter(P_Id_Owner,obj[P_Id_Owner],true,false);
				AddParameter(P_Id_Tenant,obj[P_Id_Tenant],true,true);
				AddParameter(P_Id_Power_of_Attorney,obj[P_Id_Power_of_Attorney],true,true);
				AddParameter(P_Id_Guarantor_1,obj[P_Id_Guarantor_1],true,true);
				AddParameter(P_Id_Guarantor_2,obj[P_Id_Guarantor_2],true,true);
				AddParameter(P_Doc_Date,obj[P_Doc_Date],true,false);
				AddParameter(P_Id_Doc_Preparey_By,obj[P_Id_Doc_Preparey_By],true,false);
				AddParameter(P_Id_Doc_Approved_By,obj[P_Id_Doc_Approved_By],true,true);
				AddParameter(P_Sugestions,obj[P_Sugestions],true,true);
				AddParameter(P_Remarks,obj[P_Remarks],true,true);
				AddParameter(P_ID_Card,obj[P_ID_Card],true,true);
				AddParameter(P_Ownership_Proof,obj[P_Ownership_Proof],true,true);
				AddParameter(P_Id_Plaza_Number,obj[P_Id_Plaza_Number],true,true);
				AddParameter(P_Id_House_Number,obj[P_Id_House_Number],true,true);
				AddParameter(P_Id_House_Portion,obj[P_Id_House_Portion],true,true);
				AddParameter(P_Id_Shop_Number,obj[P_Id_Shop_Number],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_IP,obj[P_IP],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(NOC obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Residential_property,obj.Id_Residential_property,true,true);
				AddParameter(P_Id_Commercial_property,obj.Id_Commercial_property,true,true);
				AddParameter(P_Case_Number,obj.Case_Number,true,true);
				AddParameter(P_Move_In_BTL,obj.Move_In_BTL,true,false);
				AddParameter(P_Move_Out_BTL,obj.Move_Out_BTL,true,false);
				AddParameter(P_Reason_for_Shifting,obj.Reason_for_Shifting,true,true);
				AddParameter(P_Rent_Agreement,obj.Rent_Agreement,true,true);
				AddParameter(P_Police_Verification_Rpt_Tenant,obj.Police_Verification_Rpt_Tenant,true,true);
				AddParameter(P_Last_Paid_Utility_Bills_Owner,obj.Last_Paid_Utility_Bills_Owner,true,true);
				AddParameter(P_NOC_Owner_Tenant,obj.NOC_Owner_Tenant,true,true);
				AddParameter(P_Property_Dealer_VCard,obj.Property_Dealer_VCard,true,true);
				AddParameter(P_Authority_Letter_Owner,obj.Authority_Letter_Owner,true,true);
				AddParameter(P_Id_Owner,obj.Id_Owner,true,false);
				AddParameter(P_Id_Tenant,obj.Id_Tenant,true,true);
				AddParameter(P_Id_Power_of_Attorney,obj.Id_Power_of_Attorney,true,true);
				AddParameter(P_Id_Guarantor_1,obj.Id_Guarantor_1,true,true);
				AddParameter(P_Id_Guarantor_2,obj.Id_Guarantor_2,true,true);
				AddParameter(P_Doc_Date,obj.Doc_Date,true,false);
				AddParameter(P_Id_Doc_Preparey_By,obj.Id_Doc_Preparey_By,true,false);
				AddParameter(P_Id_Doc_Approved_By,obj.Id_Doc_Approved_By,true,true);
				AddParameter(P_Sugestions,obj.Sugestions,true,true);
				AddParameter(P_Remarks,obj.Remarks,true,true);
				AddParameter(P_ID_Card,obj.ID_Card,true,true);
				AddParameter(P_Ownership_Proof,obj.Ownership_Proof,true,true);
				AddParameter(P_Id_Plaza_Number,obj.Id_Plaza_Number,true,true);
				AddParameter(P_Id_House_Number,obj.Id_House_Number,true,true);
				AddParameter(P_Id_House_Portion,obj.Id_House_Portion,true,true);
				AddParameter(P_Id_Shop_Number,obj.Id_Shop_Number,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_IP,obj.IP,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Residential_property,obj[P_Id_Residential_property],true,true);
				AddParameter(P_Id_Commercial_property,obj[P_Id_Commercial_property],true,true);
				AddParameter(P_Case_Number,obj[P_Case_Number],true,true);
				AddParameter(P_Move_In_BTL,obj[P_Move_In_BTL],true,false);
				AddParameter(P_Move_Out_BTL,obj[P_Move_Out_BTL],true,false);
				AddParameter(P_Reason_for_Shifting,obj[P_Reason_for_Shifting],true,true);
				AddParameter(P_Rent_Agreement,obj[P_Rent_Agreement],true,true);
				AddParameter(P_Police_Verification_Rpt_Tenant,obj[P_Police_Verification_Rpt_Tenant],true,true);
				AddParameter(P_Last_Paid_Utility_Bills_Owner,obj[P_Last_Paid_Utility_Bills_Owner],true,true);
				AddParameter(P_NOC_Owner_Tenant,obj[P_NOC_Owner_Tenant],true,true);
				AddParameter(P_Property_Dealer_VCard,obj[P_Property_Dealer_VCard],true,true);
				AddParameter(P_Authority_Letter_Owner,obj[P_Authority_Letter_Owner],true,true);
				AddParameter(P_Id_Owner,obj[P_Id_Owner],true,false);
				AddParameter(P_Id_Tenant,obj[P_Id_Tenant],true,true);
				AddParameter(P_Id_Power_of_Attorney,obj[P_Id_Power_of_Attorney],true,true);
				AddParameter(P_Id_Guarantor_1,obj[P_Id_Guarantor_1],true,true);
				AddParameter(P_Id_Guarantor_2,obj[P_Id_Guarantor_2],true,true);
				AddParameter(P_Doc_Date,obj[P_Doc_Date],true,false);
				AddParameter(P_Id_Doc_Preparey_By,obj[P_Id_Doc_Preparey_By],true,false);
				AddParameter(P_Id_Doc_Approved_By,obj[P_Id_Doc_Approved_By],true,true);
				AddParameter(P_Sugestions,obj[P_Sugestions],true,true);
				AddParameter(P_Remarks,obj[P_Remarks],true,true);
				AddParameter(P_ID_Card,obj[P_ID_Card],true,true);
				AddParameter(P_Ownership_Proof,obj[P_Ownership_Proof],true,true);
				AddParameter(P_Id_Plaza_Number,obj[P_Id_Plaza_Number],true,true);
				AddParameter(P_Id_House_Number,obj[P_Id_House_Number],true,true);
				AddParameter(P_Id_House_Portion,obj[P_Id_House_Portion],true,true);
				AddParameter(P_Id_Shop_Number,obj[P_Id_Shop_Number],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_IP,obj[P_IP],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Block=@"Select * From NOC 
								 where Id_Block = @Id_Block  order by  Case_Number";
		public const string QDeleteById_Block=@"Delete From NOC 
								 where Id_Block = @Id_Block";
		virtual	public DataTable GetById_Block(string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Block( string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_House_Portion=@"Select * From NOC 
								 where Id_House_Portion = @Id_House_Portion  order by  Case_Number";
		public const string QDeleteById_House_Portion=@"Delete From NOC 
								 where Id_House_Portion = @Id_House_Portion";
		virtual	public DataTable GetById_House_Portion(string Id_House_Portion ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_House_Portion;
				AddParameter(P_Id_House_Portion,Id_House_Portion,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_House_Portion( string Id_House_Portion ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_House_Portion;
				AddParameter(P_Id_House_Portion,Id_House_Portion,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_House_Number=@"Select * From NOC 
								 where Id_House_Number = @Id_House_Number  order by  Case_Number";
		public const string QDeleteById_House_Number=@"Delete From NOC 
								 where Id_House_Number = @Id_House_Number";
		virtual	public DataTable GetById_House_Number(string Id_House_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_House_Number;
				AddParameter(P_Id_House_Number,Id_House_Number,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_House_Number( string Id_House_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_House_Number;
				AddParameter(P_Id_House_Number,Id_House_Number,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Owner=@"Select * From NOC 
								 where Id_Owner = @Id_Owner  order by  Case_Number";
		public const string QDeleteById_Owner=@"Delete From NOC 
								 where Id_Owner = @Id_Owner";
		virtual	public DataTable GetById_Owner(string Id_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Owner;
				AddParameter(P_Id_Owner,Id_Owner,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Owner( string Id_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Owner;
				AddParameter(P_Id_Owner,Id_Owner,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Tenant=@"Select * From NOC 
								 where Id_Tenant = @Id_Tenant  order by  Case_Number";
		public const string QDeleteById_Tenant=@"Delete From NOC 
								 where Id_Tenant = @Id_Tenant";
		virtual	public DataTable GetById_Tenant(string Id_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Tenant;
				AddParameter(P_Id_Tenant,Id_Tenant,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Tenant( string Id_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Tenant;
				AddParameter(P_Id_Tenant,Id_Tenant,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Power_of_Attorney=@"Select * From NOC 
								 where Id_Power_of_Attorney = @Id_Power_of_Attorney  order by  Case_Number";
		public const string QDeleteById_Power_of_Attorney=@"Delete From NOC 
								 where Id_Power_of_Attorney = @Id_Power_of_Attorney";
		virtual	public DataTable GetById_Power_of_Attorney(string Id_Power_of_Attorney ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Power_of_Attorney;
				AddParameter(P_Id_Power_of_Attorney,Id_Power_of_Attorney,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Power_of_Attorney( string Id_Power_of_Attorney ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Power_of_Attorney;
				AddParameter(P_Id_Power_of_Attorney,Id_Power_of_Attorney,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Guarantor_1=@"Select * From NOC 
								 where Id_Guarantor_1 = @Id_Guarantor_1  order by  Case_Number";
		public const string QDeleteById_Guarantor_1=@"Delete From NOC 
								 where Id_Guarantor_1 = @Id_Guarantor_1";
		virtual	public DataTable GetById_Guarantor_1(string Id_Guarantor_1 ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Guarantor_1;
				AddParameter(P_Id_Guarantor_1,Id_Guarantor_1,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Guarantor_1( string Id_Guarantor_1 ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Guarantor_1;
				AddParameter(P_Id_Guarantor_1,Id_Guarantor_1,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Guarantor_2=@"Select * From NOC 
								 where Id_Guarantor_2 = @Id_Guarantor_2  order by  Case_Number";
		public const string QDeleteById_Guarantor_2=@"Delete From NOC 
								 where Id_Guarantor_2 = @Id_Guarantor_2";
		virtual	public DataTable GetById_Guarantor_2(string Id_Guarantor_2 ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Guarantor_2;
				AddParameter(P_Id_Guarantor_2,Id_Guarantor_2,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Guarantor_2( string Id_Guarantor_2 ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Guarantor_2;
				AddParameter(P_Id_Guarantor_2,Id_Guarantor_2,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Plaza_Number=@"Select * From NOC 
								 where Id_Plaza_Number = @Id_Plaza_Number  order by  Case_Number";
		public const string QDeleteById_Plaza_Number=@"Delete From NOC 
								 where Id_Plaza_Number = @Id_Plaza_Number";
		virtual	public DataTable GetById_Plaza_Number(string Id_Plaza_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Plaza_Number;
				AddParameter(P_Id_Plaza_Number,Id_Plaza_Number,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Plaza_Number( string Id_Plaza_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Plaza_Number;
				AddParameter(P_Id_Plaza_Number,Id_Plaza_Number,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Phase=@"Select * From NOC 
								 where Id_Phase = @Id_Phase  order by  Case_Number";
		public const string QDeleteById_Phase=@"Delete From NOC 
								 where Id_Phase = @Id_Phase";
		virtual	public DataTable GetById_Phase(string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Phase( string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Shop_Number=@"Select * From NOC 
								 where Id_Shop_Number = @Id_Shop_Number  order by  Case_Number";
		public const string QDeleteById_Shop_Number=@"Delete From NOC 
								 where Id_Shop_Number = @Id_Shop_Number";
		virtual	public DataTable GetById_Shop_Number(string Id_Shop_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Shop_Number;
				AddParameter(P_Id_Shop_Number,Id_Shop_Number,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Shop_Number( string Id_Shop_Number ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Shop_Number;
				AddParameter(P_Id_Shop_Number,Id_Shop_Number,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Street=@"Select * From NOC 
								 where Id_Street = @Id_Street  order by  Case_Number";
		public const string QDeleteById_Street=@"Delete From NOC 
								 where Id_Street = @Id_Street";
		virtual	public DataTable GetById_Street(string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Street( string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Doc_Approved_By=@"Select * From NOC 
								 where Id_Doc_Approved_By = @Id_Doc_Approved_By  order by  Case_Number";
		public const string QDeleteById_Doc_Approved_By=@"Delete From NOC 
								 where Id_Doc_Approved_By = @Id_Doc_Approved_By";
		virtual	public DataTable GetById_Doc_Approved_By(string Id_Doc_Approved_By ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Doc_Approved_By;
				AddParameter(P_Id_Doc_Approved_By,Id_Doc_Approved_By,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Doc_Approved_By( string Id_Doc_Approved_By ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Doc_Approved_By;
				AddParameter(P_Id_Doc_Approved_By,Id_Doc_Approved_By,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  NOC
	{
		public NOC()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Id_Residential_property=string.Empty;
		public string Id_Residential_property
		{
			get
			{
				 return _Id_Residential_property;
			}
			set
			{
				  _Id_Residential_property = value;
			}
		}
		protected  string _Id_Commercial_property=string.Empty;
		public string Id_Commercial_property
		{
			get
			{
				 return _Id_Commercial_property;
			}
			set
			{
				  _Id_Commercial_property = value;
			}
		}
		protected  string _Case_Number=string.Empty;
		public string Case_Number
		{
			get
			{
				 return _Case_Number;
			}
			set
			{
				  _Case_Number = value;
			}
		}
		protected  string _Move_In_BTL=string.Empty;
		public string Move_In_BTL
		{
			get
			{
				 return _Move_In_BTL;
			}
			set
			{
				  _Move_In_BTL = value;
			}
		}
		protected  string _Move_Out_BTL=string.Empty;
		public string Move_Out_BTL
		{
			get
			{
				 return _Move_Out_BTL;
			}
			set
			{
				  _Move_Out_BTL = value;
			}
		}
		protected  string _Reason_for_Shifting=string.Empty;
		public string Reason_for_Shifting
		{
			get
			{
				 return _Reason_for_Shifting;
			}
			set
			{
				  _Reason_for_Shifting = value;
			}
		}
		protected  string _Rent_Agreement=string.Empty;
		public string Rent_Agreement
		{
			get
			{
				 return _Rent_Agreement;
			}
			set
			{
				  _Rent_Agreement = value;
			}
		}
		protected  string _Police_Verification_Rpt_Tenant=string.Empty;
		public string Police_Verification_Rpt_Tenant
		{
			get
			{
				 return _Police_Verification_Rpt_Tenant;
			}
			set
			{
				  _Police_Verification_Rpt_Tenant = value;
			}
		}
		protected  string _Last_Paid_Utility_Bills_Owner=string.Empty;
		public string Last_Paid_Utility_Bills_Owner
		{
			get
			{
				 return _Last_Paid_Utility_Bills_Owner;
			}
			set
			{
				  _Last_Paid_Utility_Bills_Owner = value;
			}
		}
		protected  string _NOC_Owner_Tenant=string.Empty;
		public string NOC_Owner_Tenant
		{
			get
			{
				 return _NOC_Owner_Tenant;
			}
			set
			{
				  _NOC_Owner_Tenant = value;
			}
		}
		protected  string _Property_Dealer_VCard=string.Empty;
		public string Property_Dealer_VCard
		{
			get
			{
				 return _Property_Dealer_VCard;
			}
			set
			{
				  _Property_Dealer_VCard = value;
			}
		}
		protected  string _Authority_Letter_Owner=string.Empty;
		public string Authority_Letter_Owner
		{
			get
			{
				 return _Authority_Letter_Owner;
			}
			set
			{
				  _Authority_Letter_Owner = value;
			}
		}
		protected  string _Id_Owner=string.Empty;
		public string Id_Owner
		{
			get
			{
				 return _Id_Owner;
			}
			set
			{
				  _Id_Owner = value;
			}
		}
		protected  string _Id_Tenant=string.Empty;
		public string Id_Tenant
		{
			get
			{
				 return _Id_Tenant;
			}
			set
			{
				  _Id_Tenant = value;
			}
		}
		protected  string _Id_Power_of_Attorney=string.Empty;
		public string Id_Power_of_Attorney
		{
			get
			{
				 return _Id_Power_of_Attorney;
			}
			set
			{
				  _Id_Power_of_Attorney = value;
			}
		}
		protected  string _Id_Guarantor_1=string.Empty;
		public string Id_Guarantor_1
		{
			get
			{
				 return _Id_Guarantor_1;
			}
			set
			{
				  _Id_Guarantor_1 = value;
			}
		}
		protected  string _Id_Guarantor_2=string.Empty;
		public string Id_Guarantor_2
		{
			get
			{
				 return _Id_Guarantor_2;
			}
			set
			{
				  _Id_Guarantor_2 = value;
			}
		}
		protected  DateTime _Doc_Date=new DateTime();
		public DateTime Doc_Date
		{
			get
			{
				 return _Doc_Date;
			}
			set
			{
				  _Doc_Date = value;
			}
		}
		protected  string _Id_Doc_Preparey_By=string.Empty;
		public string Id_Doc_Preparey_By
		{
			get
			{
				 return _Id_Doc_Preparey_By;
			}
			set
			{
				  _Id_Doc_Preparey_By = value;
			}
		}
		protected  string _Id_Doc_Approved_By=string.Empty;
		public string Id_Doc_Approved_By
		{
			get
			{
				 return _Id_Doc_Approved_By;
			}
			set
			{
				  _Id_Doc_Approved_By = value;
			}
		}
		protected  string _Sugestions=string.Empty;
		public string Sugestions
		{
			get
			{
				 return _Sugestions;
			}
			set
			{
				  _Sugestions = value;
			}
		}
		protected  string _Remarks=string.Empty;
		public string Remarks
		{
			get
			{
				 return _Remarks;
			}
			set
			{
				  _Remarks = value;
			}
		}
		protected  string _ID_Card=string.Empty;
		public string ID_Card
		{
			get
			{
				 return _ID_Card;
			}
			set
			{
				  _ID_Card = value;
			}
		}
		protected  string _Ownership_Proof=string.Empty;
		public string Ownership_Proof
		{
			get
			{
				 return _Ownership_Proof;
			}
			set
			{
				  _Ownership_Proof = value;
			}
		}
		protected  string _Id_Plaza_Number=string.Empty;
		public string Id_Plaza_Number
		{
			get
			{
				 return _Id_Plaza_Number;
			}
			set
			{
				  _Id_Plaza_Number = value;
			}
		}
		protected  string _Id_House_Number=string.Empty;
		public string Id_House_Number
		{
			get
			{
				 return _Id_House_Number;
			}
			set
			{
				  _Id_House_Number = value;
			}
		}
		protected  string _Id_House_Portion=string.Empty;
		public string Id_House_Portion
		{
			get
			{
				 return _Id_House_Portion;
			}
			set
			{
				  _Id_House_Portion = value;
			}
		}
		protected  string _Id_Shop_Number=string.Empty;
		public string Id_Shop_Number
		{
			get
			{
				 return _Id_Shop_Number;
			}
			set
			{
				  _Id_Shop_Number = value;
			}
		}
		protected  string _Id_Street=string.Empty;
		public string Id_Street
		{
			get
			{
				 return _Id_Street;
			}
			set
			{
				  _Id_Street = value;
			}
		}
		protected  string _Id_Block=string.Empty;
		public string Id_Block
		{
			get
			{
				 return _Id_Block;
			}
			set
			{
				  _Id_Block = value;
			}
		}
		protected  string _Id_Phase=string.Empty;
		public string Id_Phase
		{
			get
			{
				 return _Id_Phase;
			}
			set
			{
				  _Id_Phase = value;
			}
		}
		protected  string _IP=string.Empty;
		public string IP
		{
			get
			{
				 return _IP;
			}
			set
			{
				  _IP = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public NOC(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QNOC.P_Id))
					Id = obj[QNOC.P_Id].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Residential_property))
					Id_Residential_property = obj[QNOC.P_Id_Residential_property].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Commercial_property))
					Id_Commercial_property = obj[QNOC.P_Id_Commercial_property].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Case_Number))
					Case_Number = obj[QNOC.P_Case_Number].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Move_In_BTL))
					Move_In_BTL = obj[QNOC.P_Move_In_BTL].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Move_Out_BTL))
					Move_Out_BTL = obj[QNOC.P_Move_Out_BTL].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Reason_for_Shifting))
					Reason_for_Shifting = obj[QNOC.P_Reason_for_Shifting].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Rent_Agreement))
					Rent_Agreement = obj[QNOC.P_Rent_Agreement].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Police_Verification_Rpt_Tenant))
					Police_Verification_Rpt_Tenant = obj[QNOC.P_Police_Verification_Rpt_Tenant].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Last_Paid_Utility_Bills_Owner))
					Last_Paid_Utility_Bills_Owner = obj[QNOC.P_Last_Paid_Utility_Bills_Owner].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_NOC_Owner_Tenant))
					NOC_Owner_Tenant = obj[QNOC.P_NOC_Owner_Tenant].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Property_Dealer_VCard))
					Property_Dealer_VCard = obj[QNOC.P_Property_Dealer_VCard].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Authority_Letter_Owner))
					Authority_Letter_Owner = obj[QNOC.P_Authority_Letter_Owner].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Owner))
					Id_Owner = obj[QNOC.P_Id_Owner].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Tenant))
					Id_Tenant = obj[QNOC.P_Id_Tenant].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Power_of_Attorney))
					Id_Power_of_Attorney = obj[QNOC.P_Id_Power_of_Attorney].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Guarantor_1))
					Id_Guarantor_1 = obj[QNOC.P_Id_Guarantor_1].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Guarantor_2))
					Id_Guarantor_2 = obj[QNOC.P_Id_Guarantor_2].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Doc_Date) && obj[QNOC.P_Doc_Date].ToString()!="" && obj[QNOC.P_Doc_Date] is DateTime )
					Doc_Date =(DateTime)obj[QNOC.P_Doc_Date];
				if(obj.Table.Columns.Contains(QNOC.P_Id_Doc_Preparey_By))
					Id_Doc_Preparey_By = obj[QNOC.P_Id_Doc_Preparey_By].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Doc_Approved_By))
					Id_Doc_Approved_By = obj[QNOC.P_Id_Doc_Approved_By].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Sugestions))
					Sugestions = obj[QNOC.P_Sugestions].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Remarks))
					Remarks = obj[QNOC.P_Remarks].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_ID_Card))
					ID_Card = obj[QNOC.P_ID_Card].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Ownership_Proof))
					Ownership_Proof = obj[QNOC.P_Ownership_Proof].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Plaza_Number))
					Id_Plaza_Number = obj[QNOC.P_Id_Plaza_Number].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_House_Number))
					Id_House_Number = obj[QNOC.P_Id_House_Number].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_House_Portion))
					Id_House_Portion = obj[QNOC.P_Id_House_Portion].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Shop_Number))
					Id_Shop_Number = obj[QNOC.P_Id_Shop_Number].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Street))
					Id_Street = obj[QNOC.P_Id_Street].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Block))
					Id_Block = obj[QNOC.P_Id_Block].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_Id_Phase))
					Id_Phase = obj[QNOC.P_Id_Phase].ToString();
				if(obj.Table.Columns.Contains(QNOC.P_IP))
					IP = obj[QNOC.P_IP].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}