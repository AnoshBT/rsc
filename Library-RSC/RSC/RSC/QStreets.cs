using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QStreets:Query
	{

		public const string TABLE_NAME = "Streets";
		public const string P_Id = "Id";
		public const string P_StreetNumber = "StreetNumber";
		public const string SelectColumns=@"Id,StreetNumber";
		public const string QSelect=@"Select * From Streets  order by  StreetNumber";
		public const string QSelectById=@"Select * From Streets 
								 where Id = @Id  order by  StreetNumber";
		public const string QInsert=@"Insert Into  Streets (StreetNumber) 
								 values(@StreetNumber)";
		public const string QUpdateById=@"Update Streets Set StreetNumber=@StreetNumber 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Streets 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Streets 
								 where Id = @Id";

		public QStreets(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QStreets(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QStreets(IDbConnection Connection):base(Connection)
		{
		}
		public QStreets(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Streets Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Streets(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Streets obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Streets obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Streets obj) 
		{
			try
			{
				AddParameter(P_StreetNumber,obj.StreetNumber,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_StreetNumber,obj[P_StreetNumber],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Streets obj,string Id ) 
		{
			try
			{
				AddParameter(P_StreetNumber,obj.StreetNumber,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_StreetNumber,obj[P_StreetNumber],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Streets
	{
		public Streets()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _StreetNumber=string.Empty;
		public string StreetNumber
		{
			get
			{
				 return _StreetNumber;
			}
			set
			{
				  _StreetNumber = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Streets(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QStreets.P_Id))
					Id = obj[QStreets.P_Id].ToString();
				if(obj.Table.Columns.Contains(QStreets.P_StreetNumber))
					StreetNumber = obj[QStreets.P_StreetNumber].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}