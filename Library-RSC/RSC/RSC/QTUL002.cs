using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QTUL002:Query
	{

		public const string TABLE_NAME = "TUL002";
		public const string P_UserID = "UserID";
		public const string P_Password = "Password";
		public const string P_ID = "ID";
		public const string P_Date = "Date";
		public const string P_LastPasswordChange = "LastPasswordChange";
		public const string P_IPAddress = "IPAddress";
		public const string P_LastLoginTime = "LastLoginTime";
		public const string P_LastLogoutTime = "LastLogoutTime";
		public const string P_SecurityQuestion1 = "SecurityQuestion1";
		public const string P_AnsSecurityQuestion = "AnsSecurityQuestion";
		public const string SelectColumns=@"UserID,Password,ID,Date,LastPasswordChange,IPAddress,LastLoginTime,LastLogoutTime,SecurityQuestion1,
						AnsSecurityQuestion";
		public const string QSelect=@"Select * From TUL002  order by  UserID";
		public const string QSelectByUserID=@"Select * From TUL002 
								 where UserID = @UserID  order by  UserID";
		public const string QSelectByID=@"Select UserID,Password,ID,Date,LastPasswordChange,IPAddress,LastLoginTime,LastLogoutTime,SecurityQuestion1,
						AnsSecurityQuestion 
								 From TUL002 
								 where ID=@ID";
		public const string QInsert=@"Insert Into  TUL002 (UserID,Password,Date,LastPasswordChange,IPAddress,LastLoginTime,LastLogoutTime,SecurityQuestion1,
						AnsSecurityQuestion) 
								 values(@UserID,@Password,@Date,@LastPasswordChange,@IPAddress,@LastLoginTime,@LastLogoutTime,@SecurityQuestion1,
						@AnsSecurityQuestion)";
		public const string QUpdateByUserID=@"Update TUL002 Set UserID=@UserID,Password=@Password,Date=@Date,LastPasswordChange=@LastPasswordChange,
						IPAddress=@IPAddress,LastLoginTime=@LastLoginTime,LastLogoutTime=@LastLogoutTime,SecurityQuestion1=@SecurityQuestion1,
						AnsSecurityQuestion=@AnsSecurityQuestion 
								 where UserID = @O_UserID";
		public const string QUpdateByID=@"Update TUL002 Set UserID=@UserID,Password=@Password,Date=@Date,LastPasswordChange=@LastPasswordChange,
						IPAddress=@IPAddress,LastLoginTime=@LastLoginTime,LastLogoutTime=@LastLogoutTime,SecurityQuestion1=@SecurityQuestion1,
						AnsSecurityQuestion=@AnsSecurityQuestion 
								 where ID=@O_ID";
		public const string QDeleteByUserID=@"Delete From TUL002 
								 where UserID = @UserID";
		public const string QDeleteByID=@"Delete From TUL002 
								 where ID=@ID";
		public const string QExistsByUserID=@"select UserID  From TUL002 
								 where UserID = @UserID";

		public QTUL002(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QTUL002(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QTUL002(IDbConnection Connection):base(Connection)
		{
		}
		public QTUL002(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public TUL002 Get(string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByUserID;
				AddParameter(P_UserID,UserID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new TUL002(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public TUL002 GetByID(string ID) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByID;
				AddParameter(P_ID,ID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new TUL002(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(TUL002 obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(TUL002 obj,string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserID;
				SetParameters(obj,UserID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserID;
				SetParameters(obj,UserID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int UpdateByID(TUL002 obj,string ID) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				AddParameter("O_ID",ID,false,false);
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_Password,obj.Password,true,true);
				AddParameter(P_Date,obj.Date,true,true);
				AddParameter(P_LastPasswordChange,obj.LastPasswordChange,true,true);
				AddParameter(P_IPAddress,obj.IPAddress,true,true);
				AddParameter(P_LastLoginTime,obj.LastLoginTime,true,true);
				AddParameter(P_LastLogoutTime,obj.LastLogoutTime,true,true);
				AddParameter(P_SecurityQuestion1,obj.SecurityQuestion1,true,true);
				AddParameter(P_AnsSecurityQuestion,obj.AnsSecurityQuestion,true,true);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int UpdateByID(DataRow obj,string ID) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				AddParameter("O_ID",ID,false,false);
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_Password,obj[P_Password],true,true);
				AddParameter(P_Date,obj[P_Date],true,true);
				AddParameter(P_LastPasswordChange,obj[P_LastPasswordChange],true,true);
				AddParameter(P_IPAddress,obj[P_IPAddress],true,true);
				AddParameter(P_LastLoginTime,obj[P_LastLoginTime],true,true);
				AddParameter(P_LastLogoutTime,obj[P_LastLogoutTime],true,true);
				AddParameter(P_SecurityQuestion1,obj[P_SecurityQuestion1],true,true);
				AddParameter(P_AnsSecurityQuestion,obj[P_AnsSecurityQuestion],true,true);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByID(string ID) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByID;
				AddParameter(P_ID,ID);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(TUL002 obj) 
		{
			try
			{
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_Password,obj.Password,true,true);
				AddParameter(P_Date,obj.Date,true,true);
				AddParameter(P_LastPasswordChange,obj.LastPasswordChange,true,true);
				AddParameter(P_IPAddress,obj.IPAddress,true,true);
				AddParameter(P_LastLoginTime,obj.LastLoginTime,true,true);
				AddParameter(P_LastLogoutTime,obj.LastLogoutTime,true,true);
				AddParameter(P_SecurityQuestion1,obj.SecurityQuestion1,true,true);
				AddParameter(P_AnsSecurityQuestion,obj.AnsSecurityQuestion,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_Password,obj[P_Password],true,true);
				AddParameter(P_Date,obj[P_Date],true,true);
				AddParameter(P_LastPasswordChange,obj[P_LastPasswordChange],true,true);
				AddParameter(P_IPAddress,obj[P_IPAddress],true,true);
				AddParameter(P_LastLoginTime,obj[P_LastLoginTime],true,true);
				AddParameter(P_LastLogoutTime,obj[P_LastLogoutTime],true,true);
				AddParameter(P_SecurityQuestion1,obj[P_SecurityQuestion1],true,true);
				AddParameter(P_AnsSecurityQuestion,obj[P_AnsSecurityQuestion],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(TUL002 obj,string UserID ) 
		{
			try
			{
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_Password,obj.Password,true,true);
				AddParameter(P_Date,obj.Date,true,true);
				AddParameter(P_LastPasswordChange,obj.LastPasswordChange,true,true);
				AddParameter(P_IPAddress,obj.IPAddress,true,true);
				AddParameter(P_LastLoginTime,obj.LastLoginTime,true,true);
				AddParameter(P_LastLogoutTime,obj.LastLogoutTime,true,true);
				AddParameter(P_SecurityQuestion1,obj.SecurityQuestion1,true,true);
				AddParameter(P_AnsSecurityQuestion,obj.AnsSecurityQuestion,true,true);
				AddParameter("O_UserID",UserID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string UserID ) 
		{
			try
			{
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_Password,obj[P_Password],true,true);
				AddParameter(P_Date,obj[P_Date],true,true);
				AddParameter(P_LastPasswordChange,obj[P_LastPasswordChange],true,true);
				AddParameter(P_IPAddress,obj[P_IPAddress],true,true);
				AddParameter(P_LastLoginTime,obj[P_LastLoginTime],true,true);
				AddParameter(P_LastLogoutTime,obj[P_LastLogoutTime],true,true);
				AddParameter(P_SecurityQuestion1,obj[P_SecurityQuestion1],true,true);
				AddParameter(P_AnsSecurityQuestion,obj[P_AnsSecurityQuestion],true,true);
				AddParameter("O_UserID",UserID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  TUL002
	{
		public TUL002()
		{
		}
		protected  string _UserID=string.Empty;
		public string UserID
		{
			get
			{
				 return _UserID;
			}
			set
			{
				  _UserID = value;
			}
		}
		protected  string _Password=string.Empty;
		public string Password
		{
			get
			{
				 return _Password;
			}
			set
			{
				  _Password = value;
			}
		}
		protected  string _ID=string.Empty;
		public string ID
		{
			get
			{
				 return _ID;
			}
			set
			{
				  _ID = value;
			}
		}
		protected  DateTime _Date=new DateTime();
		public DateTime Date
		{
			get
			{
				 return _Date;
			}
			set
			{
				  _Date = value;
			}
		}
		protected  DateTime _LastPasswordChange=new DateTime();
		public DateTime LastPasswordChange
		{
			get
			{
				 return _LastPasswordChange;
			}
			set
			{
				  _LastPasswordChange = value;
			}
		}
		protected  string _IPAddress=string.Empty;
		public string IPAddress
		{
			get
			{
				 return _IPAddress;
			}
			set
			{
				  _IPAddress = value;
			}
		}
		protected  DateTime _LastLoginTime=new DateTime();
		public DateTime LastLoginTime
		{
			get
			{
				 return _LastLoginTime;
			}
			set
			{
				  _LastLoginTime = value;
			}
		}
		protected  DateTime _LastLogoutTime=new DateTime();
		public DateTime LastLogoutTime
		{
			get
			{
				 return _LastLogoutTime;
			}
			set
			{
				  _LastLogoutTime = value;
			}
		}
		protected  string _SecurityQuestion1=string.Empty;
		public string SecurityQuestion1
		{
			get
			{
				 return _SecurityQuestion1;
			}
			set
			{
				  _SecurityQuestion1 = value;
			}
		}
		protected  string _AnsSecurityQuestion=string.Empty;
		public string AnsSecurityQuestion
		{
			get
			{
				 return _AnsSecurityQuestion;
			}
			set
			{
				  _AnsSecurityQuestion = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public TUL002(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QTUL002.P_UserID))
					UserID = obj[QTUL002.P_UserID].ToString();
				if(obj.Table.Columns.Contains(QTUL002.P_Password))
					Password = obj[QTUL002.P_Password].ToString();
				if(obj.Table.Columns.Contains(QTUL002.P_ID))
					ID = obj[QTUL002.P_ID].ToString();
				if(obj.Table.Columns.Contains(QTUL002.P_Date) && obj[QTUL002.P_Date].ToString()!="" && obj[QTUL002.P_Date] is DateTime )
					Date =(DateTime)obj[QTUL002.P_Date];
				if(obj.Table.Columns.Contains(QTUL002.P_LastPasswordChange) && obj[QTUL002.P_LastPasswordChange].ToString()!="" && obj[QTUL002.P_LastPasswordChange] is DateTime )
					LastPasswordChange =(DateTime)obj[QTUL002.P_LastPasswordChange];
				if(obj.Table.Columns.Contains(QTUL002.P_IPAddress))
					IPAddress = obj[QTUL002.P_IPAddress].ToString();
				if(obj.Table.Columns.Contains(QTUL002.P_LastLoginTime) && obj[QTUL002.P_LastLoginTime].ToString()!="" && obj[QTUL002.P_LastLoginTime] is DateTime )
					LastLoginTime =(DateTime)obj[QTUL002.P_LastLoginTime];
				if(obj.Table.Columns.Contains(QTUL002.P_LastLogoutTime) && obj[QTUL002.P_LastLogoutTime].ToString()!="" && obj[QTUL002.P_LastLogoutTime] is DateTime )
					LastLogoutTime =(DateTime)obj[QTUL002.P_LastLogoutTime];
				if(obj.Table.Columns.Contains(QTUL002.P_SecurityQuestion1))
					SecurityQuestion1 = obj[QTUL002.P_SecurityQuestion1].ToString();
				if(obj.Table.Columns.Contains(QTUL002.P_AnsSecurityQuestion))
					AnsSecurityQuestion = obj[QTUL002.P_AnsSecurityQuestion].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}