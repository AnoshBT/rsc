using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QPlaza:Query
	{

		public const string TABLE_NAME = "Plaza";
		public const string P_Id = "Id";
		public const string P_Plaza_Number = "Plaza_Number";
		public const string P_Plaza_Name = "Plaza_Name";
		public const string P_Id_Street = "Id_Street";
		public const string P_Id_Block = "Id_Block";
		public const string P_Id_Phase = "Id_Phase";
		public const string P_Id_Current_Owner = "Id_Current_Owner";
		public const string P_Id_Current_Tenant = "Id_Current_Tenant";
		public const string P_Residential_Commercial = "Residential_Commercial";
		public const string SelectColumns=@"Id,Plaza_Number,Plaza_Name,Id_Street,Id_Block,Id_Phase,Id_Current_Owner,Id_Current_Tenant,Residential_Commercial";
		public const string QSelect=@"Select * From Plaza  order by  Plaza_Number";
		public const string QSelectById=@"Select * From Plaza 
								 where Id = @Id  order by  Plaza_Number";
		public const string QInsert=@"Insert Into  Plaza (Plaza_Number,Plaza_Name,Id_Street,Id_Block,Id_Phase,Id_Current_Owner,Id_Current_Tenant,Residential_Commercial) 
								 values(@Plaza_Number,@Plaza_Name,@Id_Street,@Id_Block,@Id_Phase,@Id_Current_Owner,@Id_Current_Tenant,@Residential_Commercial)";
		public const string QUpdateById=@"Update Plaza Set Plaza_Number=@Plaza_Number,Plaza_Name=@Plaza_Name,Id_Street=@Id_Street,Id_Block=@Id_Block,
						Id_Phase=@Id_Phase,Id_Current_Owner=@Id_Current_Owner,Id_Current_Tenant=@Id_Current_Tenant,Residential_Commercial=@Residential_Commercial 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Plaza 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Plaza 
								 where Id = @Id";

		public QPlaza(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QPlaza(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QPlaza(IDbConnection Connection):base(Connection)
		{
		}
		public QPlaza(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Plaza Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Plaza(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Plaza obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Plaza obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Plaza obj) 
		{
			try
			{
				AddParameter(P_Plaza_Number,obj.Plaza_Number,true,true);
				AddParameter(P_Plaza_Name,obj.Plaza_Name,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
				AddParameter(P_Residential_Commercial,obj.Residential_Commercial,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Plaza_Number,obj[P_Plaza_Number],true,true);
				AddParameter(P_Plaza_Name,obj[P_Plaza_Name],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
				AddParameter(P_Residential_Commercial,obj[P_Residential_Commercial],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Plaza obj,string Id ) 
		{
			try
			{
				AddParameter(P_Plaza_Number,obj.Plaza_Number,true,true);
				AddParameter(P_Plaza_Name,obj.Plaza_Name,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
				AddParameter(P_Residential_Commercial,obj.Residential_Commercial,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Plaza_Number,obj[P_Plaza_Number],true,true);
				AddParameter(P_Plaza_Name,obj[P_Plaza_Name],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
				AddParameter(P_Residential_Commercial,obj[P_Residential_Commercial],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Plaza
	{
		public Plaza()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Plaza_Number=string.Empty;
		public string Plaza_Number
		{
			get
			{
				 return _Plaza_Number;
			}
			set
			{
				  _Plaza_Number = value;
			}
		}
		protected  string _Plaza_Name=string.Empty;
		public string Plaza_Name
		{
			get
			{
				 return _Plaza_Name;
			}
			set
			{
				  _Plaza_Name = value;
			}
		}
		protected  string _Id_Street=string.Empty;
		public string Id_Street
		{
			get
			{
				 return _Id_Street;
			}
			set
			{
				  _Id_Street = value;
			}
		}
		protected  string _Id_Block=string.Empty;
		public string Id_Block
		{
			get
			{
				 return _Id_Block;
			}
			set
			{
				  _Id_Block = value;
			}
		}
		protected  string _Id_Phase=string.Empty;
		public string Id_Phase
		{
			get
			{
				 return _Id_Phase;
			}
			set
			{
				  _Id_Phase = value;
			}
		}
		protected  string _Id_Current_Owner=string.Empty;
		public string Id_Current_Owner
		{
			get
			{
				 return _Id_Current_Owner;
			}
			set
			{
				  _Id_Current_Owner = value;
			}
		}
		protected  string _Id_Current_Tenant=string.Empty;
		public string Id_Current_Tenant
		{
			get
			{
				 return _Id_Current_Tenant;
			}
			set
			{
				  _Id_Current_Tenant = value;
			}
		}
		protected  string _Residential_Commercial=string.Empty;
		public string Residential_Commercial
		{
			get
			{
				 return _Residential_Commercial;
			}
			set
			{
				  _Residential_Commercial = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Plaza(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QPlaza.P_Id))
					Id = obj[QPlaza.P_Id].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Plaza_Number))
					Plaza_Number = obj[QPlaza.P_Plaza_Number].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Plaza_Name))
					Plaza_Name = obj[QPlaza.P_Plaza_Name].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Id_Street))
					Id_Street = obj[QPlaza.P_Id_Street].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Id_Block))
					Id_Block = obj[QPlaza.P_Id_Block].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Id_Phase))
					Id_Phase = obj[QPlaza.P_Id_Phase].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Id_Current_Owner))
					Id_Current_Owner = obj[QPlaza.P_Id_Current_Owner].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Id_Current_Tenant))
					Id_Current_Tenant = obj[QPlaza.P_Id_Current_Tenant].ToString();
				if(obj.Table.Columns.Contains(QPlaza.P_Residential_Commercial))
					Residential_Commercial = obj[QPlaza.P_Residential_Commercial].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}