using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QHouse_Portion:Query
	{

		public const string TABLE_NAME = "House_Portion";
		public const string P_Id = "Id";
		public const string P_Id_HousePortion = "Id_HousePortion";
		public const string P_Id_HouseNumber = "Id_HouseNumber";
		public const string P_Id_Street = "Id_Street";
		public const string P_Id_Phase = "Id_Phase";
		public const string P_Id_Current_Owner = "Id_Current_Owner";
		public const string P_Id_Current_Tenant = "Id_Current_Tenant";
		public const string P_Residential_Commercial = "Residential_Commercial";
		public const string P_Id_Block = "Id_Block";
		public const string SelectColumns=@"Id,Id_HousePortion,Id_HouseNumber,Id_Street,Id_Phase,Id_Current_Owner,Id_Current_Tenant,Residential_Commercial,Id_Block";
		public const string QSelect=@"Select * From House_Portion  order by  Residential_Commercial";
		public const string QSelectById=@"Select * From House_Portion 
								 where Id = @Id  order by  Residential_Commercial";
		public const string QInsert=@"Insert Into  House_Portion (Id_HousePortion,Id_HouseNumber,Id_Street,Id_Phase,Id_Current_Owner,Id_Current_Tenant,Residential_Commercial,Id_Block) 
								 values(@Id_HousePortion,@Id_HouseNumber,@Id_Street,@Id_Phase,@Id_Current_Owner,@Id_Current_Tenant,@Residential_Commercial,@Id_Block)";
		public const string QUpdateById=@"Update House_Portion Set Id_HousePortion=@Id_HousePortion,Id_HouseNumber=@Id_HouseNumber,Id_Street=@Id_Street,Id_Phase=@Id_Phase,
						Id_Current_Owner=@Id_Current_Owner,Id_Current_Tenant=@Id_Current_Tenant,Residential_Commercial=@Residential_Commercial,Id_Block=@Id_Block 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From House_Portion 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From House_Portion 
								 where Id = @Id";

		public QHouse_Portion(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QHouse_Portion(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QHouse_Portion(IDbConnection Connection):base(Connection)
		{
		}
		public QHouse_Portion(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public House_Portion Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new House_Portion(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(House_Portion obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(House_Portion obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(House_Portion obj) 
		{
			try
			{
				AddParameter(P_Id_HousePortion,obj.Id_HousePortion,true,true);
				AddParameter(P_Id_HouseNumber,obj.Id_HouseNumber,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
				AddParameter(P_Residential_Commercial,obj.Residential_Commercial,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Id_HousePortion,obj[P_Id_HousePortion],true,true);
				AddParameter(P_Id_HouseNumber,obj[P_Id_HouseNumber],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
				AddParameter(P_Residential_Commercial,obj[P_Residential_Commercial],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(House_Portion obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_HousePortion,obj.Id_HousePortion,true,true);
				AddParameter(P_Id_HouseNumber,obj.Id_HouseNumber,true,true);
				AddParameter(P_Id_Street,obj.Id_Street,true,true);
				AddParameter(P_Id_Phase,obj.Id_Phase,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
				AddParameter(P_Residential_Commercial,obj.Residential_Commercial,true,true);
				AddParameter(P_Id_Block,obj.Id_Block,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_HousePortion,obj[P_Id_HousePortion],true,true);
				AddParameter(P_Id_HouseNumber,obj[P_Id_HouseNumber],true,true);
				AddParameter(P_Id_Street,obj[P_Id_Street],true,true);
				AddParameter(P_Id_Phase,obj[P_Id_Phase],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
				AddParameter(P_Residential_Commercial,obj[P_Residential_Commercial],true,true);
				AddParameter(P_Id_Block,obj[P_Id_Block],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Block=@"Select * From House_Portion 
								 where Id_Block = @Id_Block  order by  Residential_Commercial";
		public const string QDeleteById_Block=@"Delete From House_Portion 
								 where Id_Block = @Id_Block";
		virtual	public DataTable GetById_Block(string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Block( string Id_Block ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Block;
				AddParameter(P_Id_Block,Id_Block,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_HouseNumber=@"Select * From House_Portion 
								 where Id_HouseNumber = @Id_HouseNumber  order by  Residential_Commercial";
		public const string QDeleteById_HouseNumber=@"Delete From House_Portion 
								 where Id_HouseNumber = @Id_HouseNumber";
		virtual	public DataTable GetById_HouseNumber(string Id_HouseNumber ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_HouseNumber;
				AddParameter(P_Id_HouseNumber,Id_HouseNumber,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_HouseNumber( string Id_HouseNumber ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_HouseNumber;
				AddParameter(P_Id_HouseNumber,Id_HouseNumber,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Current_Owner=@"Select * From House_Portion 
								 where Id_Current_Owner = @Id_Current_Owner  order by  Residential_Commercial";
		public const string QDeleteById_Current_Owner=@"Delete From House_Portion 
								 where Id_Current_Owner = @Id_Current_Owner";
		virtual	public DataTable GetById_Current_Owner(string Id_Current_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Current_Owner;
				AddParameter(P_Id_Current_Owner,Id_Current_Owner,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Current_Owner( string Id_Current_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Current_Owner;
				AddParameter(P_Id_Current_Owner,Id_Current_Owner,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Current_Tenant=@"Select * From House_Portion 
								 where Id_Current_Tenant = @Id_Current_Tenant  order by  Residential_Commercial";
		public const string QDeleteById_Current_Tenant=@"Delete From House_Portion 
								 where Id_Current_Tenant = @Id_Current_Tenant";
		virtual	public DataTable GetById_Current_Tenant(string Id_Current_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Current_Tenant;
				AddParameter(P_Id_Current_Tenant,Id_Current_Tenant,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Current_Tenant( string Id_Current_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Current_Tenant;
				AddParameter(P_Id_Current_Tenant,Id_Current_Tenant,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_HousePortion=@"Select * From House_Portion 
								 where Id_HousePortion = @Id_HousePortion  order by  Residential_Commercial";
		public const string QDeleteById_HousePortion=@"Delete From House_Portion 
								 where Id_HousePortion = @Id_HousePortion";
		virtual	public DataTable GetById_HousePortion(string Id_HousePortion ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_HousePortion;
				AddParameter(P_Id_HousePortion,Id_HousePortion,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_HousePortion( string Id_HousePortion ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_HousePortion;
				AddParameter(P_Id_HousePortion,Id_HousePortion,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Phase=@"Select * From House_Portion 
								 where Id_Phase = @Id_Phase  order by  Residential_Commercial";
		public const string QDeleteById_Phase=@"Delete From House_Portion 
								 where Id_Phase = @Id_Phase";
		virtual	public DataTable GetById_Phase(string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Phase( string Id_Phase ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Phase;
				AddParameter(P_Id_Phase,Id_Phase,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Street=@"Select * From House_Portion 
								 where Id_Street = @Id_Street  order by  Residential_Commercial";
		public const string QDeleteById_Street=@"Delete From House_Portion 
								 where Id_Street = @Id_Street";
		virtual	public DataTable GetById_Street(string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Street( string Id_Street ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Street;
				AddParameter(P_Id_Street,Id_Street,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  House_Portion
	{
		public House_Portion()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Id_HousePortion=string.Empty;
		public string Id_HousePortion
		{
			get
			{
				 return _Id_HousePortion;
			}
			set
			{
				  _Id_HousePortion = value;
			}
		}
		protected  string _Id_HouseNumber=string.Empty;
		public string Id_HouseNumber
		{
			get
			{
				 return _Id_HouseNumber;
			}
			set
			{
				  _Id_HouseNumber = value;
			}
		}
		protected  string _Id_Street=string.Empty;
		public string Id_Street
		{
			get
			{
				 return _Id_Street;
			}
			set
			{
				  _Id_Street = value;
			}
		}
		protected  string _Id_Phase=string.Empty;
		public string Id_Phase
		{
			get
			{
				 return _Id_Phase;
			}
			set
			{
				  _Id_Phase = value;
			}
		}
		protected  string _Id_Current_Owner=string.Empty;
		public string Id_Current_Owner
		{
			get
			{
				 return _Id_Current_Owner;
			}
			set
			{
				  _Id_Current_Owner = value;
			}
		}
		protected  string _Id_Current_Tenant=string.Empty;
		public string Id_Current_Tenant
		{
			get
			{
				 return _Id_Current_Tenant;
			}
			set
			{
				  _Id_Current_Tenant = value;
			}
		}
		protected  string _Residential_Commercial=string.Empty;
		public string Residential_Commercial
		{
			get
			{
				 return _Residential_Commercial;
			}
			set
			{
				  _Residential_Commercial = value;
			}
		}
		protected  string _Id_Block=string.Empty;
		public string Id_Block
		{
			get
			{
				 return _Id_Block;
			}
			set
			{
				  _Id_Block = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public House_Portion(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id))
					Id = obj[QHouse_Portion.P_Id].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_HousePortion))
					Id_HousePortion = obj[QHouse_Portion.P_Id_HousePortion].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_HouseNumber))
					Id_HouseNumber = obj[QHouse_Portion.P_Id_HouseNumber].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_Street))
					Id_Street = obj[QHouse_Portion.P_Id_Street].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_Phase))
					Id_Phase = obj[QHouse_Portion.P_Id_Phase].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_Current_Owner))
					Id_Current_Owner = obj[QHouse_Portion.P_Id_Current_Owner].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_Current_Tenant))
					Id_Current_Tenant = obj[QHouse_Portion.P_Id_Current_Tenant].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Residential_Commercial))
					Residential_Commercial = obj[QHouse_Portion.P_Residential_Commercial].ToString();
				if(obj.Table.Columns.Contains(QHouse_Portion.P_Id_Block))
					Id_Block = obj[QHouse_Portion.P_Id_Block].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}