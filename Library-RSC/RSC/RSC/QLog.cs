using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QLog:Query
	{

		public const string TABLE_NAME = "Log";
		public const string P_Id = "Id";
		public const string P_User = "User";
		public const string P_LoginTime = "LoginTime";
		public const string P_Logout = "Logout";
		public const string P_IPaddress = "IPaddress";
		public const string SelectColumns=@"Id,User,LoginTime,Logout,IPaddress";
		public const string QSelect=@"Select * From Log  order by  User";
		public const string QSelectById=@"Select * From Log 
								 where Id = @Id  order by  User";
		public const string QInsert=@"Insert Into  Log (User,LoginTime,Logout,IPaddress) 
								 values(@User,@LoginTime,@Logout,@IPaddress)";
		public const string QUpdateById=@"Update Log Set User=@User,LoginTime=@LoginTime,Logout=@Logout,IPaddress=@IPaddress 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Log 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Log 
								 where Id = @Id";

		public QLog(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QLog(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QLog(IDbConnection Connection):base(Connection)
		{
		}
		public QLog(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Log Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Log(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Log obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Log obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Log obj) 
		{
			try
			{
				AddParameter(P_User,obj.User,true,true);
				AddParameter(P_LoginTime,obj.LoginTime,true,true);
				AddParameter(P_Logout,obj.Logout,true,true);
				AddParameter(P_IPaddress,obj.IPaddress,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_User,obj[P_User],true,true);
				AddParameter(P_LoginTime,obj[P_LoginTime],true,true);
				AddParameter(P_Logout,obj[P_Logout],true,true);
				AddParameter(P_IPaddress,obj[P_IPaddress],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Log obj,string Id ) 
		{
			try
			{
				AddParameter(P_User,obj.User,true,true);
				AddParameter(P_LoginTime,obj.LoginTime,true,true);
				AddParameter(P_Logout,obj.Logout,true,true);
				AddParameter(P_IPaddress,obj.IPaddress,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_User,obj[P_User],true,true);
				AddParameter(P_LoginTime,obj[P_LoginTime],true,true);
				AddParameter(P_Logout,obj[P_Logout],true,true);
				AddParameter(P_IPaddress,obj[P_IPaddress],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Log
	{
		public Log()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _User=string.Empty;
		public string User
		{
			get
			{
				 return _User;
			}
			set
			{
				  _User = value;
			}
		}
		protected  DateTime _LoginTime=new DateTime();
		public DateTime LoginTime
		{
			get
			{
				 return _LoginTime;
			}
			set
			{
				  _LoginTime = value;
			}
		}
		protected  DateTime _Logout=new DateTime();
		public DateTime Logout
		{
			get
			{
				 return _Logout;
			}
			set
			{
				  _Logout = value;
			}
		}
		protected  string _IPaddress=string.Empty;
		public string IPaddress
		{
			get
			{
				 return _IPaddress;
			}
			set
			{
				  _IPaddress = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Log(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QLog.P_Id))
					Id = obj[QLog.P_Id].ToString();
				if(obj.Table.Columns.Contains(QLog.P_User))
					User = obj[QLog.P_User].ToString();
				if(obj.Table.Columns.Contains(QLog.P_LoginTime) && obj[QLog.P_LoginTime].ToString()!="" && obj[QLog.P_LoginTime] is DateTime )
					LoginTime =(DateTime)obj[QLog.P_LoginTime];
				if(obj.Table.Columns.Contains(QLog.P_Logout) && obj[QLog.P_Logout].ToString()!="" && obj[QLog.P_Logout] is DateTime )
					Logout =(DateTime)obj[QLog.P_Logout];
				if(obj.Table.Columns.Contains(QLog.P_IPaddress))
					IPaddress = obj[QLog.P_IPaddress].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}