using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QShop_Office_Employees:Query
	{

		public const string TABLE_NAME = "Shop_Office_Employees";
		public const string P_Id = "Id";
		public const string P_Emp_First_Name = "Emp_First_Name";
		public const string P_Emp_Last_Name = "Emp_Last_Name";
		public const string P_Cell_No = "Cell_No";
		public const string P_Id_Shop_Flat_Office = "Id_Shop_Flat_Office";
		public const string P_CNIC = "CNIC";
		public const string P_Date_From = "Date_From";
		public const string P_Date_To = "Date_To";
		public const string SelectColumns=@"Id,Emp_First_Name,Emp_Last_Name,Cell_No,Id_Shop_Flat_Office,CNIC,Date_From,Date_To";
		public const string QSelect=@"Select * From Shop_Office_Employees  order by  Emp_First_Name";
		public const string QSelectById=@"Select * From Shop_Office_Employees 
								 where Id = @Id  order by  Emp_First_Name";
		public const string QInsert=@"Insert Into  Shop_Office_Employees (Emp_First_Name,Emp_Last_Name,Cell_No,Id_Shop_Flat_Office,CNIC,Date_From,Date_To) 
								 values(@Emp_First_Name,@Emp_Last_Name,@Cell_No,@Id_Shop_Flat_Office,@CNIC,@Date_From,@Date_To)";
		public const string QUpdateById=@"Update Shop_Office_Employees Set Emp_First_Name=@Emp_First_Name,Emp_Last_Name=@Emp_Last_Name,Cell_No=@Cell_No,Id_Shop_Flat_Office=@Id_Shop_Flat_Office,
						CNIC=@CNIC,Date_From=@Date_From,Date_To=@Date_To 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Shop_Office_Employees 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Shop_Office_Employees 
								 where Id = @Id";

		public QShop_Office_Employees(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QShop_Office_Employees(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QShop_Office_Employees(IDbConnection Connection):base(Connection)
		{
		}
		public QShop_Office_Employees(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Shop_Office_Employees Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Shop_Office_Employees(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Shop_Office_Employees obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Shop_Office_Employees obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Shop_Office_Employees obj) 
		{
			try
			{
				AddParameter(P_Emp_First_Name,obj.Emp_First_Name,true,false);
				AddParameter(P_Emp_Last_Name,obj.Emp_Last_Name,true,true);
				AddParameter(P_Cell_No,obj.Cell_No,true,true);
				AddParameter(P_Id_Shop_Flat_Office,obj.Id_Shop_Flat_Office,true,false);
				AddParameter(P_CNIC,obj.CNIC,true,true);
				AddParameter(P_Date_From,obj.Date_From,true,true);
				AddParameter(P_Date_To,obj.Date_To,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Emp_First_Name,obj[P_Emp_First_Name],true,false);
				AddParameter(P_Emp_Last_Name,obj[P_Emp_Last_Name],true,true);
				AddParameter(P_Cell_No,obj[P_Cell_No],true,true);
				AddParameter(P_Id_Shop_Flat_Office,obj[P_Id_Shop_Flat_Office],true,false);
				AddParameter(P_CNIC,obj[P_CNIC],true,true);
				AddParameter(P_Date_From,obj[P_Date_From],true,true);
				AddParameter(P_Date_To,obj[P_Date_To],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Shop_Office_Employees obj,string Id ) 
		{
			try
			{
				AddParameter(P_Emp_First_Name,obj.Emp_First_Name,true,false);
				AddParameter(P_Emp_Last_Name,obj.Emp_Last_Name,true,true);
				AddParameter(P_Cell_No,obj.Cell_No,true,true);
				AddParameter(P_Id_Shop_Flat_Office,obj.Id_Shop_Flat_Office,true,false);
				AddParameter(P_CNIC,obj.CNIC,true,true);
				AddParameter(P_Date_From,obj.Date_From,true,true);
				AddParameter(P_Date_To,obj.Date_To,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Emp_First_Name,obj[P_Emp_First_Name],true,false);
				AddParameter(P_Emp_Last_Name,obj[P_Emp_Last_Name],true,true);
				AddParameter(P_Cell_No,obj[P_Cell_No],true,true);
				AddParameter(P_Id_Shop_Flat_Office,obj[P_Id_Shop_Flat_Office],true,false);
				AddParameter(P_CNIC,obj[P_CNIC],true,true);
				AddParameter(P_Date_From,obj[P_Date_From],true,true);
				AddParameter(P_Date_To,obj[P_Date_To],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Shop_Flat_Office=@"Select * From Shop_Office_Employees 
								 where Id_Shop_Flat_Office = @Id_Shop_Flat_Office  order by  Emp_First_Name";
		public const string QDeleteById_Shop_Flat_Office=@"Delete From Shop_Office_Employees 
								 where Id_Shop_Flat_Office = @Id_Shop_Flat_Office";
		virtual	public DataTable GetById_Shop_Flat_Office(string Id_Shop_Flat_Office ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Shop_Flat_Office;
				AddParameter(P_Id_Shop_Flat_Office,Id_Shop_Flat_Office,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Shop_Flat_Office( string Id_Shop_Flat_Office ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Shop_Flat_Office;
				AddParameter(P_Id_Shop_Flat_Office,Id_Shop_Flat_Office,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Shop_Office_Employees
	{
		public Shop_Office_Employees()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Emp_First_Name=string.Empty;
		public string Emp_First_Name
		{
			get
			{
				 return _Emp_First_Name;
			}
			set
			{
				  _Emp_First_Name = value;
			}
		}
		protected  string _Emp_Last_Name=string.Empty;
		public string Emp_Last_Name
		{
			get
			{
				 return _Emp_Last_Name;
			}
			set
			{
				  _Emp_Last_Name = value;
			}
		}
		protected  string _Cell_No=string.Empty;
		public string Cell_No
		{
			get
			{
				 return _Cell_No;
			}
			set
			{
				  _Cell_No = value;
			}
		}
		protected  string _Id_Shop_Flat_Office=string.Empty;
		public string Id_Shop_Flat_Office
		{
			get
			{
				 return _Id_Shop_Flat_Office;
			}
			set
			{
				  _Id_Shop_Flat_Office = value;
			}
		}
		protected  string _CNIC=string.Empty;
		public string CNIC
		{
			get
			{
				 return _CNIC;
			}
			set
			{
				  _CNIC = value;
			}
		}
		protected  DateTime _Date_From=new DateTime();
		public DateTime Date_From
		{
			get
			{
				 return _Date_From;
			}
			set
			{
				  _Date_From = value;
			}
		}
		protected  DateTime _Date_To=new DateTime();
		public DateTime Date_To
		{
			get
			{
				 return _Date_To;
			}
			set
			{
				  _Date_To = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Shop_Office_Employees(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Id))
					Id = obj[QShop_Office_Employees.P_Id].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Emp_First_Name))
					Emp_First_Name = obj[QShop_Office_Employees.P_Emp_First_Name].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Emp_Last_Name))
					Emp_Last_Name = obj[QShop_Office_Employees.P_Emp_Last_Name].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Cell_No))
					Cell_No = obj[QShop_Office_Employees.P_Cell_No].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Id_Shop_Flat_Office))
					Id_Shop_Flat_Office = obj[QShop_Office_Employees.P_Id_Shop_Flat_Office].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_CNIC))
					CNIC = obj[QShop_Office_Employees.P_CNIC].ToString();
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Date_From) && obj[QShop_Office_Employees.P_Date_From].ToString()!="" && obj[QShop_Office_Employees.P_Date_From] is DateTime )
					Date_From =(DateTime)obj[QShop_Office_Employees.P_Date_From];
				if(obj.Table.Columns.Contains(QShop_Office_Employees.P_Date_To) && obj[QShop_Office_Employees.P_Date_To].ToString()!="" && obj[QShop_Office_Employees.P_Date_To] is DateTime )
					Date_To =(DateTime)obj[QShop_Office_Employees.P_Date_To];
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}