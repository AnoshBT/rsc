using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Data.OracleClient;
using System.Data.OleDb;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

public enum Operators : int { GreaterEqual = 8805, LessEqual = 8804, NotEqual = 8800 };
public enum ConnectionStringPart { DataSource, UserID, Password, Database, Provider }
public enum ProviderType { SQLCLIENT = 0, ORACLE = 1, OLEDB = 2, MSACCESS = 3, NONE = 4 };
public partial class DBProvider
{
    /// <summary>
    /// SQLCLIENT
    /// </summary>
    public const string SQLCLIENT = "SQLCLIENT";
    /// <summary>
    /// ORACLECLIENT
    /// </summary>
    public const string ORACLECLIENT = "ORACLECLIENT";
    /// <summary>
    /// OLEDB
    /// </summary>
    public const string OLEDB = "OLEDB";
    /// <summary>
    /// ODBC
    /// </summary>
    public const string ODBC = "ODBC";


}

    /// <summary>
    /// MS SQLServer query
    /// </summary>
    public partial class DBQuery : IQuery
    {
         
        #region Declaration
        protected string connectionString = "";
        protected IDbConnection connection = null;
        protected IDbCommand command = null;
        protected IDbTransaction transaction = null;
        protected IDbDataAdapter dataAdapter = null;
        protected string provider=DBProvider.SQLCLIENT;
        protected bool autoConnectionClose = true;
        #endregion

        #region Properties
        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            set
            {
                connectionString = value;
                //if (General.GetConnectionStringPart(value, ConnectionStringPart.Provider).Length > 0)
                //    connectionString = General.GetIDbConnectionString(value);
                if (connectionString != "")
                {
                    if (connection == null)
                    {
                        switch (provider.ToUpper())
                        {
                            case DBProvider.SQLCLIENT:
                                connection = new SqlConnection(ConnectionString);
                                break;
                            case DBProvider.ORACLECLIENT:
                                connection = new OracleConnection(ConnectionString);
                                break;
                            case DBProvider.OLEDB:
                                connection = new OleDbConnection(ConnectionString);
                                break;


                        }
                        connection.ConnectionString = connectionString;

                    }
                }
            }
        }
        public string Provider
        {
            get {
                  return provider;
                }
            set
            {
                provider = value;
            }
        }
        public bool IsConnectionOpen
        {
            get
            {
                if (connection != null && ((connection.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open))
                    return true;
                return false;
            }
        }
        /// <summary>
        /// Close Connection after each command execution 
        /// </summary>
        public bool IsAutoConnectionClose
        {
            get
            {
                return autoConnectionClose;
            }
            set
            {
                autoConnectionClose = value;
            }
        }
        public IDbTransaction Transaction
        {
            get {
                if(command==null)return null;
               return command.Transaction; }
            set
            {
                transaction = value;
                Command.Transaction = transaction;
            }
        }
        public bool IsTransaction
        {
            get
            {
                if (command==null ||command.Transaction == null)
                    return false;
                return true;
            }
        }
        public IDbConnection Connection
        {
            get
            {
               
                    if (connection == null)
                    {
                        connection = NewConnection(connectionString); 
                    }
                return connection;
            }
            set
            {
                connection = value;
                if (command != null)
                    command.Connection = connection;
            }
        }
        public IDbCommand Command
        {
            get
            {
                if (command == null)
                {
                    command = Connection.CreateCommand();
                }
                return command;
            }
            set
            {
                command = value;
                if (command.Connection != null)
                    connection = command.Connection;
            }
        }
        public IDbDataAdapter DataAdapter
        {
            get
            {
                if (dataAdapter == null)
                {
                    dataAdapter = NewDataAdapter(Command);
                }
                return dataAdapter;
            }
            set
            {
                dataAdapter = value;
            }
        }
        public IDataParameterCollection Parameters
        {
            get
            {
                return Command.Parameters;
            }
        }
        public string CommandText
        {
            get
            {
                return Command.CommandText;
            }
            set
            {
                Command.CommandText =GetQuery(value);
            }
        }
        #endregion

        #region constructors
        public DBQuery(string IDbConnectionString)
        {           
            this.ConnectionString = IDbConnectionString;           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IDbConnectionString">Connection String</param>
        /// <param name="Provider">SQLCLIENT,ORACLECLIENT,OLEDB </param>
        public DBQuery(string IDbConnectionString,string DBProvider)
        {
            this.ConnectionString = IDbConnectionString;
            Provider = DBProvider;
        }
        public DBQuery(IDbCommand _command)
        {
            this.connection = _command.Connection;
            this.command = _command;
            this.transaction = _command.Transaction;
            //
            // TODO: Add constructor logic here
            //
        }
        public DBQuery(IDbConnection _connection)
        {
             Connection = _connection;
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion

        #region
        ~DBQuery()
        {
            if (IsConnectionOpen)
            {
                connection.Close();
                connection = null;
                transaction = null;
                command = null;
            }
        }
        #endregion

        #region Fuctions
        public bool Connect()
        {
            try
            {
                if (!IsConnectionOpen) Connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Close the connection.If Transaction is not committed then rollback the transaction.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                // Disconnect can be called from Dispose and should guarantee no errors

                if (!IsConnectionOpen)
                    return;
                if (IsTransaction)
                    RollbackTransaction();
                if (IsConnectionOpen)
                    connection.Close();
            }
            catch (Exception ex)
            {
                if (connection != null)
                    connection.Close();
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }


        }
        public void BeginTransaction()
        {
            Connect();
            Transaction = Connection.BeginTransaction();
            Command.Transaction = Transaction;
        }
        /// <summary>
        ///  Save changes on database.
        /// </summary>
        public void CommitTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Commit();
                Transaction = null;
            }
        }
        /// <summary>
        /// do not save changes on database.
        /// </summary>
        public void RollbackTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Rollback();
                Transaction = null;
            }
        }
        public virtual DataTable GetUserTables()
        {
            try
            {
                string query = "SELECT name FROM sysobjects WHERE  (xtype = 'U') order by Name";
                return GetDataTable(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public virtual DataTable GetSchemaTable(string TableName)
        {

            try
            {
                string query = "select * From " + TableName;
                Command.CommandText = query;
                Connect();
                IDataReader reader = Command.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
                DataTable tableSchema;
                tableSchema = reader.GetSchemaTable();
                reader.Close();
                return tableSchema;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose) Disconnect();
            }
        }
        public DataSet GetDataSet(string query)
        {
            try
            {
                return GetDataSet(query, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public DataSet GetDataSet(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();

            }
        }
        public bool FillDataSet(string query, DataSet ds)
        {
            try
            {
                return FillDataSet(query, ds, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool FillDataSet(string query, DataSet ds, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataAdapter.Fill(ds);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public DataRow GetDataRow(string TableName, bool IsDistinct)
        {
            return GetDataRow(TableName, "*", string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields)
        {
            return GetDataRow(TableName, fields, false);
        }
        public DataRow GetDataRow(string TableName, string fields, bool IsDistinct)
        {
            return GetDataRow(TableName, fields, string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition)
        {
            return GetDataRow(TableName, fields, Condition, false);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" TOP 1 {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataRow(query.ToString());

        }
        public DataRow GetDataRow(string query)
        {
            return GetDataRow(query, CommandType.Text);
        }
        public DataRow GetDataRow(string query, CommandType commandType)
        {
            try
            {
                 DataTable dt =GetDataTable(query,commandType);
                 if (dt.Rows.Count > 0)
                    return dt.Rows[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }
        public DataTable GetDataTable(string TableName, bool IsDistinct)
        {
            return GetDataTable(TableName, "*", string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields)
        {
            return GetDataTable(TableName, fields, false);
        }
        public DataTable GetDataTable(string TableName, string fields, bool IsDistinct)
        {
            return GetDataTable(TableName, fields, string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition)
        {
            return GetDataTable(TableName, fields, Condition, false);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataTable(query.ToString());

        }
        public DataTable GetDataTable(string query)
        {
            return GetDataTable(query, CommandType.Text);
        }
       
       
        public DataTable GetDataTable(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseDataBoundControl"></param>
        /// <param name="query"></param>
        public void BindControl(BaseDataBoundControl baseDataBoundControl, string query)
        {
            try
            {
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = "";
                baseDataBoundControl.DataBind();
                Connect();
                CommandText = query;
                IDataReader rd = Command.ExecuteReader();
                if (rd.FieldCount == 0) return;
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = rd;
                if (baseDataBoundControl is ListControl && rd.FieldCount > 0)
                {
                    ListControl lc = (ListControl)baseDataBoundControl;
                    if (lc.DataTextField == "")
                        lc.DataTextField = rd.GetName(0);
                    if (rd.FieldCount > 1 && lc.DataValueField == "")
                        lc.DataValueField = rd.GetName(1);
                }
                baseDataBoundControl.DataBind();
                rd.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query);
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query, ListItem FirstItem)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControl(ListControl ddl, string query, ListItem FirstItem)
        {
            BindControl(ddl, query);
            ddl.Items.Insert(0, FirstItem);
        }
        public void BindControl(ListControl ddl, string TableName, string DataTextValueField)
        {

            string query = "";
            query = "select distinct  " + DataTextValueField + "  from  " + TableName + " order by " + DataTextValueField;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void ExecuteScriptFile(string SriptFilePath)
        {
            FileInfo file = new FileInfo(SriptFilePath);
            string script = file.OpenText().ReadToEnd();
            //Server server = new Server(new ServerConnection(conn));
            //server.ConnectionContext.ExecuteNonQuery(script);
        }
        #region Wraper methods for ADO.NET
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL)
        {
            return this.ExecuteReader(sSQL, CommandType.Text);
        }
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="oType"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandBehavior comBehavior)
        {
            try
            {
                return ExecuteReader(sSQL, CommandType.Text, comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandType oType, CommandBehavior comBehavior)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader(comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public object ExecuteScalar(string TableName, string field)
        {
            return ExecuteScalar(TableName, field, string.Empty);
        }
        public object ExecuteScalar(string TableName, string field, string Condition)
        {
            StringBuilder query = new StringBuilder();
            query.AppendFormat("select {0}  From  {1} ", TableName, field);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteScalar(query.ToString(), CommandType.Text);
        }
        public object ExecuteScalar(string sSQL)
        {
            return ExecuteScalar(sSQL, CommandType.Text);
        }
        public object ExecuteScalarDefault(string sSQL, object DefaultValue)
        {
            object value = ExecuteScalar(sSQL);
            if (value == null || value == System.DBNull.Value)
                value = DefaultValue;
            return value;
        }
        public bool IsRecordExist(string sSQL)
        {
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value) return true;
            return false;
        }
        public bool IsRecordExist(string sSQL, out object value)
        {
            value = null;
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value)
            {
                value = obj;
                return true;
            }
            return false;
        }
        public object ExecuteScalar(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                object obj = Command.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    return obj;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public int ExecuteNonQuery(string sSQL)
        {
            return ExecuteNonQuery(sSQL, CommandType.Text);
        }
        public int ExecuteNonQuery(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void AddParameter(IDbDataParameter oParam)
        {
            if (oParam.Value == null)
                oParam.Value = DBNull.Value;
            foreach (IDbDataParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam.ParameterName, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = oParam.Value;
                    return;
                }
            }
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value)
        {
            oParam_Name = oParam_Name.Trim();
            string type = ParamterType();
            if (oParam_Name.Length > 0 && oParam_Name[0].ToString() != type)
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = type + oParam_Name;
                else oParam_Name = type + oParam_Name.Remove(0, 1);
            }
            if (value == null || value.ToString() == DateTime.MinValue.ToString())
                value = DBNull.Value;

            foreach (IDbDataParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            IDbDataParameter oParam =  NewParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull)
        {
            oParam_Name = oParam_Name.Trim();
            string type = ParamterType();
            if (oParam_Name.Length > 0 && oParam_Name[0].ToString() != type)
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = type + oParam_Name;
                else oParam_Name = type + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;


            foreach (IDbDataParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            IDbDataParameter oParam = NewParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull, bool IsNullAble)
        {
            oParam_Name = oParam_Name.Trim();
            string type = ParamterType();
            if (oParam_Name.Length > 0 && oParam_Name[0].ToString() != type)
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = type + oParam_Name;
                else oParam_Name = type + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (!IsNullAble)// Null Value is not allowed
            {
                if (value == null || value.ToString() == date.ToString())
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
                else if (IsEmptyNull && value.ToString() == "")
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
            }

            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;

            foreach (IDbDataParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            IDbDataParameter oParam = NewParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void ClearParameters()
        {
            Command.Parameters.Clear();
        }
        public int Delete(string TableName, string Condition)
        {
            StringBuilder query = new StringBuilder("delete");
            query.AppendFormat(" From {0} ", TableName);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteNonQuery(query.ToString());
        }
        public IDbDataParameter NewParameter()
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return new SqlParameter();
                    
                case DBProvider.ORACLECLIENT:
                    return new OracleParameter();
                    
                case DBProvider.OLEDB:
                    return new OleDbParameter();
                    
            }
            return null;
        }
        public IDbDataParameter NewParameter(string oParam_Name,object  value)
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return new SqlParameter(oParam_Name, value);

                case DBProvider.ORACLECLIENT:
                    return new OracleParameter(oParam_Name, value);

                case DBProvider.OLEDB:
                    return new OleDbParameter(oParam_Name, value);

            }
            return null;
        }
        public IDbConnection NewConnection()
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return new SqlConnection();
                    ;
                case DBProvider.ORACLECLIENT:
                    return new OracleConnection();
                    ;
                case DBProvider.OLEDB:
                    return new OleDbConnection();
                    ;
            }
            return null;
        }
        public IDbConnection NewConnection(string conectionString)
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return new SqlConnection(conectionString);
                    ;
                case DBProvider.ORACLECLIENT:
                    return new OracleConnection(conectionString);
                    ;
                case DBProvider.OLEDB:
                    return new OleDbConnection(conectionString);
                    ;
            }
            return null;
        }
        public IDbDataAdapter NewDataAdapter(IDbCommand cmd)
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return new SqlDataAdapter((SqlCommand)cmd);
                    ;
                case DBProvider.ORACLECLIENT:
                    return new OracleDataAdapter((OracleCommand)cmd);
                    ;
                case DBProvider.OLEDB:
                    return new OleDbDataAdapter((OleDbCommand)cmd);
                    ;
            }
            return null;
        }
        public string ParamterType()
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return "@";
                    
                case DBProvider.ORACLECLIENT:
                    return ":";
                    
                case DBProvider.OLEDB:
                    return "?";
                    
            }
            return "";
        }
        public string GetQuery(string query)
        {
            switch (provider.ToUpper())
            {
                case DBProvider.SQLCLIENT:
                    return  query;

                case DBProvider.ORACLECLIENT:
                    return query.Replace('@', ':');

              //  case DBProvider.OLEDB:
              //      return "?";

            }
            return query;
        }
        #endregion


        #endregion

        #region IDisposable Members 

        public void Dispose()
        {
            Disconnect();
            connection = null;
            command = null;
        }

        #endregion
    }
    public interface IQuery : IDisposable
    {
        void AddParameter(string oParam_Name, object value);
        void AddParameter(string oParam_Name, object value, bool IsEmptyNull);
        void AddParameter(string oParam_Name, object value, bool IsEmptyNull, bool IsNullAble);
        void BeginTransaction();
        void ClearParameters();
        string CommandText { get; set; }
        void CommitTransaction();
        bool Connect();
        string ConnectionString { get; set; }
        void Disconnect();
        int ExecuteNonQuery(string sSQL);
        int ExecuteNonQuery(string sSQL, System.Data.CommandType oType);
        System.Data.IDataReader ExecuteReader(string sSQL);
        System.Data.IDataReader ExecuteReader(string sSQL, System.Data.CommandType oType);
        System.Data.IDataReader ExecuteReader(string sSQL, CommandBehavior cmdBehavior);
        System.Data.IDataReader ExecuteReader(string sSQL, System.Data.CommandType oType, CommandBehavior cmdBehavior);
        object ExecuteScalar(string sSQL, System.Data.CommandType oType);
        object ExecuteScalar(string sSQL);
        object ExecuteScalarDefault(string sSQL, object DefaultValue);
        bool FillDataSet(string query, System.Data.DataSet ds);
        DataSet GetDataSet(string query);
        DataSet GetDataSet(string query, System.Data.CommandType commandType);
        DataTable GetSchemaTable(string TableName);
        bool FillDataSet(string query, System.Data.DataSet ds, System.Data.CommandType commandType);
        System.Data.DataTable GetDataTable(string query);
        System.Data.DataTable GetDataTable(string TableName, string fields);
        System.Data.DataTable GetDataTable(string TableName, string fields, bool IsDistinct);
        System.Data.DataTable GetDataTable(string TableName, bool IsDistinct);
        System.Data.DataTable GetDataTable(string TableName, string fields, string Condition);
        System.Data.DataTable GetDataTable(string TableName, string fields, string Condition, bool IsDistinct);
        System.Data.DataRow GetDataRow(string query);
        System.Data.DataRow GetDataRow(string TableName, string fields);
        System.Data.DataRow GetDataRow(string TableName, string fields, string Condition);
        System.Data.DataRow GetDataRow(string TableName, string fields, string Condition, bool IsDistinct);
        System.Data.DataRow GetDataRow(string TableName, string fields, bool IsDistinct);
        object ExecuteScalar(string TableName, string field, string Condition);
        object ExecuteScalar(string TableName, string field);
        System.Data.DataTable GetDataTable(string query, System.Data.CommandType commandType);
        bool IsConnectionOpen { get; }
        bool IsAutoConnectionClose { get; set; }
        bool IsRecordExist(string sSQL);
        bool IsRecordExist(string sSQL, out object value);
        bool IsTransaction { get; }
        void BindControl(System.Web.UI.WebControls.ListControl ddl, string DataTextField, string DataValueField, string query, System.Web.UI.WebControls.ListItem FirstItem);
        void BindControl(System.Web.UI.WebControls.BaseDataBoundControl baseDataBoundControl, string query);
        void BindControl(System.Web.UI.WebControls.ListControl ddl, string DataTextField, string DataValueField, string query);
        void BindControl(System.Web.UI.WebControls.ListControl ddl, string TableName, string DataTextValueField);
        void BindControl(System.Web.UI.WebControls.ListControl ddl, string query, System.Web.UI.WebControls.ListItem FirstItem);
        void BindControlByTable(System.Web.UI.WebControls.ListControl ddl, string TableName, string DataTextField, string DataValueField);
        void BindControlByTable(System.Web.UI.WebControls.ListControl ddl, string TableName, string DataTextField, string DataValueField, System.Web.UI.WebControls.ListItem FirstItem);
        void BindControlByTable(System.Web.UI.WebControls.ListControl ddl, string TableName, string DataTextField);
        void BindControlByTable(System.Web.UI.WebControls.ListControl ddl, string TableName, string DataTextField, System.Web.UI.WebControls.ListItem FirstItem);
        int Delete(string TableName, string Condition);
        //int Add(DataRow obj);
        //int Update(DataRow obj,string ID);
        //int Update(DataRow obj,string ID,string ID2);
        //int Update(DataRow obj,string ID,string ID2,string ID3);

        void RollbackTransaction();

    }
    public partial class OracleQuery : IQuery
    {
        #region Declaration
        protected string connectionString = "";
        protected OracleConnection connection = null;
        protected OracleCommand command = null;
        protected OracleTransaction transaction = null;
        protected OracleDataAdapter dataAdapter = null;
        protected bool autoConnectionClose = true;
        #endregion

        #region Properties
        public string ConnectionString
        {
            get
            {

                return connectionString;
            }
            set
            {
                connectionString = value;
                if (General.GetConnectionStringPart(value, ConnectionStringPart.Provider).Length > 0)
                    connectionString = General.GetOracleConnectionString(value);
                if (connectionString != "")
                {
                    if (connection == null)
                        connection = new OracleConnection(ConnectionString);
                    connection.ConnectionString = connectionString;

                }
            }
        }
        public bool IsConnectionOpen
        {
            get
            {
                if (connection != null && ((connection.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open))
                    return true;
                return false;
            }
        }
        /// <summary>
        /// Close Connection after each command execution 
        /// </summary>
        public bool IsAutoConnectionClose
        {
            get
            {
                return autoConnectionClose;
            }
            set
            {
                autoConnectionClose = value;
            }
        }
        public OracleTransaction Transaction
        {
            get
            {
                if (command == null) return null;
                return command.Transaction;
            }
            set
            {
                transaction = value;
                Command.Transaction = transaction;
            }
        }
        public bool IsTransaction
        {
            get
            {
                if (command == null || command.Transaction == null)
                    return false;
                return true;
            }
        }

        public OracleConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new OracleConnection(ConnectionString);
                    return connection;
                }

                return connection;
            }
            set
            {
                connection = value;
                if (command != null)
                    command.Connection = connection;
            }
        }
        public OracleCommand Command
        {
            get
            {
                if (command == null)
                {
                    command = Connection.CreateCommand();
                }
                return command;
            }
            set
            {
                command = value;
                if (command.Connection != null)
                    connection = command.Connection;
            }
        }
        public OracleDataAdapter DataAdapter
        {
            get
            {
                if (dataAdapter == null)
                {
                    dataAdapter = new OracleDataAdapter(Command);
                }
                return dataAdapter;
            }
            set
            {
                dataAdapter = value;
            }
        }
        public OracleParameterCollection Parameters
        {
            get
            {
                return Command.Parameters;
            }
        }
        public string CommandText
        {
            get
            {
                return Command.CommandText;
            }
            set
            {
                Command.CommandText = value;
            }
        }
        #endregion

        #region constructors
        public OracleQuery(string OracleConnectionString)
        {
            this.ConnectionString = OracleConnectionString;
            //
            // TODO: Add constructor logic here
            //
        }
        public OracleQuery(OracleCommand _command)
        {
            this.connectionString = _command.Connection.ConnectionString;
            this.connection = _command.Connection;
            this.command = _command;
            this.transaction = _command.Transaction;
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        ~OracleQuery()
        {
            if (IsConnectionOpen)
            {
                connection.Close();
                connection = null;
                transaction = null;
                command = null;
            }
        }

        #region Fuctions
        public bool Connect()
        {
            try
            {
                if (!IsConnectionOpen) Connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Close the connection.If Transaction is not committed then rollback the transaction.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                // Disconnect can be called from Dispose and should guarantee no errors

                if (!IsConnectionOpen)
                    return;
                if (IsTransaction)
                    RollbackTransaction();
            }
            catch (Exception ex)
            {
                if (connection != null)
                    connection.Close();
                throw new Exception(ex.Message);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }


        }
        public void BeginTransaction()
        {
            Connect();
            Transaction = Connection.BeginTransaction();
            Command.Transaction = Transaction;
        }
        /// <summary>
        ///  Save changes on database.
        /// </summary>
        public void CommitTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Commit();
                Transaction = null;
            }
        }
        /// <summary>
        /// do not save changes on database.
        /// </summary>
        public void RollbackTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Rollback();
                Transaction = null;
            }
        }
        public virtual DataTable GetSchemaTable(string TableName)
        {

            try
            {
                string query = "select * From " + TableName;
                Command.CommandText = query;
                Connect();
                IDataReader reader = Command.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
                DataTable tableSchema;
                tableSchema = reader.GetSchemaTable();
                reader.Close();
                return tableSchema;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose) Disconnect();
            }
        }
        public DataSet GetDataSet(string query)
        {
            try
            {
                return GetDataSet(query, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public DataSet GetDataSet(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public bool FillDataSet(string query, DataSet ds)
        {
            return FillDataSet(query, ds, CommandType.Text);
        }
        public bool FillDataSet(string query, DataSet ds, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataAdapter.Fill(ds);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public DataRow GetDataRow(string TableName, bool IsDistinct)
        {
            return GetDataRow(TableName, "*", string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields)
        {
            return GetDataRow(TableName, fields, false);
        }
        public DataRow GetDataRow(string TableName, string fields, bool IsDistinct)
        {
            return GetDataRow(TableName, fields, string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition)
        {
            return GetDataRow(TableName, fields, Condition, false);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" TOP 1 {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataRow(query.ToString());

        }
        public DataRow GetDataRow(string query)
        {
            return GetDataRow(query, CommandType.Text);
        }
        public DataRow GetDataRow(string query, CommandType commandType)
        {
            try
            {
                DataTable dt = GetDataTable(query, commandType);
                if (dt.Rows.Count > 0)
                    return dt.Rows[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }
        public DataTable GetDataTable(string TableName, bool IsDistinct)
        {
            return GetDataTable(TableName, "*", string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields)
        {
            return GetDataTable(TableName, fields, false);
        }
        public DataTable GetDataTable(string TableName, string fields, bool IsDistinct)
        {
            return GetDataTable(TableName, fields, string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition)
        {
            return GetDataTable(TableName, fields, Condition, false);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataTable(query.ToString());

        }
        public DataTable GetDataTable(string query)
        {
            return GetDataTable(query, CommandType.Text);
        }


        public DataTable GetDataTable(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseDataBoundControl"></param>
        /// <param name="query"></param>
        public void BindControl(BaseDataBoundControl baseDataBoundControl, string query)
        {
            try
            {
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = "";
                baseDataBoundControl.DataBind();
                Connect();
                CommandText = query;
                IDataReader rd = Command.ExecuteReader();
                if (rd.FieldCount == 0) return;
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = rd;
                if (baseDataBoundControl is ListControl && rd.FieldCount > 0)
                {
                    ListControl lc = (ListControl)baseDataBoundControl;
                    if (lc.DataTextField == "")
                        lc.DataTextField = rd.GetName(0);
                    if (rd.FieldCount > 1 && lc.DataValueField == "")
                        lc.DataValueField = rd.GetName(1);
                }
                baseDataBoundControl.DataBind();
                rd.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query);
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query, ListItem FirstItem)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControl(ListControl ddl, string query, ListItem FirstItem)
        {
            BindControl(ddl, query);
            ddl.Items.Insert(0, FirstItem);
        }
        public void BindControl(ListControl ddl, string TableName, string DataTextValueField)
        {

            string query = "";
            query = "select distinct  " + DataTextValueField + "  from  " + TableName + " order by " + DataTextValueField;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void ExecuteScriptFile(string SriptFilePath)
        {
            FileInfo file = new FileInfo(SriptFilePath);
            string script = file.OpenText().ReadToEnd();
            //Server server = new Server(new ServerConnection(conn));
            //server.ConnectionContext.ExecuteNonQuery(script);
        }
        #region Wraper methods for ADO.NET
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL)
        {
            return this.ExecuteReader(sSQL, CommandType.Text);
        }
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="oType"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandBehavior comBehavior)
        {
            try
            {
                return ExecuteReader(sSQL, CommandType.Text, comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandType oType, CommandBehavior comBehavior)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader(comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public object ExecuteScalar(string TableName, string field)
        {
            return ExecuteScalar(TableName, field, string.Empty);
        }
        public object ExecuteScalar(string TableName, string field, string Condition)
        {
            StringBuilder query = new StringBuilder();
            query.AppendFormat("select {0}  From  {1} ", TableName, field);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteScalar(query.ToString(), CommandType.Text);
        }
        public object ExecuteScalar(string sSQL)
        {
            return ExecuteScalar(sSQL, CommandType.Text);
        }
        public object ExecuteScalarDefault(string sSQL, object DefaultValue)
        {
            object value = ExecuteScalar(sSQL);
            if (value == null || value == System.DBNull.Value)
                value = DefaultValue;
            return value;
        }
        public bool IsRecordExist(string sSQL)
        {
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value) return true;
            return false;
        }
        public bool IsRecordExist(string sSQL, out object value)
        {
            value = null;
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value)
            {
                value = obj;
                return true;
            }
            return false;
        }
        public object ExecuteScalar(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                object obj = Command.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    return obj;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public int ExecuteNonQuery(string sSQL)
        {
            return ExecuteNonQuery(sSQL, CommandType.Text);
        }
        public int ExecuteNonQuery(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void AddParameter(OracleParameter oParam)
        {
            if (oParam.Value == null)
                oParam.Value = DBNull.Value;
            foreach (OracleParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam.ParameterName, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = oParam.Value;
                    return;
                }
            }
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != ':')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = ":" + oParam_Name;
                else oParam_Name = ":" + oParam_Name.Remove(0, 1);
            }
            if (value == null || value.ToString() == DateTime.MinValue.ToString())
                value = DBNull.Value;

            foreach (OracleParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OracleParameter oParam = new OracleParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != ':')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = ":" + oParam_Name;
                else oParam_Name = ":" + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;


            foreach (OracleParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OracleParameter oParam = new OracleParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull, bool IsNullAble)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != ':')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = ":" + oParam_Name;
                else oParam_Name = ":" + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (!IsNullAble)// Null Value is not allowed
            {
                if (value == null || value.ToString() == date.ToString())
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
                else if (IsEmptyNull && value.ToString() == "")
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
            }

            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;

            foreach (OracleParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OracleParameter oParam = new OracleParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void ClearParameters()
        {
            Command.Parameters.Clear();
        }
        public int Delete(string TableName, string Condition)
        {
            StringBuilder query = new StringBuilder("delete");
            query.AppendFormat(" From {0} ", TableName);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteNonQuery(query.ToString());
        }
        #endregion

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Disconnect();
            connection = null;
            command = null;
        }

        #endregion
    }
    public class OleDbQuery : IQuery
    {
        #region Declaration
        protected string connectionString = "";
        protected OleDbConnection connection = null;
        protected OleDbCommand command = null;
        protected OleDbTransaction transaction = null;
        protected OleDbDataAdapter dataAdapter = null;
        protected ProviderType provider = ProviderType.NONE;
        protected bool autoConnectionClose = true;
        #endregion

        #region Properties
        public string ConnectionString
        {
            get
            {
                //if (connection != null)
                //    connectionString = connection.ConnectionString;
                return connectionString;
            }
            set
            {
                connectionString = value;
                if (connectionString != "")
                {
                    if (connection == null)
                        connection = new OleDbConnection(ConnectionString);
                    connection.ConnectionString = connectionString;
                    provider = General.GetProviderType(value);
                }
            }
        }
        public ProviderType Provider
        {
            get
            {
                return provider;
            }
        }
        OleDbTransaction Transaction
        {
            get { return command.Transaction; }
            set
            {
                transaction = value;
                Command.Transaction = transaction;
            }
        }
        public bool IsTransaction
        {
            get
            {
                if (command == null || command.Transaction == null)
                    return false;
                return true;
            }
        }

        public OleDbConnection Connection
        {
            get
            {

                if (connection == null)
                {
                    connection = new OleDbConnection(ConnectionString);
                    return connection;
                }

                return connection;
            }
            set
            {
                connection = value;
                if (command != null)
                    command.Connection = connection;

            }
        }
        public OleDbCommand Command
        {
            get
            {
                if (command == null)
                {
                    command = Connection.CreateCommand();
                }
                return command;
            }
            set
            {
                command = value;
                if (command.Connection != null)
                    connection = command.Connection;
            }
        }
        public bool IsConnectionOpen
        {
            get
            {
                if (connection != null && ((connection.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open))
                    return true;
                return false;
            }
        }
        /// <summary>
        /// Close Connection after each command execution 
        /// </summary>
        public bool IsAutoConnectionClose
        {
            get
            {
                return autoConnectionClose;
            }
            set
            {
                autoConnectionClose = value;
            }
        }
        public OleDbDataAdapter DataAdapter
        {
            get
            {
                if (dataAdapter == null)
                {
                    dataAdapter = new OleDbDataAdapter(Command);
                }
                return dataAdapter;
            }
            set
            {
                dataAdapter = value;
            }
        }
        public OleDbParameterCollection Parameters
        {
            get
            {
                return Command.Parameters;
            }
        }
        public string CommandText
        {
            get
            {
                return Command.CommandText;
            }
            set
            {
                Command.CommandText = value;
            }
        }
        #endregion

        #region constructors
        public OleDbQuery(string oleDbConnectionString)
        {
            this.ConnectionString = oleDbConnectionString;
            //
            // TODO: Add constructor logic here
            //
        }
        public OleDbQuery(string ConnectionString, ProviderType provider)
        {
            this.ConnectionString = General.GetOleDbConnectionString(ConnectionString, provider);
            //
            // TODO: Add constructor logic here
            //
        }
        public OleDbQuery(OleDbCommand _command)
        {
            this.connectionString = _command.Connection.ConnectionString;
            this.connection = _command.Connection;
            this.command = _command;
            this.transaction = _command.Transaction;
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion

        ~OleDbQuery()
        {
            if (IsConnectionOpen)
            {
                connection.Close();
                connection = null;
                transaction = null;
                command = null;
            }
        }

        #region Fuctions
        public bool Connect()
        {
            try
            {
                if (!IsConnectionOpen) Connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Close the connection.If Transaction is not committed then rollback the transaction.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                // Disconnect can be called from Dispose and should guarantee no errors

                if (!IsConnectionOpen)
                    return;
                if (IsTransaction)
                    RollbackTransaction();
            }
            catch (Exception ex)
            {
                if (connection != null)
                    connection.Close();
                throw new Exception(ex.Message);
            }
            finally
            {
                Connection.Close();
            }


        }
        public void BeginTransaction()
        {
            Connect();
            Transaction = Connection.BeginTransaction();
            Command.Transaction = Transaction;
        }
        /// <summary>
        ///  Save changes on database.
        /// </summary>
        public void CommitTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Commit();
                Transaction = null;
            }
        }
        /// <summary>
        /// do not save changes on database.
        /// </summary>
        public void RollbackTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Rollback();
                Transaction = null;
            }
        }
        public virtual DataTable GetSchemaTable(string TableName)
        {

            try
            {
                string query = "select * From " + TableName;
                Command.CommandText = query;
                Connect();
                IDataReader reader = Command.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
                DataTable tableSchema;
                tableSchema = reader.GetSchemaTable();
                reader.Close();
                return tableSchema;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose) Disconnect();
            }
        }
        public DataSet GetDataSet(string query)
        {
            try
            {
                return GetDataSet(query, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public DataSet GetDataSet(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public bool FillDataSet(string query, DataSet ds)
        {
            return FillDataSet(query, ds, CommandType.Text);
        }
        public bool FillDataSet(string query, DataSet ds, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataAdapter.Fill(ds);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public DataRow GetDataRow(string TableName, bool IsDistinct)
        {
            return GetDataRow(TableName, "*", string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields)
        {
            return GetDataRow(TableName, fields, false);
        }
        public DataRow GetDataRow(string TableName, string fields, bool IsDistinct)
        {
            return GetDataRow(TableName, fields, string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition)
        {
            return GetDataRow(TableName, fields, Condition, false);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" TOP 1 {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataRow(query.ToString());

        }
        public DataRow GetDataRow(string query)
        {
            return GetDataRow(query, CommandType.Text);
        }
        public DataRow GetDataRow(string query, CommandType commandType)
        {
            try
            {
                DataTable dt = GetDataTable(query, commandType);
                if (dt.Rows.Count > 0)
                    return dt.Rows[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }
        public DataTable GetDataTable(string TableName, bool IsDistinct)
        {
            return GetDataTable(TableName, "*", string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields)
        {
            return GetDataTable(TableName, fields, false);
        }
        public DataTable GetDataTable(string TableName, string fields, bool IsDistinct)
        {
            return GetDataTable(TableName, fields, string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition)
        {
            return GetDataTable(TableName, fields, Condition, false);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataTable(query.ToString());

        }
        public DataTable GetDataTable(string query)
        {
            return GetDataTable(query, CommandType.Text);
        }


        public DataTable GetDataTable(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseDataBoundControl"></param>
        /// <param name="query"></param>
        public void BindControl(BaseDataBoundControl baseDataBoundControl, string query)
        {
            try
            {
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = "";
                baseDataBoundControl.DataBind();
                Connect();
                CommandText = query;
                IDataReader rd = Command.ExecuteReader();
                if (rd.FieldCount == 0) return;
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = rd;
                if (baseDataBoundControl is ListControl && rd.FieldCount > 0)
                {
                    ListControl lc = (ListControl)baseDataBoundControl;
                    if (lc.DataTextField == "")
                        lc.DataTextField = rd.GetName(0);
                    if (rd.FieldCount > 1 && lc.DataValueField == "")
                        lc.DataValueField = rd.GetName(1);
                }
                baseDataBoundControl.DataBind();
                rd.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query);
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query, ListItem FirstItem)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControl(ListControl ddl, string query, ListItem FirstItem)
        {
            BindControl(ddl, query);
            ddl.Items.Insert(0, FirstItem);
        }
        public void BindControl(ListControl ddl, string TableName, string DataTextValueField)
        {

            string query = "";
            query = "select distinct  " + DataTextValueField + "  from  " + TableName + " order by " + DataTextValueField;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void ExecuteScriptFile(string SriptFilePath)
        {
            FileInfo file = new FileInfo(SriptFilePath);
            string script = file.OpenText().ReadToEnd();
            //Server server = new Server(new ServerConnection(conn));
            //server.ConnectionContext.ExecuteNonQuery(script);
        }
        #region Wraper methods for ADO.NET
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL)
        {
            return this.ExecuteReader(sSQL, CommandType.Text);
        }
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="oType"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandBehavior comBehavior)
        {
            try
            {
                return ExecuteReader(sSQL, CommandType.Text, comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandType oType, CommandBehavior comBehavior)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader(comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public object ExecuteScalar(string TableName, string field)
        {
            return ExecuteScalar(TableName, field, string.Empty);
        }
        public object ExecuteScalar(string TableName, string field, string Condition)
        {
            StringBuilder query = new StringBuilder();
            query.AppendFormat("select {0}  From  {1} ", TableName, field);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteScalar(query.ToString(), CommandType.Text);
        }
        public object ExecuteScalar(string sSQL)
        {
            return ExecuteScalar(sSQL, CommandType.Text);
        }
        public object ExecuteScalarDefault(string sSQL, object DefaultValue)
        {
            object value = ExecuteScalar(sSQL);
            if (value == null || value == System.DBNull.Value)
                value = DefaultValue;
            return value;
        }
        public bool IsRecordExist(string sSQL)
        {
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value) return true;
            return false;
        }
        public bool IsRecordExist(string sSQL, out object value)
        {
            value = null;
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value)
            {
                value = obj;
                return true;
            }
            return false;
        }
        public object ExecuteScalar(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                object obj = Command.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    return obj;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public int ExecuteNonQuery(string sSQL)
        {
            return ExecuteNonQuery(sSQL, CommandType.Text);
        }
        public int ExecuteNonQuery(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void AddParameter(OleDbParameter oParam)
        {
            if (oParam.Value == null)
                oParam.Value = DBNull.Value;

            foreach (OleDbParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam.ParameterName, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = oParam.Value;
                    return;
                }
            }
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value)
        {
            oParam_Name = oParam_Name.Trim();
            if (value == null || value.ToString() == DateTime.MinValue.ToString())
                value = DBNull.Value;
            foreach (OleDbParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OleDbParameter oParam = new OleDbParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull)
        {
            oParam_Name = oParam_Name.Trim();
            DateTime date = new DateTime();
            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;


            foreach (OleDbParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OleDbParameter oParam = new OleDbParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull, bool IsNullAble)
        {
            oParam_Name = oParam_Name.Trim();
            DateTime date = new DateTime();
            if (!IsNullAble)// Null Value is not allowed
            {
                if (value == null || value.ToString() == date.ToString())
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
                else if (IsEmptyNull && value.ToString() == "")
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
            }

            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;

            foreach (OleDbParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            OleDbParameter oParam = new OleDbParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void ClearParameters()
        {
            Command.Parameters.Clear();
        }
        public int Delete(string TableName, string Condition)
        {
            StringBuilder query = new StringBuilder("delete");
            query.AppendFormat(" From {0} ", TableName);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteNonQuery(query.ToString());
        }
        #endregion

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Disconnect();
            connection = null;
            command = null;
        }

        #endregion
    }
    public partial class SqlQuery : IQuery
    {
        #region Declaration
        protected string connectionString = "";
        protected SqlConnection connection = null;
        protected SqlCommand command = null;
        protected SqlTransaction transaction = null;
        protected SqlDataAdapter dataAdapter = null;

        protected bool autoConnectionClose = true;
        #endregion

        #region Properties
        public string ConnectionString
        {
            get
            {
                return connectionString;
            }
            set
            {
                connectionString = value;
                if (General.GetConnectionStringPart(value, ConnectionStringPart.Provider).Length > 0)
                    connectionString = General.GetSqlConnectionString(value);
                if (connectionString != "")
                {
                    if (connection == null)
                        connection = new SqlConnection(ConnectionString);
                    connection.ConnectionString = connectionString;

                }
            }
        }
        public bool IsConnectionOpen
        {
            get
            {
                if (connection != null && ((connection.State & System.Data.ConnectionState.Open) == System.Data.ConnectionState.Open))
                    return true;
                return false;
            }
        }
        /// <summary>
        /// Close Connection after each command execution 
        /// </summary>
        public bool IsAutoConnectionClose
        {
            get
            {
                return autoConnectionClose;
            }
            set
            {
                autoConnectionClose = value;
            }
        }
        public SqlTransaction Transaction
        {
            get { return command.Transaction; }
            set
            {
                transaction = value;
                Command.Transaction = transaction;
            }
        }
        public bool IsTransaction
        {
            get
            {
                if (command.Transaction == null)
                    return false;
                return true;
            }
        }
        public SqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new SqlConnection(ConnectionString);
                    return connection;
                }

                return connection;
            }
            set
            {
                connection = value;
                if (command != null)
                    command.Connection = connection;
            }
        }
        public SqlCommand Command
        {
            get
            {
                if (command == null)
                {
                    command = Connection.CreateCommand();
                }
                return command;
            }
            set
            {
                command = value;
                if (command.Connection != null)
                    connection = command.Connection;
            }
        }
        public SqlDataAdapter DataAdapter
        {
            get
            {
                if (dataAdapter == null)
                {
                    dataAdapter = new SqlDataAdapter(Command);
                }
                return dataAdapter;
            }
            set
            {
                dataAdapter = value;
            }
        }
        public SqlParameterCollection Parameters
        {
            get
            {
                return Command.Parameters;
            }
        }
        public string CommandText
        {
            get
            {
                return Command.CommandText;
            }
            set
            {
                Command.CommandText = value;
            }
        }
        #endregion

        #region constructors
        public SqlQuery(string SqlConnectionString)
        {
            this.ConnectionString = SqlConnectionString;
        }
        public SqlQuery(SqlCommand _command)
        {
            this.connection = _command.Connection;
            this.command = _command;
            this.transaction = _command.Transaction;
            //
            // TODO: Add constructor logic here
            //
        }
        public SqlQuery(SqlConnection _connection)
        {
            Connection = _connection;
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion

        #region
        ~SqlQuery()
        {
            if (IsConnectionOpen)
            {
                connection.Close();
                connection = null;
                transaction = null;
                command = null;
            }
        }
        #endregion

        #region Fuctions
        public bool Connect()
        {
            try
            {
                if (!IsConnectionOpen) Connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Close the connection.If Transaction is not committed then rollback the transaction.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                // Disconnect can be called from Dispose and should guarantee no errors

                if (!IsConnectionOpen)
                    return;
                if (IsTransaction)
                    RollbackTransaction();
                if (IsConnectionOpen)
                    connection.Close();
            }
            catch (Exception ex)
            {
                if (connection != null)
                    connection.Close();
                throw new Exception(ex.Message);
            }
            finally
            {
                Connection.Close();
            }


        }
        public void BeginTransaction()
        {
            Connect();
            Transaction = Connection.BeginTransaction();
            Command.Transaction = Transaction;
        }
        /// <summary>
        ///  Save changes on database.
        /// </summary>
        public void CommitTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Commit();
                Transaction = null;
            }
        }
        /// <summary>
        /// do not save changes on database.
        /// </summary>
        public void RollbackTransaction()
        {
            if (IsTransaction)
            {
                Transaction.Rollback();
                Transaction = null;
            }
        }
        public virtual DataTable GetUserTables()
        {
            try
            {
                string query = "SELECT name FROM sysobjects WHERE  (xtype = 'U') order by Name";
                return GetDataTable(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public virtual DataTable GetSchemaTable(string TableName)
        {

            try
            {
                string query = "select * From " + TableName;
                Command.CommandText = query;
                Connect();
                IDataReader reader = Command.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
                DataTable tableSchema;
                tableSchema = reader.GetSchemaTable();
                reader.Close();
                return tableSchema;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose) Disconnect();
            }
        }
        public DataSet GetDataSet(string query)
        {
            try
            {
                return GetDataSet(query, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public DataSet GetDataSet(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();

            }
        }
        public bool FillDataSet(string query, DataSet ds)
        {
            try
            {
                return FillDataSet(query, ds, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool FillDataSet(string query, DataSet ds, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataAdapter.Fill(ds);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public DataRow GetDataRow(string TableName, bool IsDistinct)
        {
            return GetDataRow(TableName, "*", string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields)
        {
            return GetDataRow(TableName, fields, false);
        }
        public DataRow GetDataRow(string TableName, string fields, bool IsDistinct)
        {
            return GetDataRow(TableName, fields, string.Empty, IsDistinct);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition)
        {
            return GetDataRow(TableName, fields, Condition, false);
        }
        public DataRow GetDataRow(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" TOP 1 {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataRow(query.ToString());

        }
        public DataRow GetDataRow(string query)
        {
            return GetDataRow(query, CommandType.Text);
        }
        public DataRow GetDataRow(string query, CommandType commandType)
        {
            try
            {
                DataTable dt = GetDataTable(query, commandType);
                if (dt.Rows.Count > 0)
                    return dt.Rows[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }
        public DataTable GetDataTable(string TableName, bool IsDistinct)
        {
            return GetDataTable(TableName, "*", string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields)
        {
            return GetDataTable(TableName, fields, false);
        }
        public DataTable GetDataTable(string TableName, string fields, bool IsDistinct)
        {
            return GetDataTable(TableName, fields, string.Empty, IsDistinct);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition)
        {
            return GetDataTable(TableName, fields, Condition, false);
        }
        public DataTable GetDataTable(string TableName, string fields, string Condition, bool IsDistinct)
        {
            StringBuilder query = new StringBuilder("select");
            if (IsDistinct)
            {
                query.AppendFormat(" distinct ");
            }
            query.AppendFormat(" {0}  From  {1} ", TableName, fields);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return GetDataTable(query.ToString());

        }
        public DataTable GetDataTable(string query)
        {
            return GetDataTable(query, CommandType.Text);
        }


        public DataTable GetDataTable(string query, CommandType commandType)
        {
            try
            {
                CommandText = query;
                Command.CommandType = commandType;
                DataSet ds = new DataSet();
                DataAdapter.Fill(ds);
                if (ds.Tables.Count > 0)
                    return ds.Tables[0];
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction)
                    Disconnect();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseDataBoundControl"></param>
        /// <param name="query"></param>
        public void BindControl(BaseDataBoundControl baseDataBoundControl, string query)
        {
            try
            {
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = "";
                baseDataBoundControl.DataBind();
                Connect();
                CommandText = query;
                IDataReader rd = Command.ExecuteReader();
                if (rd.FieldCount == 0) return;
                baseDataBoundControl.DataSourceID = "";
                baseDataBoundControl.DataSource = rd;
                if (baseDataBoundControl is ListControl && rd.FieldCount > 0)
                {
                    ListControl lc = (ListControl)baseDataBoundControl;
                    if (lc.DataTextField == "")
                        lc.DataTextField = rd.GetName(0);
                    if (rd.FieldCount > 1 && lc.DataValueField == "")
                        lc.DataValueField = rd.GetName(1);
                }
                baseDataBoundControl.DataBind();
                rd.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query);
        }
        public void BindControl(ListControl ddl, string DataTextField, string DataValueField, string query, ListItem FirstItem)
        {
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControl(ListControl ddl, string query, ListItem FirstItem)
        {
            BindControl(ddl, query);
            ddl.Items.Insert(0, FirstItem);
        }
        public void BindControl(ListControl ddl, string TableName, string DataTextValueField)
        {

            string query = "";
            query = "select distinct  " + DataTextValueField + "  from  " + TableName + " order by " + DataTextValueField;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, string DataValueField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataValueField;
            if (DataTextField.Equals(DataValueField, StringComparison.OrdinalIgnoreCase))
            {
                query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            }
            else
                query = "select distinct " + DataTextField + "," + DataValueField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query);
        }
        public void BindControlByTable(ListControl ddl, string TableName, string DataTextField, ListItem FirstItem)
        {
            string query = "";
            ddl.DataTextField = DataTextField;
            ddl.DataValueField = DataTextField;
            query = "select distinct  " + DataTextField + "  from  " + TableName + " order by " + DataTextField; ;
            BindControl(ddl, query, FirstItem);
        }
        public void ExecuteScriptFile(string SriptFilePath)
        {
            FileInfo file = new FileInfo(SriptFilePath);
            string script = file.OpenText().ReadToEnd();
            //Server server = new Server(new ServerConnection(conn));
            //server.ConnectionContext.ExecuteNonQuery(script);
        }
        #region Wraper methods for ADO.NET
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL)
        {
            return this.ExecuteReader(sSQL, CommandType.Text);
        }
        /// <summary>
        /// Open connection before calling this method.Other this method open a connection 
        /// but do not close connection.
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="oType"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandBehavior comBehavior)
        {
            try
            {
                return ExecuteReader(sSQL, CommandType.Text, comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public IDataReader ExecuteReader(string sSQL, CommandType oType, CommandBehavior comBehavior)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteReader(comBehavior);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
        }
        public object ExecuteScalar(string TableName, string field)
        {
            return ExecuteScalar(TableName, field, string.Empty);
        }
        public object ExecuteScalar(string TableName, string field, string Condition)
        {
            StringBuilder query = new StringBuilder();
            query.AppendFormat("select {0}  From  {1} ", TableName, field);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteScalar(query.ToString(), CommandType.Text);
        }
        public object ExecuteScalar(string sSQL)
        {
            return ExecuteScalar(sSQL, CommandType.Text);
        }
        public object ExecuteScalarDefault(string sSQL, object DefaultValue)
        {
            object value = ExecuteScalar(sSQL);
            if (value == null || value == System.DBNull.Value)
                value = DefaultValue;
            return value;
        }
        public bool IsRecordExist(string sSQL)
        {
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value) return true;
            return false;
        }
        public bool IsRecordExist(string sSQL, out object value)
        {
            value = null;
            object obj = ExecuteScalar(sSQL, CommandType.Text);
            if (obj != null && obj != System.DBNull.Value)
            {
                value = obj;
                return true;
            }
            return false;
        }
        public object ExecuteScalar(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                object obj = Command.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    return obj;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public int ExecuteNonQuery(string sSQL)
        {
            return ExecuteNonQuery(sSQL, CommandType.Text);
        }
        public int ExecuteNonQuery(string sSQL, CommandType oType)
        {
            try
            {
                Connect();
                CommandText = sSQL;
                Command.CommandType = oType;
                return Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (!IsTransaction && IsAutoConnectionClose)
                    Disconnect();
            }
        }
        public void AddParameter(SqlParameter oParam)
        {
            if (oParam.Value == null)
                oParam.Value = DBNull.Value;
            foreach (SqlParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam.ParameterName, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = oParam.Value;
                    return;
                }
            }
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != '@')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = "@" + oParam_Name;
                else oParam_Name = "@" + oParam_Name.Remove(0, 1);
            }
            if (value == null || value.ToString() == DateTime.MinValue.ToString())
                value = DBNull.Value;

            foreach (SqlParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            SqlParameter oParam = new SqlParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != '@')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = "@" + oParam_Name;
                else oParam_Name = "@" + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;


            foreach (SqlParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            SqlParameter oParam = new SqlParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void AddParameter(string oParam_Name, object value, bool IsEmptyNull, bool IsNullAble)
        {
            oParam_Name = oParam_Name.Trim();
            if (oParam_Name.Length > 0 && oParam_Name[0] != '@')
            {
                if (char.IsLetterOrDigit(oParam_Name[0]))
                    oParam_Name = "@" + oParam_Name;
                else oParam_Name = "@" + oParam_Name.Remove(0, 1);
            }
            DateTime date = new DateTime();
            if (!IsNullAble)// Null Value is not allowed
            {
                if (value == null || value.ToString() == date.ToString())
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
                else if (IsEmptyNull && value.ToString() == "")
                {
                    throw new Exception("Invalid " + oParam_Name + "!");
                }
            }

            if (value == null || value.ToString() == date.ToString())
                value = DBNull.Value;
            else if (IsEmptyNull && value.ToString() == "")
                value = DBNull.Value;

            foreach (SqlParameter par in Command.Parameters)
            {
                if (par.ParameterName.Equals(oParam_Name, StringComparison.OrdinalIgnoreCase))
                {
                    par.Value = value;
                    return;
                }
            }
            SqlParameter oParam = new SqlParameter(oParam_Name, value);
            Command.Parameters.Add(oParam);
        }
        public void ClearParameters()
        {
            Command.Parameters.Clear();
        }
        public int Delete(string TableName, string Condition)
        {
            StringBuilder query = new StringBuilder("delete");
            query.AppendFormat(" From {0} ", TableName);
            if (Condition != null && Condition != string.Empty)
            {
                query.AppendFormat(" where ({0})", Condition);
            }
            return ExecuteNonQuery(query.ToString());
        }
        #endregion


        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Disconnect();
            connection = null;
            command = null;
        }

        #endregion
    }
    public partial class General
    {
        public static string ServerPath
        {
            get
            {
                Page objPage = new Page();
                return objPage.Server.MapPath("~/");

            }
        }
        public void ClearApplicationCache(System.Web.Caching.Cache Cache)
        {

            List<string> keys = new List<string>();



            // retrieve application Cache enumerator

            IDictionaryEnumerator enumerator = Cache.GetEnumerator();



            // copy all keys that currently exist in Cache

            while (enumerator.MoveNext())
            {

                keys.Add(enumerator.Key.ToString());

            }



            // delete every key from cache

            for (int i = 0; i < keys.Count; i++)
            {

                Cache.Remove(keys[i]);

            }

        }


        /// <summary>
        /// dd-mm-yyyy  (en-GB)
        /// </summary>
        public static IFormatProvider DateTimeFormat
        {
            get
            {
                return new CultureInfo("en-GB", true);
            }
        }
        public static string GetDayName(System.DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Friday:
                    return "Fri";
                case DayOfWeek.Monday:
                    return "Mon";
                case DayOfWeek.Saturday:
                    return "Sat";
                case DayOfWeek.Sunday:
                    return "Sun";
                case DayOfWeek.Thursday:
                    return "Thu";
                case DayOfWeek.Tuesday:
                    return "Tue";
                case DayOfWeek.Wednesday:
                    return "Wed";
            }
            return "";
        }
        public static object GetDBValue(object value, object defaultValue)
        {
            if (value == null || value == DBNull.Value) return defaultValue;
            return value;
        }
        public static object GetDBValue(object value)
        {
            if (value == null || value == DBNull.Value) return null;
            return value;
        }
        public static object GetDRValue(IDataReader dr, string Param_Name, object DefaultValue)
        {
            object value = DefaultValue;
            try
            {
                string parName = Param_Name;
                if (Param_Name.Length > 0 && char.IsPunctuation(Param_Name[0]))
                    parName = Param_Name.Remove(0, 1);
                value = dr[parName];
                if (value == null || value == DBNull.Value) return DefaultValue;
                return value;
            }
            catch (Exception ex)
            {
                return DefaultValue;
            }
        }
        public static object GetDRValue(IDataReader dr, string Param_Name)
        {
            object value = null;
            try
            {
                string parName = Param_Name;
                if (Param_Name.Length > 0 && char.IsPunctuation(Param_Name[0]))
                    parName = Param_Name.Remove(0, 1);
                value = dr[parName];
                if (value == null || value == DBNull.Value) return null;
                return value;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string GetMonthShortName(int Month)
        {
            switch (Month)
            {
                case 1:
                    return "Jan.";
                case 2:
                    return "Feb.";
                case 3:
                    return "Mar.";
                case 4:
                    return "Apr.";
                case 5:
                    return "May.";
                case 6:
                    return "Jun.";
                case 7:
                    return "Jul.";
                case 8:
                    return "Aug.";
                case 9:
                    return "Sep.";
                case 10:
                    return "Oct.";
                case 11:
                    return "Nov.";
                case 12:
                    return "Dec.";
            }
            return "";
        }
        public static string GetMonthFullName(int Month)
        {
            switch (Month)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
            }
            return "";
        }
        /// <summary>
        /// 24 hrs format (0000,2359)
        /// </summary>
        /// <param name="Time"></param>
        /// <returns></returns>
        public static bool IsValidTime(uint Time)
        {
            try
            {
                if (Time > 2359 && Time < 0)
                    return false;
                if (Time < 60)
                    return true;
                string value = Time.ToString();
                if (value.Length == 2)
                    return false;

                if (value.Length > 2 || value.Length < 5)
                {
                    string lastTwo = value.Substring(value.Length - 2, 2);
                    if (int.Parse(lastTwo) > 59)
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        ///  24 hrs format (0000,2359)
        /// </summary>
        /// <param name="Time"></param>
        /// <returns></returns>
        public static string TimeFormat(uint Time)
        {
            try
            {
                if (!IsValidTime(Time)) return "";
                string strTime = Time.ToString();
                if (strTime.Length == 1)
                    strTime = "000" + strTime;
                else if (strTime.Length == 2)
                    strTime = "00" + strTime;
                else if (strTime.Length == 3)
                    strTime = "0" + strTime;
                return strTime;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /// <summary>
        ///  24 hrs format (00:00,23:59)
        /// </summary>
        /// <param name="Time"></param>
        /// <returns></returns>
        public static string TimeFormatColon(uint Time)
        {
            try
            {
                if (!IsValidTime(Time)) return "";
                string strTime = Time.ToString();
                if (strTime.Length == 1)
                    strTime = "00:0" + strTime;
                else if (strTime.Length == 2)
                    strTime = "00:" + strTime;
                else if (strTime.Length == 3)
                {
                    strTime = "0" + strTime[0] + ":" + strTime[1] + strTime[3];
                }
                return strTime;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static bool IsNumeric(string theValue)
        {
            try
            {
                if (theValue.Trim().Length == 0) return false;
                string numeric = "1234567890.";
                bool IsFirstPoint = true;
                for (int i = 0; i < theValue.Length; i++)
                {
                    char ch = theValue[i];
                    if (i == 0 && (ch == '+' || ch == '-')) continue;
                    if (ch == '.')
                    {
                        if (IsFirstPoint)
                        {
                            IsFirstPoint = false;
                            continue;
                        }
                        else return false;
                    }
                    else
                    {
                        if (numeric.IndexOf(ch) >= 0) continue;
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 1234567890
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static bool IsNumber(string theValue)//IsNumber
        {
            try
            {
                if (theValue.Trim().Length == 0) return false;
                string numeric = "1234567890";
                for (int i = 0; i < theValue.Length; i++)
                {
                    char ch = theValue[i];
                    if (numeric.IndexOf(ch) >= 0) continue;
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 1234567890-+
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static bool IsInteger(string theValue)//IsInteger
        {
            try
            {
                if (theValue.Trim().Length == 0) return false;
                string numeric = "1234567890";
                for (int i = 0; i < theValue.Length; i++)
                {
                    char ch = theValue[i];
                    if (i == 0 && (ch == '+' || ch == '-')) continue;
                    if (numeric.IndexOf(ch) >= 0) continue;
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Get Value of Connection String Part.
        /// </summary>
        /// <param name="para">DataSource, UserID, Password, Database, Provider</param>
        /// <returns></returns>
        public static string GetConnectionStringPart(string connectionString, ConnectionStringPart para)
        {
            string[] split = connectionString.Split(';');
            string TargetString = "";

            for (int i = 0; i < split.Length; i++)
            {
                string[] tvalue;
                if (para == ConnectionStringPart.DataSource)
                {
                    if (split[i].StartsWith("Data Source", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Server", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString = tvalue[1];
                        break;
                    }
                }

                if (para == ConnectionStringPart.UserID)
                {
                    if (split[i].StartsWith("User ID", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("uid", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString = tvalue[1];
                        break;
                    }
                }

                if (para == ConnectionStringPart.Database)
                {
                    if (split[i].StartsWith("Initial Catalog", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Database", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString = tvalue[1];
                        break;
                    }
                }
                if (para == ConnectionStringPart.Password)
                {
                    if (split[i].StartsWith("Password", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("pwd", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString = tvalue[1];
                        break;
                    }
                }
                if (para == ConnectionStringPart.Provider)
                {
                    if (split[i].StartsWith("Provider", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString = tvalue[1];
                        break;
                    }
                }
            }

            return TargetString;
        }
        public static string GetSqlConnectionString(string OleDbConnectionString)
        {
            string ConnectionString = OleDbConnectionString;
            string[] split = ConnectionString.Split(';');
            string TargetString = "";

            for (int i = 0; i < split.Length; i++)
            {
                string[] tvalue;
                split[i] = split[i].TrimStart();
                if (split[i].StartsWith("Data Source", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Server", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += "Server = " + tvalue[1] + ";";
                }

                if (split[i].StartsWith("User ID", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("uid", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += "uid = " + tvalue[1] + ";";
                }

                if (split[i].StartsWith("Initial Catalog", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Database", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += "Database = " + tvalue[1] + ";";
                }

                if (split[i].StartsWith("Password", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("pwd", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += "pwd = " + tvalue[1] + ";";
                }
            }

            return TargetString;
        }
        public static string GetOleDbConnectionString(string ConnectionString, ProviderType provider)
        {
            string[] split = ConnectionString.Split(';');
            string TargetString = "";
            if (provider == ProviderType.SQLCLIENT)
                TargetString = "Provider=SQLOLEDB.1;Persist Security Info=False;";
            else if (provider == ProviderType.ORACLE)
                TargetString = "Provider=SMD;Persist Security Info=False;";
            else if (provider == ProviderType.MSACCESS)
                TargetString = "Provider=Microsoft.Jet.OLEDB.4.0;Persist Security Info=False;";

            for (int i = 0; i < split.Length; i++)
            {
                string[] tvalue;
                if (split[i].StartsWith("Data Source", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Server", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += " Data Source = " + tvalue[1] + ";";
                }

                if (split[i].StartsWith("User ID", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("uid", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += " User ID = " + tvalue[1] + ";";
                }
                if (provider == ProviderType.SQLCLIENT)
                    if (split[i].StartsWith("Initial Catalog", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Database", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tvalue = split[i].Split('=');
                        TargetString += " Initial Catalog = " + tvalue[1] + ";";
                    }
                if (split[i].StartsWith("Password", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("pwd", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += " Password = " + tvalue[1] + ";";
                }
            }

            return TargetString;
        }
        public static string GetOracleConnectionString(string OleDbConnectionString)
        {
            string ConnectionString = OleDbConnectionString;
            string[] split = ConnectionString.Split(';');
            string TargetString = "";
            for (int i = 0; i < split.Length; i++)
            {
                string[] tvalue;
                if (split[i].StartsWith("Data Source", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("Server", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += " Data Source =" + tvalue[1] + ";";
                }

                if (split[i].StartsWith("User ID", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("uid", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += " User Id= " + tvalue[1] + ";";
                }
                if (split[i].StartsWith("Password", StringComparison.CurrentCultureIgnoreCase) || split[i].StartsWith("pwd", StringComparison.CurrentCultureIgnoreCase))
                {
                    tvalue = split[i].Split('=');
                    TargetString += "Password=" + tvalue[1] + ";";
                }
            }

            return TargetString;
        }
        public static ProviderType GetProviderType(string OleDbConnectionString)
        {
            string provider = GetConnectionStringPart(OleDbConnectionString, ConnectionStringPart.Provider);
            if (provider.StartsWith("SQL", StringComparison.OrdinalIgnoreCase))
                return ProviderType.SQLCLIENT;
            if (provider.StartsWith("OleDb", StringComparison.OrdinalIgnoreCase))
                return ProviderType.OLEDB;
            if (provider.StartsWith("Md", StringComparison.OrdinalIgnoreCase))
                return ProviderType.ORACLE;
            if (provider.StartsWith("MSAccess", StringComparison.OrdinalIgnoreCase))
                return ProviderType.MSACCESS;
            return ProviderType.NONE;
        }
        public static bool SetPrimaryKey(DataTable FilledTable, string colName)
        {
            try
            {
                DataColumn[] dc = new DataColumn[1];
                dc[0] = FilledTable.Columns[colName];
                if (dc[0] != null)
                {
                    FilledTable.PrimaryKey = dc;
                    return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Comparison operators
        /// </summary>
        /// <param name="LCOperators"></param>
        //public static void LoadOperators(ListControl LCOperators)
        //{

        //    LCOperators.Items.Clear();
        //    LCOperators.Items.Add("=");
        //    LCOperators.Items.Add(">");
        //    LCOperators.Items.Add("<");
        //    LCOperators.Items.Add(System.Convert.ToChar((int)Operators.GreaterEqual).ToString());
        //    LCOperators.Items.Add(System.Convert.ToChar((int)Operators.LessEqual).ToString());
        //    LCOperators.Items.Add(System.Convert.ToChar((int)Operators.NotEqual).ToString());
        //}
        public static bool SelectWizardStep(Wizard objWizard, string WizardStepID)
        {
            IEnumerator IENum = objWizard.WizardSteps.GetEnumerator();
            while (IENum.MoveNext())
            {
                WizardStepBase WSB = (WizardStepBase)IENum.Current;
                if (WSB.ID == WizardStepID)
                {
                    objWizard.MoveTo(WSB);
                    return true;
                }
            }
            return false;
        }
        public static DateTime GetDateTime(String dd_MM_YYYY)
        {
            DateTime newDateTime = DateTime.Now;
            try
            {
                if (dd_MM_YYYY.Length == 0) return newDateTime;
                return DateTime.Parse(dd_MM_YYYY, DateTimeFormat);
            }
            catch (Exception ex)
            {
                return newDateTime;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DropDownList_ListBox"></param>
        /// <param name="value"></param>
        /// <param name="IgnoreCase"></param>
        /// <returns></returns>
        public static bool SelectItemByValue(ListControl DropDownList_ListBox, string value, bool IgnoreCase)
        {
            ListControl lc = DropDownList_ListBox;
            value = value.Trim();
            lc.SelectedIndex = -1;
            if (!IgnoreCase)
            {
                ListItem objListItem = lc.Items.FindByValue(value);
                if (objListItem != null)
                {
                    objListItem.Selected = true;
                    return true;
                }
            }
            else
            {
                foreach (ListItem item in lc.Items)
                {
                    if (item.Value.Equals(value, StringComparison.OrdinalIgnoreCase))
                    {
                        item.Selected = true;
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Compare strings using ordinal sort rules and ignoring the case of the strings being compared.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool SelectItemByValue(ListControl DropDownList_ListBox, string value)
        {
            ListControl ddl = DropDownList_ListBox;
            value = value.Trim();
            ddl.SelectedIndex = -1;
            foreach (ListItem objListItem in ddl.Items)
            {
                if (objListItem.Value.Equals(value, StringComparison.OrdinalIgnoreCase))
                {
                    objListItem.Selected = true;
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// Compare strings using ordinal sort rules and ignoring the case of the strings being compared.
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="Text"></param>
        /// <returns></returns>
        public static bool SelectItemByText(ListControl DropDownList_ListBox, string Text)
        {
            ListControl ddl = DropDownList_ListBox;
            Text = Text.Trim();
            ddl.SelectedIndex = -1;
            foreach (ListItem objListItem in ddl.Items)
            {
                if (objListItem.Text.Equals(Text, StringComparison.OrdinalIgnoreCase))
                {
                    objListItem.Selected = true;
                    return true;
                }
            }
            return false;
        }
        public static bool SelectItemByText(ListControl DropDownList_ListBox, string text, bool IgnoreCase)
        {
            ListControl lc = DropDownList_ListBox;
            text = text.Trim();
            lc.SelectedIndex = -1;
            if (!IgnoreCase)
            {
                ListItem objListItem = lc.Items.FindByText(text);
                if (objListItem != null)
                {
                    objListItem.Selected = true;
                    return true;
                }
            }
            else
            {
                foreach (ListItem item in lc.Items)
                {
                    if (item.Text.Equals(text, StringComparison.OrdinalIgnoreCase))
                    {
                        item.Selected = true;
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Get Numeric Guid
        /// </summary>
        public static string ID
        {
            get
            {
                String temp = Guid.NewGuid().ToString().Substring(0, 8);
                return int.Parse(temp, System.Globalization.NumberStyles.HexNumber).ToString();
            }
        }
        /// <summary>
        /// Get Current User ID with out domain name.
        /// </summary>
        /// <param name="UserIdentity">HttpContext.Current.User.Identity.Name</param>
        /// <returns> User ID</returns>
        public static string GetUserIdentity(string UserIdentity)
        {
            try
            {

                string[] strArr = UserIdentity.Split(new char[] { '\\' });
                if (strArr.GetLength(0) > 1)
                {
                    return strArr[1];
                }
                else
                    return UserIdentity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static string GetUserIdentity(HttpRequest Request)
        {
            try
            {
                string userid = "";
                if (!HttpContext.Current.User.Identity.IsAuthenticated) // Gets a value that indicates whether the user has been authenticated - namespace of "System.Web.Security;"
                {
                    return userid;
                }
                if (Request.QueryString["user"] == null)
                {
                    userid = GetUserIdentity(HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    userid = Request.QueryString["user"];
                }
                return userid;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void ClearControls(Control Container)
        {
            try
            {
                foreach (Control con in Container.Controls)
                {
                    if (con is ListControl)
                    {
                        ((ListControl)con).SelectedIndex = -1;
                    }
                    else if (con is TextBox)
                    {
                        ((TextBox)con).Text = "";
                    }
                    else if (con is HtmlInputText)
                    {
                        ((HtmlInputText)con).Value = "";
                    }
                    else if (con is GridView)
                    {
                        ((GridView)con).SelectedIndex = -1;
                    }
                    ClearControls(con);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void Load(ListControl ddl, DataTable dt, string TextField, string ValueField, ListItem FirstItem)
        {
            try
            {
                ddl.Items.Clear();
                ddl.DataTextField = TextField;
                ddl.DataValueField = ValueField;
                ddl.DataSourceID = "";
                ddl.DataSource = dt.DefaultView;
                ddl.DataBind();
                if (FirstItem != null)
                {
                    ddl.Items.Insert(0, FirstItem);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public static void Copy(ListControl ddl_From, ListControl ddl_To)
        {
            try
            {
                ddl_To.Items.Clear();
                for (int i = 0; i < ddl_From.Items.Count; i++)
                {
                    ddl_To.Items.Add(new ListItem(ddl_From.Items[i].Text, ddl_From.Items[i].Value));
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }