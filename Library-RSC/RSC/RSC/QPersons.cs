using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QPersons:Query
	{

		public const string TABLE_NAME = "Persons";
		public const string P_Id = "Id";
		public const string P_First_Name = "First_Name";
		public const string P_Middle_Name = "Middle_Name";
		public const string P_Last_Name = "Last_Name";
		public const string P_Perm_Address_1 = "Perm_Address_1";
		public const string P_Perm_Address_2 = "Perm_Address_2";
		public const string P_Perm_City = "Perm_City";
		public const string P_Perm_Provice = "Perm_Provice";
		public const string P_Perm_Id_Country = "Perm_Id_Country";
		public const string P_Sex = "Sex";
		public const string P_CNIC = "CNIC";
		public const string P_Landline_No = "Landline_No";
		public const string P_Cell_No = "Cell_No";
		public const string P_Occupation_Profession = "Occupation_Profession";
		public const string P_Photo = "Photo";
		public const string P_Id_Relative = "Id_Relative";
		public const string P_Relation = "Relation";
		public const string P_Type = "Type";
		public const string P_Alive = "Alive";
		public const string SelectColumns=@"Id,First_Name,Middle_Name,Last_Name,Perm_Address_1,Perm_Address_2,Perm_City,Perm_Provice,Perm_Id_Country,
						Sex,CNIC,Landline_No,Cell_No,Occupation_Profession,Photo,Id_Relative,Relation,
						Type,Alive";
		public const string QSelect=@"Select * From Persons  order by  First_Name";
		public const string QSelectById=@"Select * From Persons 
								 where Id = @Id  order by  First_Name";
		public const string QInsert=@"Insert Into  Persons (First_Name,Middle_Name,Last_Name,Perm_Address_1,Perm_Address_2,Perm_City,Perm_Provice,Perm_Id_Country,
						Sex,CNIC,Landline_No,Cell_No,Occupation_Profession,Photo,Id_Relative,Relation,
						Type,Alive) 
								 values(@First_Name,@Middle_Name,@Last_Name,@Perm_Address_1,@Perm_Address_2,@Perm_City,@Perm_Provice,@Perm_Id_Country,
						@Sex,@CNIC,@Landline_No,@Cell_No,@Occupation_Profession,@Photo,@Id_Relative,@Relation,
						@Type,@Alive)";
		public const string QUpdateById=@"Update Persons Set First_Name=@First_Name,Middle_Name=@Middle_Name,Last_Name=@Last_Name,Perm_Address_1=@Perm_Address_1,
						Perm_Address_2=@Perm_Address_2,Perm_City=@Perm_City,Perm_Provice=@Perm_Provice,Perm_Id_Country=@Perm_Id_Country,
						Sex=@Sex,CNIC=@CNIC,Landline_No=@Landline_No,Cell_No=@Cell_No,
						Occupation_Profession=@Occupation_Profession,Photo=@Photo,Id_Relative=@Id_Relative,Relation=@Relation,
						Type=@Type,Alive=@Alive 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Persons 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Persons 
								 where Id = @Id";

		public QPersons(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QPersons(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QPersons(IDbConnection Connection):base(Connection)
		{
		}
		public QPersons(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Persons Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Persons(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Persons obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Persons obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Persons obj) 
		{
			try
			{
				AddParameter(P_First_Name,obj.First_Name,true,false);
				AddParameter(P_Middle_Name,obj.Middle_Name,true,true);
				AddParameter(P_Last_Name,obj.Last_Name,true,false);
				AddParameter(P_Perm_Address_1,obj.Perm_Address_1,true,true);
				AddParameter(P_Perm_Address_2,obj.Perm_Address_2,true,true);
				AddParameter(P_Perm_City,obj.Perm_City,true,true);
				AddParameter(P_Perm_Provice,obj.Perm_Provice,true,true);
				AddParameter(P_Perm_Id_Country,obj.Perm_Id_Country,true,false);
				AddParameter(P_Sex,obj.Sex,true,true);
				AddParameter(P_CNIC,obj.CNIC,true,true);
				AddParameter(P_Landline_No,obj.Landline_No,true,true);
				AddParameter(P_Cell_No,obj.Cell_No,true,false);
				AddParameter(P_Occupation_Profession,obj.Occupation_Profession,true,true);
				AddParameter(P_Photo,obj.Photo,true,true);
				AddParameter(P_Id_Relative,obj.Id_Relative,true,true);
				AddParameter(P_Relation,obj.Relation,true,true);
				AddParameter(P_Type,obj.Type,true,false);
				AddParameter(P_Alive,obj.Alive,true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_First_Name,obj[P_First_Name],true,false);
				AddParameter(P_Middle_Name,obj[P_Middle_Name],true,true);
				AddParameter(P_Last_Name,obj[P_Last_Name],true,false);
				AddParameter(P_Perm_Address_1,obj[P_Perm_Address_1],true,true);
				AddParameter(P_Perm_Address_2,obj[P_Perm_Address_2],true,true);
				AddParameter(P_Perm_City,obj[P_Perm_City],true,true);
				AddParameter(P_Perm_Provice,obj[P_Perm_Provice],true,true);
				AddParameter(P_Perm_Id_Country,obj[P_Perm_Id_Country],true,false);
				AddParameter(P_Sex,obj[P_Sex],true,true);
				AddParameter(P_CNIC,obj[P_CNIC],true,true);
				AddParameter(P_Landline_No,obj[P_Landline_No],true,true);
				AddParameter(P_Cell_No,obj[P_Cell_No],true,false);
				AddParameter(P_Occupation_Profession,obj[P_Occupation_Profession],true,true);
				AddParameter(P_Photo,obj[P_Photo],true,true);
				AddParameter(P_Id_Relative,obj[P_Id_Relative],true,true);
				AddParameter(P_Relation,obj[P_Relation],true,true);
				AddParameter(P_Type,obj[P_Type],true,false);
				AddParameter(P_Alive,obj[P_Alive],true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Persons obj,string Id ) 
		{
			try
			{
				AddParameter(P_First_Name,obj.First_Name,true,false);
				AddParameter(P_Middle_Name,obj.Middle_Name,true,true);
				AddParameter(P_Last_Name,obj.Last_Name,true,false);
				AddParameter(P_Perm_Address_1,obj.Perm_Address_1,true,true);
				AddParameter(P_Perm_Address_2,obj.Perm_Address_2,true,true);
				AddParameter(P_Perm_City,obj.Perm_City,true,true);
				AddParameter(P_Perm_Provice,obj.Perm_Provice,true,true);
				AddParameter(P_Perm_Id_Country,obj.Perm_Id_Country,true,false);
				AddParameter(P_Sex,obj.Sex,true,true);
				AddParameter(P_CNIC,obj.CNIC,true,true);
				AddParameter(P_Landline_No,obj.Landline_No,true,true);
				AddParameter(P_Cell_No,obj.Cell_No,true,false);
				AddParameter(P_Occupation_Profession,obj.Occupation_Profession,true,true);
				AddParameter(P_Photo,obj.Photo,true,true);
				AddParameter(P_Id_Relative,obj.Id_Relative,true,true);
				AddParameter(P_Relation,obj.Relation,true,true);
				AddParameter(P_Type,obj.Type,true,false);
				AddParameter(P_Alive,obj.Alive,true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_First_Name,obj[P_First_Name],true,false);
				AddParameter(P_Middle_Name,obj[P_Middle_Name],true,true);
				AddParameter(P_Last_Name,obj[P_Last_Name],true,false);
				AddParameter(P_Perm_Address_1,obj[P_Perm_Address_1],true,true);
				AddParameter(P_Perm_Address_2,obj[P_Perm_Address_2],true,true);
				AddParameter(P_Perm_City,obj[P_Perm_City],true,true);
				AddParameter(P_Perm_Provice,obj[P_Perm_Provice],true,true);
				AddParameter(P_Perm_Id_Country,obj[P_Perm_Id_Country],true,false);
				AddParameter(P_Sex,obj[P_Sex],true,true);
				AddParameter(P_CNIC,obj[P_CNIC],true,true);
				AddParameter(P_Landline_No,obj[P_Landline_No],true,true);
				AddParameter(P_Cell_No,obj[P_Cell_No],true,false);
				AddParameter(P_Occupation_Profession,obj[P_Occupation_Profession],true,true);
				AddParameter(P_Photo,obj[P_Photo],true,true);
				AddParameter(P_Id_Relative,obj[P_Id_Relative],true,true);
				AddParameter(P_Relation,obj[P_Relation],true,true);
				AddParameter(P_Type,obj[P_Type],true,false);
				AddParameter(P_Alive,obj[P_Alive],true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectByPerm_Id_Country=@"Select * From Persons 
								 where Perm_Id_Country = @Perm_Id_Country  order by  First_Name";
		public const string QDeleteByPerm_Id_Country=@"Delete From Persons 
								 where Perm_Id_Country = @Perm_Id_Country";
		virtual	public DataTable GetByPerm_Id_Country(string Perm_Id_Country ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByPerm_Id_Country;
				AddParameter(P_Perm_Id_Country,Perm_Id_Country,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByPerm_Id_Country( string Perm_Id_Country ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByPerm_Id_Country;
				AddParameter(P_Perm_Id_Country,Perm_Id_Country,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Relative=@"Select * From Persons 
								 where Id_Relative = @Id_Relative  order by  First_Name";
		public const string QDeleteById_Relative=@"Delete From Persons 
								 where Id_Relative = @Id_Relative";
		virtual	public DataTable GetById_Relative(string Id_Relative ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Relative;
				AddParameter(P_Id_Relative,Id_Relative,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Relative( string Id_Relative ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Relative;
				AddParameter(P_Id_Relative,Id_Relative,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Persons
	{
		public Persons()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _First_Name=string.Empty;
		public string First_Name
		{
			get
			{
				 return _First_Name;
			}
			set
			{
				  _First_Name = value;
			}
		}
		protected  string _Middle_Name=string.Empty;
		public string Middle_Name
		{
			get
			{
				 return _Middle_Name;
			}
			set
			{
				  _Middle_Name = value;
			}
		}
		protected  string _Last_Name=string.Empty;
		public string Last_Name
		{
			get
			{
				 return _Last_Name;
			}
			set
			{
				  _Last_Name = value;
			}
		}
		protected  string _Perm_Address_1=string.Empty;
		public string Perm_Address_1
		{
			get
			{
				 return _Perm_Address_1;
			}
			set
			{
				  _Perm_Address_1 = value;
			}
		}
		protected  string _Perm_Address_2=string.Empty;
		public string Perm_Address_2
		{
			get
			{
				 return _Perm_Address_2;
			}
			set
			{
				  _Perm_Address_2 = value;
			}
		}
		protected  string _Perm_City=string.Empty;
		public string Perm_City
		{
			get
			{
				 return _Perm_City;
			}
			set
			{
				  _Perm_City = value;
			}
		}
		protected  string _Perm_Provice=string.Empty;
		public string Perm_Provice
		{
			get
			{
				 return _Perm_Provice;
			}
			set
			{
				  _Perm_Provice = value;
			}
		}
		protected  string _Perm_Id_Country=string.Empty;
		public string Perm_Id_Country
		{
			get
			{
				 return _Perm_Id_Country;
			}
			set
			{
				  _Perm_Id_Country = value;
			}
		}
		protected  string _Sex=string.Empty;
		public string Sex
		{
			get
			{
				 return _Sex;
			}
			set
			{
				  _Sex = value;
			}
		}
		protected  string _CNIC=string.Empty;
		public string CNIC
		{
			get
			{
				 return _CNIC;
			}
			set
			{
				  _CNIC = value;
			}
		}
		protected  string _Landline_No=string.Empty;
		public string Landline_No
		{
			get
			{
				 return _Landline_No;
			}
			set
			{
				  _Landline_No = value;
			}
		}
		protected  string _Cell_No=string.Empty;
		public string Cell_No
		{
			get
			{
				 return _Cell_No;
			}
			set
			{
				  _Cell_No = value;
			}
		}
		protected  string _Occupation_Profession=string.Empty;
		public string Occupation_Profession
		{
			get
			{
				 return _Occupation_Profession;
			}
			set
			{
				  _Occupation_Profession = value;
			}
		}
		protected  string _Photo=string.Empty;
		public string Photo
		{
			get
			{
				 return _Photo;
			}
			set
			{
				  _Photo = value;
			}
		}
		protected  string _Id_Relative=string.Empty;
		public string Id_Relative
		{
			get
			{
				 return _Id_Relative;
			}
			set
			{
				  _Id_Relative = value;
			}
		}
		protected  string _Relation=string.Empty;
		public string Relation
		{
			get
			{
				 return _Relation;
			}
			set
			{
				  _Relation = value;
			}
		}
		protected  string _Type=string.Empty;
		public string Type
		{
			get
			{
				 return _Type;
			}
			set
			{
				  _Type = value;
			}
		}
		protected  string _Alive=string.Empty;
		public string Alive
		{
			get
			{
				 return _Alive;
			}
			set
			{
				  _Alive = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Persons(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QPersons.P_Id))
					Id = obj[QPersons.P_Id].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_First_Name))
					First_Name = obj[QPersons.P_First_Name].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Middle_Name))
					Middle_Name = obj[QPersons.P_Middle_Name].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Last_Name))
					Last_Name = obj[QPersons.P_Last_Name].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Perm_Address_1))
					Perm_Address_1 = obj[QPersons.P_Perm_Address_1].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Perm_Address_2))
					Perm_Address_2 = obj[QPersons.P_Perm_Address_2].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Perm_City))
					Perm_City = obj[QPersons.P_Perm_City].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Perm_Provice))
					Perm_Provice = obj[QPersons.P_Perm_Provice].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Perm_Id_Country))
					Perm_Id_Country = obj[QPersons.P_Perm_Id_Country].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Sex))
					Sex = obj[QPersons.P_Sex].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_CNIC))
					CNIC = obj[QPersons.P_CNIC].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Landline_No))
					Landline_No = obj[QPersons.P_Landline_No].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Cell_No))
					Cell_No = obj[QPersons.P_Cell_No].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Occupation_Profession))
					Occupation_Profession = obj[QPersons.P_Occupation_Profession].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Photo))
					Photo = obj[QPersons.P_Photo].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Id_Relative))
					Id_Relative = obj[QPersons.P_Id_Relative].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Relation))
					Relation = obj[QPersons.P_Relation].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Type))
					Type = obj[QPersons.P_Type].ToString();
				if(obj.Table.Columns.Contains(QPersons.P_Alive))
					Alive = obj[QPersons.P_Alive].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}