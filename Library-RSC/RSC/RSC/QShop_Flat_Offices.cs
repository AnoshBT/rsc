using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QShop_Flat_Offices:Query
	{

		public const string TABLE_NAME = "Shop_Flat_Offices";
		public const string P_Id = "Id";
		public const string P_Id_Plaza = "Id_Plaza";
		public const string P_Shop_Flat_Office = "Shop_Flat_Office";
		public const string P_Shop_Flat_Office_No = "Shop_Flat_Office_No";
		public const string P_Floor = "Floor";
		public const string P_Shop_Office_Name = "Shop_Office_Name";
		public const string P_Id_Current_Owner = "Id_Current_Owner";
		public const string P_Id_Current_Tenant = "Id_Current_Tenant";
		public const string SelectColumns=@"Id,Id_Plaza,Shop_Flat_Office,Shop_Flat_Office_No,Floor,Shop_Office_Name,Id_Current_Owner,Id_Current_Tenant";
		public const string QSelect=@"Select * From Shop_Flat_Offices  order by  Shop_Flat_Office";
		public const string QSelectById=@"Select * From Shop_Flat_Offices 
								 where Id = @Id  order by  Shop_Flat_Office";
		public const string QInsert=@"Insert Into  Shop_Flat_Offices (Id_Plaza,Shop_Flat_Office,Shop_Flat_Office_No,Floor,Shop_Office_Name,Id_Current_Owner,Id_Current_Tenant) 
								 values(@Id_Plaza,@Shop_Flat_Office,@Shop_Flat_Office_No,@Floor,@Shop_Office_Name,@Id_Current_Owner,@Id_Current_Tenant)";
		public const string QUpdateById=@"Update Shop_Flat_Offices Set Id_Plaza=@Id_Plaza,Shop_Flat_Office=@Shop_Flat_Office,Shop_Flat_Office_No=@Shop_Flat_Office_No,Floor=@Floor,
						Shop_Office_Name=@Shop_Office_Name,Id_Current_Owner=@Id_Current_Owner,Id_Current_Tenant=@Id_Current_Tenant 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Shop_Flat_Offices 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Shop_Flat_Offices 
								 where Id = @Id";

		public QShop_Flat_Offices(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QShop_Flat_Offices(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QShop_Flat_Offices(IDbConnection Connection):base(Connection)
		{
		}
		public QShop_Flat_Offices(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Shop_Flat_Offices Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Shop_Flat_Offices(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Shop_Flat_Offices obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Shop_Flat_Offices obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Shop_Flat_Offices obj) 
		{
			try
			{
				AddParameter(P_Id_Plaza,obj.Id_Plaza,true,false);
				AddParameter(P_Shop_Flat_Office,obj.Shop_Flat_Office,true,false);
				AddParameter(P_Shop_Flat_Office_No,obj.Shop_Flat_Office_No,true,false);
				AddParameter(P_Floor,obj.Floor,true,false);
				AddParameter(P_Shop_Office_Name,obj.Shop_Office_Name,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Id_Plaza,obj[P_Id_Plaza],true,false);
				AddParameter(P_Shop_Flat_Office,obj[P_Shop_Flat_Office],true,false);
				AddParameter(P_Shop_Flat_Office_No,obj[P_Shop_Flat_Office_No],true,false);
				AddParameter(P_Floor,obj[P_Floor],true,false);
				AddParameter(P_Shop_Office_Name,obj[P_Shop_Office_Name],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Shop_Flat_Offices obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Plaza,obj.Id_Plaza,true,false);
				AddParameter(P_Shop_Flat_Office,obj.Shop_Flat_Office,true,false);
				AddParameter(P_Shop_Flat_Office_No,obj.Shop_Flat_Office_No,true,false);
				AddParameter(P_Floor,obj.Floor,true,false);
				AddParameter(P_Shop_Office_Name,obj.Shop_Office_Name,true,true);
				AddParameter(P_Id_Current_Owner,obj.Id_Current_Owner,true,true);
				AddParameter(P_Id_Current_Tenant,obj.Id_Current_Tenant,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Id_Plaza,obj[P_Id_Plaza],true,false);
				AddParameter(P_Shop_Flat_Office,obj[P_Shop_Flat_Office],true,false);
				AddParameter(P_Shop_Flat_Office_No,obj[P_Shop_Flat_Office_No],true,false);
				AddParameter(P_Floor,obj[P_Floor],true,false);
				AddParameter(P_Shop_Office_Name,obj[P_Shop_Office_Name],true,true);
				AddParameter(P_Id_Current_Owner,obj[P_Id_Current_Owner],true,true);
				AddParameter(P_Id_Current_Tenant,obj[P_Id_Current_Tenant],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectByFloor=@"Select * From Shop_Flat_Offices 
								 where Floor = @Floor  order by  Shop_Flat_Office";
		public const string QDeleteByFloor=@"Delete From Shop_Flat_Offices 
								 where Floor = @Floor";
		virtual	public DataTable GetByFloor(string Floor ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByFloor;
				AddParameter(P_Floor,Floor,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByFloor( string Floor ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByFloor;
				AddParameter(P_Floor,Floor,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Current_Owner=@"Select * From Shop_Flat_Offices 
								 where Id_Current_Owner = @Id_Current_Owner  order by  Shop_Flat_Office";
		public const string QDeleteById_Current_Owner=@"Delete From Shop_Flat_Offices 
								 where Id_Current_Owner = @Id_Current_Owner";
		virtual	public DataTable GetById_Current_Owner(string Id_Current_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Current_Owner;
				AddParameter(P_Id_Current_Owner,Id_Current_Owner,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Current_Owner( string Id_Current_Owner ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Current_Owner;
				AddParameter(P_Id_Current_Owner,Id_Current_Owner,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Current_Tenant=@"Select * From Shop_Flat_Offices 
								 where Id_Current_Tenant = @Id_Current_Tenant  order by  Shop_Flat_Office";
		public const string QDeleteById_Current_Tenant=@"Delete From Shop_Flat_Offices 
								 where Id_Current_Tenant = @Id_Current_Tenant";
		virtual	public DataTable GetById_Current_Tenant(string Id_Current_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Current_Tenant;
				AddParameter(P_Id_Current_Tenant,Id_Current_Tenant,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Current_Tenant( string Id_Current_Tenant ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Current_Tenant;
				AddParameter(P_Id_Current_Tenant,Id_Current_Tenant,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Plaza=@"Select * From Shop_Flat_Offices 
								 where Id_Plaza = @Id_Plaza  order by  Shop_Flat_Office";
		public const string QDeleteById_Plaza=@"Delete From Shop_Flat_Offices 
								 where Id_Plaza = @Id_Plaza";
		virtual	public DataTable GetById_Plaza(string Id_Plaza ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Plaza;
				AddParameter(P_Id_Plaza,Id_Plaza,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Plaza( string Id_Plaza ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Plaza;
				AddParameter(P_Id_Plaza,Id_Plaza,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Shop_Flat_Offices
	{
		public Shop_Flat_Offices()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Id_Plaza=string.Empty;
		public string Id_Plaza
		{
			get
			{
				 return _Id_Plaza;
			}
			set
			{
				  _Id_Plaza = value;
			}
		}
		protected  string _Shop_Flat_Office=string.Empty;
		public string Shop_Flat_Office
		{
			get
			{
				 return _Shop_Flat_Office;
			}
			set
			{
				  _Shop_Flat_Office = value;
			}
		}
		protected  string _Shop_Flat_Office_No=string.Empty;
		public string Shop_Flat_Office_No
		{
			get
			{
				 return _Shop_Flat_Office_No;
			}
			set
			{
				  _Shop_Flat_Office_No = value;
			}
		}
		protected  string _Floor=string.Empty;
		public string Floor
		{
			get
			{
				 return _Floor;
			}
			set
			{
				  _Floor = value;
			}
		}
		protected  string _Shop_Office_Name=string.Empty;
		public string Shop_Office_Name
		{
			get
			{
				 return _Shop_Office_Name;
			}
			set
			{
				  _Shop_Office_Name = value;
			}
		}
		protected  string _Id_Current_Owner=string.Empty;
		public string Id_Current_Owner
		{
			get
			{
				 return _Id_Current_Owner;
			}
			set
			{
				  _Id_Current_Owner = value;
			}
		}
		protected  string _Id_Current_Tenant=string.Empty;
		public string Id_Current_Tenant
		{
			get
			{
				 return _Id_Current_Tenant;
			}
			set
			{
				  _Id_Current_Tenant = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Shop_Flat_Offices(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Id))
					Id = obj[QShop_Flat_Offices.P_Id].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Id_Plaza))
					Id_Plaza = obj[QShop_Flat_Offices.P_Id_Plaza].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Shop_Flat_Office))
					Shop_Flat_Office = obj[QShop_Flat_Offices.P_Shop_Flat_Office].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Shop_Flat_Office_No))
					Shop_Flat_Office_No = obj[QShop_Flat_Offices.P_Shop_Flat_Office_No].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Floor))
					Floor = obj[QShop_Flat_Offices.P_Floor].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Shop_Office_Name))
					Shop_Office_Name = obj[QShop_Flat_Offices.P_Shop_Office_Name].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Id_Current_Owner))
					Id_Current_Owner = obj[QShop_Flat_Offices.P_Id_Current_Owner].ToString();
				if(obj.Table.Columns.Contains(QShop_Flat_Offices.P_Id_Current_Tenant))
					Id_Current_Tenant = obj[QShop_Flat_Offices.P_Id_Current_Tenant].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}