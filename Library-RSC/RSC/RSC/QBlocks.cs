using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QBlocks:Query
	{

		public const string TABLE_NAME = "Blocks";
		public const string P_Id = "Id";
		public const string P_Block_Name = "Block_Name";
		public const string SelectColumns=@"Id,Block_Name";
		public const string QSelect=@"Select * From Blocks  order by  Block_Name";
		public const string QSelectById=@"Select * From Blocks 
								 where Id = @Id  order by  Block_Name";
		public const string QInsert=@"Insert Into  Blocks (Block_Name) 
								 values(@Block_Name)";
		public const string QUpdateById=@"Update Blocks Set Block_Name=@Block_Name 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Blocks 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Blocks 
								 where Id = @Id";

		public QBlocks(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QBlocks(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QBlocks(IDbConnection Connection):base(Connection)
		{
		}
		public QBlocks(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Blocks Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Blocks(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Blocks obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Blocks obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Blocks obj) 
		{
			try
			{
				AddParameter(P_Block_Name,obj.Block_Name,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Block_Name,obj[P_Block_Name],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Blocks obj,string Id ) 
		{
			try
			{
				AddParameter(P_Block_Name,obj.Block_Name,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Block_Name,obj[P_Block_Name],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Blocks
	{
		public Blocks()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Block_Name=string.Empty;
		public string Block_Name
		{
			get
			{
				 return _Block_Name;
			}
			set
			{
				  _Block_Name = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Blocks(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QBlocks.P_Id))
					Id = obj[QBlocks.P_Id].ToString();
				if(obj.Table.Columns.Contains(QBlocks.P_Block_Name))
					Block_Name = obj[QBlocks.P_Block_Name].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}