using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QTU001:Query
	{

		public const string TABLE_NAME = "TU001";
		public const string P_ID = "ID";
		public const string P_UserID = "UserID";
		public const string P_UserLevelID = "UserLevelID";
		public const string P_Email = "Email";
		public const string P_FirstName = "FirstName";
		public const string P_LastName = "LastName";
		public const string P_Telephone = "Telephone";
		public const string P_Mobile = "Mobile";
		public const string P_Address = "Address";
		public const string P_City = "City";
		public const string P_Postcode = "Postcode";
		public const string P_State = "State";
		public const string P_Country = "Country";
		public const string P_SecurityQuestion = "SecurityQuestion";
		public const string P_Answer = "Answer";
		public const string P_CreateDate = "CreateDate";
		public const string P_ModifyDate = "ModifyDate";
		public const string P_IsBlock = "IsBlock";
		public const string SelectColumns=@"ID,UserID,UserLevelID,Email,FirstName,LastName,Telephone,Mobile,Address,
						City,Postcode,State,Country,SecurityQuestion,Answer,CreateDate,ModifyDate,
						IsBlock";
		public const string QSelect=@"Select * From TU001  order by  UserID";
		public const string QSelectByUserID=@"Select * From TU001 
								 where UserID = @UserID  order by  UserID";
		public const string QSelectByID=@"Select ID,UserID,UserLevelID,Email,FirstName,LastName,Telephone,Mobile,Address,
						City,Postcode,State,Country,SecurityQuestion,Answer,CreateDate,ModifyDate,
						IsBlock 
								 From TU001 
								 where ID=@ID";
		public const string QInsert=@"Insert Into  TU001 (UserID,UserLevelID,Email,FirstName,LastName,Telephone,Mobile,Address,
						City,Postcode,State,Country,SecurityQuestion,Answer,CreateDate,ModifyDate,
						IsBlock) 
								 values(@UserID,@UserLevelID,@Email,@FirstName,@LastName,@Telephone,@Mobile,@Address,
						@City,@Postcode,@State,@Country,@SecurityQuestion,@Answer,@CreateDate,@ModifyDate,
						@IsBlock)";
		public const string QUpdateByUserID=@"Update TU001 Set UserID=@UserID,UserLevelID=@UserLevelID,Email=@Email,FirstName=@FirstName,
						LastName=@LastName,Telephone=@Telephone,Mobile=@Mobile,Address=@Address,
						City=@City,Postcode=@Postcode,State=@State,Country=@Country,
						SecurityQuestion=@SecurityQuestion,Answer=@Answer,CreateDate=@CreateDate,ModifyDate=@ModifyDate,
						IsBlock=@IsBlock 
								 where UserID = @O_UserID";
		public const string QUpdateByID=@"Update TU001 Set UserID=@UserID,UserLevelID=@UserLevelID,Email=@Email,FirstName=@FirstName,
						LastName=@LastName,Telephone=@Telephone,Mobile=@Mobile,Address=@Address,
						City=@City,Postcode=@Postcode,State=@State,Country=@Country,
						SecurityQuestion=@SecurityQuestion,Answer=@Answer,CreateDate=@CreateDate,ModifyDate=@ModifyDate,
						IsBlock=@IsBlock 
								 where ID=@O_ID";
		public const string QDeleteByUserID=@"Delete From TU001 
								 where UserID = @UserID";
		public const string QDeleteByID=@"Delete From TU001 
								 where ID=@ID";
		public const string QExistsByUserID=@"select UserID  From TU001 
								 where UserID = @UserID";

		public QTU001(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QTU001(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QTU001(IDbConnection Connection):base(Connection)
		{
		}
		public QTU001(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public TU001 Get(string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByUserID;
				AddParameter(P_UserID,UserID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new TU001(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public TU001 GetByID(string ID) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByID;
				AddParameter(P_ID,ID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new TU001(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(TU001 obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(TU001 obj,string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserID;
				SetParameters(obj,UserID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserID;
				SetParameters(obj,UserID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int UpdateByID(TU001 obj,string ID) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				AddParameter("O_ID",ID,false,false);
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_UserLevelID,obj.UserLevelID,true,true);
				AddParameter(P_Email,obj.Email,true,true);
				AddParameter(P_FirstName,obj.FirstName,true,true);
				AddParameter(P_LastName,obj.LastName,true,true);
				AddParameter(P_Telephone,obj.Telephone,true,true);
				AddParameter(P_Mobile,obj.Mobile,true,true);
				AddParameter(P_Address,obj.Address,true,true);
				AddParameter(P_City,obj.City,true,true);
				AddParameter(P_Postcode,obj.Postcode,true,true);
				AddParameter(P_State,obj.State,true,true);
				AddParameter(P_Country,obj.Country,true,true);
				AddParameter(P_SecurityQuestion,obj.SecurityQuestion,true,true);
				AddParameter(P_Answer,obj.Answer,true,true);
				AddParameter(P_CreateDate,obj.CreateDate,true,true);
				AddParameter(P_ModifyDate,obj.ModifyDate,true,true);
				AddParameter(P_IsBlock,obj.IsBlock,true,true);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int UpdateByID(DataRow obj,string ID) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				AddParameter("O_ID",ID,false,false);
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,true);
				AddParameter(P_Email,obj[P_Email],true,true);
				AddParameter(P_FirstName,obj[P_FirstName],true,true);
				AddParameter(P_LastName,obj[P_LastName],true,true);
				AddParameter(P_Telephone,obj[P_Telephone],true,true);
				AddParameter(P_Mobile,obj[P_Mobile],true,true);
				AddParameter(P_Address,obj[P_Address],true,true);
				AddParameter(P_City,obj[P_City],true,true);
				AddParameter(P_Postcode,obj[P_Postcode],true,true);
				AddParameter(P_State,obj[P_State],true,true);
				AddParameter(P_Country,obj[P_Country],true,true);
				AddParameter(P_SecurityQuestion,obj[P_SecurityQuestion],true,true);
				AddParameter(P_Answer,obj[P_Answer],true,true);
				AddParameter(P_CreateDate,obj[P_CreateDate],true,true);
				AddParameter(P_ModifyDate,obj[P_ModifyDate],true,true);
				AddParameter(P_IsBlock,obj[P_IsBlock],true,true);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByID(string ID) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByID;
				AddParameter(P_ID,ID);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(TU001 obj) 
		{
			try
			{
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_UserLevelID,obj.UserLevelID,true,true);
				AddParameter(P_Email,obj.Email,true,true);
				AddParameter(P_FirstName,obj.FirstName,true,true);
				AddParameter(P_LastName,obj.LastName,true,true);
				AddParameter(P_Telephone,obj.Telephone,true,true);
				AddParameter(P_Mobile,obj.Mobile,true,true);
				AddParameter(P_Address,obj.Address,true,true);
				AddParameter(P_City,obj.City,true,true);
				AddParameter(P_Postcode,obj.Postcode,true,true);
				AddParameter(P_State,obj.State,true,true);
				AddParameter(P_Country,obj.Country,true,true);
				AddParameter(P_SecurityQuestion,obj.SecurityQuestion,true,true);
				AddParameter(P_Answer,obj.Answer,true,true);
				AddParameter(P_CreateDate,obj.CreateDate,true,true);
				AddParameter(P_ModifyDate,obj.ModifyDate,true,true);
				AddParameter(P_IsBlock,obj.IsBlock,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,true);
				AddParameter(P_Email,obj[P_Email],true,true);
				AddParameter(P_FirstName,obj[P_FirstName],true,true);
				AddParameter(P_LastName,obj[P_LastName],true,true);
				AddParameter(P_Telephone,obj[P_Telephone],true,true);
				AddParameter(P_Mobile,obj[P_Mobile],true,true);
				AddParameter(P_Address,obj[P_Address],true,true);
				AddParameter(P_City,obj[P_City],true,true);
				AddParameter(P_Postcode,obj[P_Postcode],true,true);
				AddParameter(P_State,obj[P_State],true,true);
				AddParameter(P_Country,obj[P_Country],true,true);
				AddParameter(P_SecurityQuestion,obj[P_SecurityQuestion],true,true);
				AddParameter(P_Answer,obj[P_Answer],true,true);
				AddParameter(P_CreateDate,obj[P_CreateDate],true,true);
				AddParameter(P_ModifyDate,obj[P_ModifyDate],true,true);
				AddParameter(P_IsBlock,obj[P_IsBlock],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(TU001 obj,string UserID ) 
		{
			try
			{
				AddParameter(P_UserID,obj.UserID,true,false);
				AddParameter(P_UserLevelID,obj.UserLevelID,true,true);
				AddParameter(P_Email,obj.Email,true,true);
				AddParameter(P_FirstName,obj.FirstName,true,true);
				AddParameter(P_LastName,obj.LastName,true,true);
				AddParameter(P_Telephone,obj.Telephone,true,true);
				AddParameter(P_Mobile,obj.Mobile,true,true);
				AddParameter(P_Address,obj.Address,true,true);
				AddParameter(P_City,obj.City,true,true);
				AddParameter(P_Postcode,obj.Postcode,true,true);
				AddParameter(P_State,obj.State,true,true);
				AddParameter(P_Country,obj.Country,true,true);
				AddParameter(P_SecurityQuestion,obj.SecurityQuestion,true,true);
				AddParameter(P_Answer,obj.Answer,true,true);
				AddParameter(P_CreateDate,obj.CreateDate,true,true);
				AddParameter(P_ModifyDate,obj.ModifyDate,true,true);
				AddParameter(P_IsBlock,obj.IsBlock,true,true);
				AddParameter("O_UserID",UserID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string UserID ) 
		{
			try
			{
				AddParameter(P_UserID,obj[P_UserID],true,false);
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,true);
				AddParameter(P_Email,obj[P_Email],true,true);
				AddParameter(P_FirstName,obj[P_FirstName],true,true);
				AddParameter(P_LastName,obj[P_LastName],true,true);
				AddParameter(P_Telephone,obj[P_Telephone],true,true);
				AddParameter(P_Mobile,obj[P_Mobile],true,true);
				AddParameter(P_Address,obj[P_Address],true,true);
				AddParameter(P_City,obj[P_City],true,true);
				AddParameter(P_Postcode,obj[P_Postcode],true,true);
				AddParameter(P_State,obj[P_State],true,true);
				AddParameter(P_Country,obj[P_Country],true,true);
				AddParameter(P_SecurityQuestion,obj[P_SecurityQuestion],true,true);
				AddParameter(P_Answer,obj[P_Answer],true,true);
				AddParameter(P_CreateDate,obj[P_CreateDate],true,true);
				AddParameter(P_ModifyDate,obj[P_ModifyDate],true,true);
				AddParameter(P_IsBlock,obj[P_IsBlock],true,true);
				AddParameter("O_UserID",UserID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  TU001
	{
		public TU001()
		{
		}
		protected  string _ID=string.Empty;
		public string ID
		{
			get
			{
				 return _ID;
			}
			set
			{
				  _ID = value;
			}
		}
		protected  string _UserID=string.Empty;
		public string UserID
		{
			get
			{
				 return _UserID;
			}
			set
			{
				  _UserID = value;
			}
		}
		protected  string _UserLevelID=string.Empty;
		public string UserLevelID
		{
			get
			{
				 return _UserLevelID;
			}
			set
			{
				  _UserLevelID = value;
			}
		}
		protected  string _Email=string.Empty;
		public string Email
		{
			get
			{
				 return _Email;
			}
			set
			{
				  _Email = value;
			}
		}
		protected  string _FirstName=string.Empty;
		public string FirstName
		{
			get
			{
				 return _FirstName;
			}
			set
			{
				  _FirstName = value;
			}
		}
		protected  string _LastName=string.Empty;
		public string LastName
		{
			get
			{
				 return _LastName;
			}
			set
			{
				  _LastName = value;
			}
		}
		protected  string _Telephone=string.Empty;
		public string Telephone
		{
			get
			{
				 return _Telephone;
			}
			set
			{
				  _Telephone = value;
			}
		}
		protected  string _Mobile=string.Empty;
		public string Mobile
		{
			get
			{
				 return _Mobile;
			}
			set
			{
				  _Mobile = value;
			}
		}
		protected  string _Address=string.Empty;
		public string Address
		{
			get
			{
				 return _Address;
			}
			set
			{
				  _Address = value;
			}
		}
		protected  string _City=string.Empty;
		public string City
		{
			get
			{
				 return _City;
			}
			set
			{
				  _City = value;
			}
		}
		protected  string _Postcode=string.Empty;
		public string Postcode
		{
			get
			{
				 return _Postcode;
			}
			set
			{
				  _Postcode = value;
			}
		}
		protected  string _State=string.Empty;
		public string State
		{
			get
			{
				 return _State;
			}
			set
			{
				  _State = value;
			}
		}
		protected  string _Country=string.Empty;
		public string Country
		{
			get
			{
				 return _Country;
			}
			set
			{
				  _Country = value;
			}
		}
		protected  string _SecurityQuestion=string.Empty;
		public string SecurityQuestion
		{
			get
			{
				 return _SecurityQuestion;
			}
			set
			{
				  _SecurityQuestion = value;
			}
		}
		protected  string _Answer=string.Empty;
		public string Answer
		{
			get
			{
				 return _Answer;
			}
			set
			{
				  _Answer = value;
			}
		}
		protected  DateTime _CreateDate=new DateTime();
		public DateTime CreateDate
		{
			get
			{
				 return _CreateDate;
			}
			set
			{
				  _CreateDate = value;
			}
		}
		protected  DateTime _ModifyDate=new DateTime();
		public DateTime ModifyDate
		{
			get
			{
				 return _ModifyDate;
			}
			set
			{
				  _ModifyDate = value;
			}
		}
		protected  string _IsBlock=string.Empty;
		public string IsBlock
		{
			get
			{
				 return _IsBlock;
			}
			set
			{
				  _IsBlock = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public TU001(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QTU001.P_ID))
					ID = obj[QTU001.P_ID].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_UserID))
					UserID = obj[QTU001.P_UserID].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_UserLevelID))
					UserLevelID = obj[QTU001.P_UserLevelID].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Email))
					Email = obj[QTU001.P_Email].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_FirstName))
					FirstName = obj[QTU001.P_FirstName].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_LastName))
					LastName = obj[QTU001.P_LastName].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Telephone))
					Telephone = obj[QTU001.P_Telephone].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Mobile))
					Mobile = obj[QTU001.P_Mobile].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Address))
					Address = obj[QTU001.P_Address].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_City))
					City = obj[QTU001.P_City].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Postcode))
					Postcode = obj[QTU001.P_Postcode].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_State))
					State = obj[QTU001.P_State].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Country))
					Country = obj[QTU001.P_Country].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_SecurityQuestion))
					SecurityQuestion = obj[QTU001.P_SecurityQuestion].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_Answer))
					Answer = obj[QTU001.P_Answer].ToString();
				if(obj.Table.Columns.Contains(QTU001.P_CreateDate) && obj[QTU001.P_CreateDate].ToString()!="" && obj[QTU001.P_CreateDate] is DateTime )
					CreateDate =(DateTime)obj[QTU001.P_CreateDate];
				if(obj.Table.Columns.Contains(QTU001.P_ModifyDate) && obj[QTU001.P_ModifyDate].ToString()!="" && obj[QTU001.P_ModifyDate] is DateTime )
					ModifyDate =(DateTime)obj[QTU001.P_ModifyDate];
				if(obj.Table.Columns.Contains(QTU001.P_IsBlock))
					IsBlock = obj[QTU001.P_IsBlock].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}