using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QVehicle_Owners:Query
	{

		public const string TABLE_NAME = "Vehicle_Owners";
		public const string P_Id = "Id";
		public const string P_Veh_Make = "Veh_Make";
		public const string P_Veh_Model = "Veh_Model";
		public const string P_Veh_Model_Year = "Veh_Model_Year";
		public const string P_Id_Veh_Type = "Id_Veh_Type";
		public const string P_Id_Person = "Id_Person";
		public const string SelectColumns=@"Id,Veh_Make,Veh_Model,Veh_Model_Year,Id_Veh_Type,Id_Person";
		public const string QSelect=@"Select * From Vehicle_Owners  order by  Veh_Make";
		public const string QSelectById=@"Select * From Vehicle_Owners 
								 where Id = @Id  order by  Veh_Make";
		public const string QInsert=@"Insert Into  Vehicle_Owners (Veh_Make,Veh_Model,Veh_Model_Year,Id_Veh_Type,Id_Person) 
								 values(@Veh_Make,@Veh_Model,@Veh_Model_Year,@Id_Veh_Type,@Id_Person)";
		public const string QUpdateById=@"Update Vehicle_Owners Set Veh_Make=@Veh_Make,Veh_Model=@Veh_Model,Veh_Model_Year=@Veh_Model_Year,Id_Veh_Type=@Id_Veh_Type,
						Id_Person=@Id_Person 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Vehicle_Owners 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Vehicle_Owners 
								 where Id = @Id";

		public QVehicle_Owners(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QVehicle_Owners(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QVehicle_Owners(IDbConnection Connection):base(Connection)
		{
		}
		public QVehicle_Owners(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Vehicle_Owners Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Vehicle_Owners(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Vehicle_Owners obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Vehicle_Owners obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Vehicle_Owners obj) 
		{
			try
			{
				AddParameter(P_Veh_Make,obj.Veh_Make,true,false);
				AddParameter(P_Veh_Model,obj.Veh_Model,true,false);
				AddParameter(P_Veh_Model_Year,obj.Veh_Model_Year,true,true);
				AddParameter(P_Id_Veh_Type,obj.Id_Veh_Type,true,false);
				AddParameter(P_Id_Person,obj.Id_Person,true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Veh_Make,obj[P_Veh_Make],true,false);
				AddParameter(P_Veh_Model,obj[P_Veh_Model],true,false);
				AddParameter(P_Veh_Model_Year,obj[P_Veh_Model_Year],true,true);
				AddParameter(P_Id_Veh_Type,obj[P_Id_Veh_Type],true,false);
				AddParameter(P_Id_Person,obj[P_Id_Person],true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Vehicle_Owners obj,string Id ) 
		{
			try
			{
				AddParameter(P_Veh_Make,obj.Veh_Make,true,false);
				AddParameter(P_Veh_Model,obj.Veh_Model,true,false);
				AddParameter(P_Veh_Model_Year,obj.Veh_Model_Year,true,true);
				AddParameter(P_Id_Veh_Type,obj.Id_Veh_Type,true,false);
				AddParameter(P_Id_Person,obj.Id_Person,true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Veh_Make,obj[P_Veh_Make],true,false);
				AddParameter(P_Veh_Model,obj[P_Veh_Model],true,false);
				AddParameter(P_Veh_Model_Year,obj[P_Veh_Model_Year],true,true);
				AddParameter(P_Id_Veh_Type,obj[P_Id_Veh_Type],true,false);
				AddParameter(P_Id_Person,obj[P_Id_Person],true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Person=@"Select * From Vehicle_Owners 
								 where Id_Person = @Id_Person  order by  Veh_Make";
		public const string QDeleteById_Person=@"Delete From Vehicle_Owners 
								 where Id_Person = @Id_Person";
		virtual	public DataTable GetById_Person(string Id_Person ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Person;
				AddParameter(P_Id_Person,Id_Person,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Person( string Id_Person ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Person;
				AddParameter(P_Id_Person,Id_Person,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public const string QSelectById_Veh_Type=@"Select * From Vehicle_Owners 
								 where Id_Veh_Type = @Id_Veh_Type  order by  Veh_Make";
		public const string QDeleteById_Veh_Type=@"Delete From Vehicle_Owners 
								 where Id_Veh_Type = @Id_Veh_Type";
		virtual	public DataTable GetById_Veh_Type(string Id_Veh_Type ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Veh_Type;
				AddParameter(P_Id_Veh_Type,Id_Veh_Type,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Veh_Type( string Id_Veh_Type ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Veh_Type;
				AddParameter(P_Id_Veh_Type,Id_Veh_Type,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Vehicle_Owners
	{
		public Vehicle_Owners()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Veh_Make=string.Empty;
		public string Veh_Make
		{
			get
			{
				 return _Veh_Make;
			}
			set
			{
				  _Veh_Make = value;
			}
		}
		protected  string _Veh_Model=string.Empty;
		public string Veh_Model
		{
			get
			{
				 return _Veh_Model;
			}
			set
			{
				  _Veh_Model = value;
			}
		}
		protected  string _Veh_Model_Year=string.Empty;
		public string Veh_Model_Year
		{
			get
			{
				 return _Veh_Model_Year;
			}
			set
			{
				  _Veh_Model_Year = value;
			}
		}
		protected  string _Id_Veh_Type=string.Empty;
		public string Id_Veh_Type
		{
			get
			{
				 return _Id_Veh_Type;
			}
			set
			{
				  _Id_Veh_Type = value;
			}
		}
		protected  string _Id_Person=string.Empty;
		public string Id_Person
		{
			get
			{
				 return _Id_Person;
			}
			set
			{
				  _Id_Person = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Vehicle_Owners(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Id))
					Id = obj[QVehicle_Owners.P_Id].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Veh_Make))
					Veh_Make = obj[QVehicle_Owners.P_Veh_Make].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Veh_Model))
					Veh_Model = obj[QVehicle_Owners.P_Veh_Model].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Veh_Model_Year))
					Veh_Model_Year = obj[QVehicle_Owners.P_Veh_Model_Year].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Id_Veh_Type))
					Id_Veh_Type = obj[QVehicle_Owners.P_Id_Veh_Type].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Owners.P_Id_Person))
					Id_Person = obj[QVehicle_Owners.P_Id_Person].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}