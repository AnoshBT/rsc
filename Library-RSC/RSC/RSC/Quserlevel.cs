using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  Quserlevel:Query
	{

		public const string TABLE_NAME = "userlevel";
		public const string P_UserLevelID = "UserLevelID";
		public const string P_UserTitle = "UserTitle";
		public const string P_Descrip = "Descrip";
		public const string P_ParentLevelID = "ParentLevelID";
		public const string P_IsActive = "IsActive";
		public const string P_CreatedBy = "CreatedBy";
		public const string P_CreatedDate = "CreatedDate";
		public const string P_UpdatedBy = "UpdatedBy";
		public const string P_UpdatedDate = "UpdatedDate";
		public const string P_Color = "Color";
		public const string P_AccessPage = "AccessPage";
		public const string P_Viewable = "Viewable";
		public const string SelectColumns=@"UserLevelID,UserTitle,Descrip,ParentLevelID,IsActive,CreatedBy,CreatedDate,UpdatedBy,
						UpdatedDate,Color,AccessPage,Viewable";
		public const string QSelect=@"Select * From userlevel  order by  UserTitle";
		public const string QSelectByUserLevelID=@"Select * From userlevel 
								 where UserLevelID = @UserLevelID  order by  UserTitle";
		public const string QInsert=@"Insert Into  userlevel (UserLevelID,UserTitle,Descrip,ParentLevelID,IsActive,CreatedBy,CreatedDate,UpdatedBy,
						UpdatedDate,Color,AccessPage,Viewable) 
								 values(@UserLevelID,@UserTitle,@Descrip,@ParentLevelID,@IsActive,@CreatedBy,@CreatedDate,@UpdatedBy,
						@UpdatedDate,@Color,@AccessPage,@Viewable)";
		public const string QUpdateByUserLevelID=@"Update userlevel Set UserLevelID=@UserLevelID,UserTitle=@UserTitle,Descrip=@Descrip,ParentLevelID=@ParentLevelID,
						IsActive=@IsActive,CreatedBy=@CreatedBy,CreatedDate=@CreatedDate,UpdatedBy=@UpdatedBy,
						UpdatedDate=@UpdatedDate,Color=@Color,AccessPage=@AccessPage,Viewable=@Viewable 
								 where UserLevelID = @O_UserLevelID";
		public const string QDeleteByUserLevelID=@"Delete From userlevel 
								 where UserLevelID = @UserLevelID";
		public const string QExistsByUserLevelID=@"select UserLevelID  From userlevel 
								 where UserLevelID = @UserLevelID";

		public Quserlevel(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public Quserlevel(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public Quserlevel(IDbConnection Connection):base(Connection)
		{
		}
		public Quserlevel(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public userlevel Get(string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByUserLevelID;
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new userlevel(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(userlevel obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(userlevel obj,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserLevelID;
				SetParameters(obj,UserLevelID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByUserLevelID;
				SetParameters(obj,UserLevelID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByUserLevelID;
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsByUserLevelID;
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(userlevel obj) 
		{
			try
			{
				AddParameter(P_UserLevelID,obj.UserLevelID,true,false);
				AddParameter(P_UserTitle,obj.UserTitle,true,false);
				AddParameter(P_Descrip,obj.Descrip,true,true);
				AddParameter(P_ParentLevelID,obj.ParentLevelID,true,true);
				AddParameter(P_IsActive,obj.IsActive,true,false);
				AddParameter(P_CreatedBy,obj.CreatedBy,true,true);
				AddParameter(P_CreatedDate,obj.CreatedDate,true,false);
				AddParameter(P_UpdatedBy,obj.UpdatedBy,true,true);
				AddParameter(P_UpdatedDate,obj.UpdatedDate,true,true);
				AddParameter(P_Color,obj.Color,true,true);
				AddParameter(P_AccessPage,obj.AccessPage,true,true);
				AddParameter(P_Viewable,obj.Viewable,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,false);
				AddParameter(P_UserTitle,obj[P_UserTitle],true,false);
				AddParameter(P_Descrip,obj[P_Descrip],true,true);
				AddParameter(P_ParentLevelID,obj[P_ParentLevelID],true,true);
				AddParameter(P_IsActive,obj[P_IsActive],true,false);
				AddParameter(P_CreatedBy,obj[P_CreatedBy],true,true);
				AddParameter(P_CreatedDate,obj[P_CreatedDate],true,false);
				AddParameter(P_UpdatedBy,obj[P_UpdatedBy],true,true);
				AddParameter(P_UpdatedDate,obj[P_UpdatedDate],true,true);
				AddParameter(P_Color,obj[P_Color],true,true);
				AddParameter(P_AccessPage,obj[P_AccessPage],true,true);
				AddParameter(P_Viewable,obj[P_Viewable],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(userlevel obj,string UserLevelID ) 
		{
			try
			{
				AddParameter(P_UserLevelID,obj.UserLevelID,true,false);
				AddParameter(P_UserTitle,obj.UserTitle,true,false);
				AddParameter(P_Descrip,obj.Descrip,true,true);
				AddParameter(P_ParentLevelID,obj.ParentLevelID,true,true);
				AddParameter(P_IsActive,obj.IsActive,true,false);
				AddParameter(P_CreatedBy,obj.CreatedBy,true,true);
				AddParameter(P_CreatedDate,obj.CreatedDate,true,false);
				AddParameter(P_UpdatedBy,obj.UpdatedBy,true,true);
				AddParameter(P_UpdatedDate,obj.UpdatedDate,true,true);
				AddParameter(P_Color,obj.Color,true,true);
				AddParameter(P_AccessPage,obj.AccessPage,true,true);
				AddParameter(P_Viewable,obj.Viewable,true,true);
				AddParameter("O_UserLevelID",UserLevelID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string UserLevelID ) 
		{
			try
			{
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,false);
				AddParameter(P_UserTitle,obj[P_UserTitle],true,false);
				AddParameter(P_Descrip,obj[P_Descrip],true,true);
				AddParameter(P_ParentLevelID,obj[P_ParentLevelID],true,true);
				AddParameter(P_IsActive,obj[P_IsActive],true,false);
				AddParameter(P_CreatedBy,obj[P_CreatedBy],true,true);
				AddParameter(P_CreatedDate,obj[P_CreatedDate],true,false);
				AddParameter(P_UpdatedBy,obj[P_UpdatedBy],true,true);
				AddParameter(P_UpdatedDate,obj[P_UpdatedDate],true,true);
				AddParameter(P_Color,obj[P_Color],true,true);
				AddParameter(P_AccessPage,obj[P_AccessPage],true,true);
				AddParameter(P_Viewable,obj[P_Viewable],true,true);
				AddParameter("O_UserLevelID",UserLevelID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectByParentLevelID=@"Select * From userlevel 
								 where ParentLevelID = @ParentLevelID  order by  UserTitle";
	
		virtual	public DataTable GetByParentLevelID(string ParentLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByParentLevelID;
				AddParameter(P_ParentLevelID,ParentLevelID,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByParentLevelID( string ParentLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByParentLevelID;
				AddParameter(P_ParentLevelID,ParentLevelID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		
		public const string QDeleteByParentLevelID=@"Delete From userlevel 
								 where ParentLevelID = @ParentLevelID";
	
		
	}
	public partial class  userlevel
	{
		public userlevel()
		{
		}
		protected  string _UserLevelID=string.Empty;
		public string UserLevelID
		{
			get
			{
				 return _UserLevelID;
			}
			set
			{
				  _UserLevelID = value;
			}
		}
		protected  string _UserTitle=string.Empty;
		public string UserTitle
		{
			get
			{
				 return _UserTitle;
			}
			set
			{
				  _UserTitle = value;
			}
		}
		protected  string _Descrip=string.Empty;
		public string Descrip
		{
			get
			{
				 return _Descrip;
			}
			set
			{
				  _Descrip = value;
			}
		}
		protected  string _ParentLevelID=string.Empty;
		public string ParentLevelID
		{
			get
			{
				 return _ParentLevelID;
			}
			set
			{
				  _ParentLevelID = value;
			}
		}
		protected  string _IsActive=string.Empty;
		public string IsActive
		{
			get
			{
				 return _IsActive;
			}
			set
			{
				  _IsActive = value;
			}
		}
		protected  string _CreatedBy=string.Empty;
		public string CreatedBy
		{
			get
			{
				 return _CreatedBy;
			}
			set
			{
				  _CreatedBy = value;
			}
		}
		protected  DateTime _CreatedDate=new DateTime();
		public DateTime CreatedDate
		{
			get
			{
				 return _CreatedDate;
			}
			set
			{
				  _CreatedDate = value;
			}
		}
		protected  string _UpdatedBy=string.Empty;
		public string UpdatedBy
		{
			get
			{
				 return _UpdatedBy;
			}
			set
			{
				  _UpdatedBy = value;
			}
		}
		protected  DateTime _UpdatedDate=new DateTime();
		public DateTime UpdatedDate
		{
			get
			{
				 return _UpdatedDate;
			}
			set
			{
				  _UpdatedDate = value;
			}
		}
		protected  string _Color=string.Empty;
		public string Color
		{
			get
			{
				 return _Color;
			}
			set
			{
				  _Color = value;
			}
		}
		protected  string _AccessPage=string.Empty;
		public string AccessPage
		{
			get
			{
				 return _AccessPage;
			}
			set
			{
				  _AccessPage = value;
			}
		}
		protected  string _Viewable=string.Empty;
		public string Viewable
		{
			get
			{
				 return _Viewable;
			}
			set
			{
				  _Viewable = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public userlevel(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(Quserlevel.P_UserLevelID))
					UserLevelID = obj[Quserlevel.P_UserLevelID].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_UserTitle))
					UserTitle = obj[Quserlevel.P_UserTitle].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_Descrip))
					Descrip = obj[Quserlevel.P_Descrip].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_ParentLevelID))
					ParentLevelID = obj[Quserlevel.P_ParentLevelID].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_IsActive))
					IsActive = obj[Quserlevel.P_IsActive].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_CreatedBy))
					CreatedBy = obj[Quserlevel.P_CreatedBy].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_CreatedDate) && obj[Quserlevel.P_CreatedDate].ToString()!="" && obj[Quserlevel.P_CreatedDate] is DateTime )
					CreatedDate =(DateTime)obj[Quserlevel.P_CreatedDate];
				if(obj.Table.Columns.Contains(Quserlevel.P_UpdatedBy))
					UpdatedBy = obj[Quserlevel.P_UpdatedBy].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_UpdatedDate) && obj[Quserlevel.P_UpdatedDate].ToString()!="" && obj[Quserlevel.P_UpdatedDate] is DateTime )
					UpdatedDate =(DateTime)obj[Quserlevel.P_UpdatedDate];
				if(obj.Table.Columns.Contains(Quserlevel.P_Color))
					Color = obj[Quserlevel.P_Color].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_AccessPage))
					AccessPage = obj[Quserlevel.P_AccessPage].ToString();
				if(obj.Table.Columns.Contains(Quserlevel.P_Viewable))
					Viewable = obj[Quserlevel.P_Viewable].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}