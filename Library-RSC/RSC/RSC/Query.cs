using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  Query:DBQuery
	{

		public Query(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public Query(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public Query(IDbConnection Connection):base(Connection)
		{
		}
		public Query(IDbCommand Command):base(Command)
		{
		}
		protected  QBlocks _QBlocks=null;
		public QBlocks QBlocks
		{
			get
			{
				 if( _QBlocks==null)
					 _QBlocks=new QBlocks(Command);
				 return _QBlocks;
			}
			set
			{
				  _QBlocks = value;
			}
		}
		protected  QCountries _QCountries=null;
		public QCountries QCountries
		{
			get
			{
				 if( _QCountries==null)
					 _QCountries=new QCountries(Command);
				 return _QCountries;
			}
			set
			{
				  _QCountries = value;
			}
		}
		protected  QDesignations _QDesignations=null;
		public QDesignations QDesignations
		{
			get
			{
				 if( _QDesignations==null)
					 _QDesignations=new QDesignations(Command);
				 return _QDesignations;
			}
			set
			{
				  _QDesignations = value;
			}
		}
		protected  QFloors _QFloors=null;
		public QFloors QFloors
		{
			get
			{
				 if( _QFloors==null)
					 _QFloors=new QFloors(Command);
				 return _QFloors;
			}
			set
			{
				  _QFloors = value;
			}
		}
		protected  QHouse_Portion _QHouse_Portion=null;
		public QHouse_Portion QHouse_Portion
		{
			get
			{
				 if( _QHouse_Portion==null)
					 _QHouse_Portion=new QHouse_Portion(Command);
				 return _QHouse_Portion;
			}
			set
			{
				  _QHouse_Portion = value;
			}
		}
		protected  QHouses _QHouses=null;
		public QHouses QHouses
		{
			get
			{
				 if( _QHouses==null)
					 _QHouses=new QHouses(Command);
				 return _QHouses;
			}
			set
			{
				  _QHouses = value;
			}
		}
		protected  QLog _QLog=null;
		public QLog QLog
		{
			get
			{
				 if( _QLog==null)
					 _QLog=new QLog(Command);
				 return _QLog;
			}
			set
			{
				  _QLog = value;
			}
		}
		protected  QNOC _QNOC=null;
		public QNOC QNOC
		{
			get
			{
				 if( _QNOC==null)
					 _QNOC=new QNOC(Command);
				 return _QNOC;
			}
			set
			{
				  _QNOC = value;
			}
		}
		protected  QPersons _QPersons=null;
		public QPersons QPersons
		{
			get
			{
				 if( _QPersons==null)
					 _QPersons=new QPersons(Command);
				 return _QPersons;
			}
			set
			{
				  _QPersons = value;
			}
		}
		protected  QPlaza _QPlaza=null;
		public QPlaza QPlaza
		{
			get
			{
				 if( _QPlaza==null)
					 _QPlaza=new QPlaza(Command);
				 return _QPlaza;
			}
			set
			{
				  _QPlaza = value;
			}
		}
		protected  QPortions _QPortions=null;
		public QPortions QPortions
		{
			get
			{
				 if( _QPortions==null)
					 _QPortions=new QPortions(Command);
				 return _QPortions;
			}
			set
			{
				  _QPortions = value;
			}
		}
		protected  QProject_City _QProject_City=null;
		public QProject_City QProject_City
		{
			get
			{
				 if( _QProject_City==null)
					 _QProject_City=new QProject_City(Command);
				 return _QProject_City;
			}
			set
			{
				  _QProject_City = value;
			}
		}
		protected  QProject_Phase _QProject_Phase=null;
		public QProject_Phase QProject_Phase
		{
			get
			{
				 if( _QProject_Phase==null)
					 _QProject_Phase=new QProject_Phase(Command);
				 return _QProject_Phase;
			}
			set
			{
				  _QProject_Phase = value;
			}
		}
		protected  QProperty_Servants _QProperty_Servants=null;
		public QProperty_Servants QProperty_Servants
		{
			get
			{
				 if( _QProperty_Servants==null)
					 _QProperty_Servants=new QProperty_Servants(Command);
				 return _QProperty_Servants;
			}
			set
			{
				  _QProperty_Servants = value;
			}
		}
		protected  QShop_Flat_Offices _QShop_Flat_Offices=null;
		public QShop_Flat_Offices QShop_Flat_Offices
		{
			get
			{
				 if( _QShop_Flat_Offices==null)
					 _QShop_Flat_Offices=new QShop_Flat_Offices(Command);
				 return _QShop_Flat_Offices;
			}
			set
			{
				  _QShop_Flat_Offices = value;
			}
		}
		protected  QShop_Office_Employees _QShop_Office_Employees=null;
		public QShop_Office_Employees QShop_Office_Employees
		{
			get
			{
				 if( _QShop_Office_Employees==null)
					 _QShop_Office_Employees=new QShop_Office_Employees(Command);
				 return _QShop_Office_Employees;
			}
			set
			{
				  _QShop_Office_Employees = value;
			}
		}
		protected  QStreets _QStreets=null;
		public QStreets QStreets
		{
			get
			{
				 if( _QStreets==null)
					 _QStreets=new QStreets(Command);
				 return _QStreets;
			}
			set
			{
				  _QStreets = value;
			}
		}
		protected  QTU001 _QTU001=null;
		public QTU001 QTU001
		{
			get
			{
				 if( _QTU001==null)
					 _QTU001=new QTU001(Command);
				 return _QTU001;
			}
			set
			{
				  _QTU001 = value;
			}
		}
		protected  QTUL002 _QTUL002=null;
		public QTUL002 QTUL002
		{
			get
			{
				 if( _QTUL002==null)
					 _QTUL002=new QTUL002(Command);
				 return _QTUL002;
			}
			set
			{
				  _QTUL002 = value;
			}
		}
		protected  QTUS003 _QTUS003=null;
		public QTUS003 QTUS003
		{
			get
			{
				 if( _QTUS003==null)
					 _QTUS003=new QTUS003(Command);
				 return _QTUS003;
			}
			set
			{
				  _QTUS003 = value;
			}
		}
		protected  Quserlevel _Quserlevel=null;
		public Quserlevel Quserlevel
		{
			get
			{
				 if( _Quserlevel==null)
					 _Quserlevel=new Quserlevel(Command);
				 return _Quserlevel;
			}
			set
			{
				  _Quserlevel = value;
			}
		}
		protected  QUserLevelServices _QUserLevelServices=null;
		public QUserLevelServices QUserLevelServices
		{
			get
			{
				 if( _QUserLevelServices==null)
					 _QUserLevelServices=new QUserLevelServices(Command);
				 return _QUserLevelServices;
			}
			set
			{
				  _QUserLevelServices = value;
			}
		}
		protected  QVehicle_Owners _QVehicle_Owners=null;
		public QVehicle_Owners QVehicle_Owners
		{
			get
			{
				 if( _QVehicle_Owners==null)
					 _QVehicle_Owners=new QVehicle_Owners(Command);
				 return _QVehicle_Owners;
			}
			set
			{
				  _QVehicle_Owners = value;
			}
		}
		protected  QVehicle_Types _QVehicle_Types=null;
		public QVehicle_Types QVehicle_Types
		{
			get
			{
				 if( _QVehicle_Types==null)
					 _QVehicle_Types=new QVehicle_Types(Command);
				 return _QVehicle_Types;
			}
			set
			{
				  _QVehicle_Types = value;
			}
		}
	}
}