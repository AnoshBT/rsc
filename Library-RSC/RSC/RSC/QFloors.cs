using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QFloors:Query
	{

		public const string TABLE_NAME = "Floors";
		public const string P_Id = "Id";
		public const string P_FloorNumber = "FloorNumber";
		public const string SelectColumns=@"Id,FloorNumber";
		public const string QSelect=@"Select * From Floors  order by  FloorNumber";
		public const string QSelectById=@"Select * From Floors 
								 where Id = @Id  order by  FloorNumber";
		public const string QInsert=@"Insert Into  Floors (FloorNumber) 
								 values(@FloorNumber)";
		public const string QUpdateById=@"Update Floors Set FloorNumber=@FloorNumber 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Floors 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Floors 
								 where Id = @Id";

		public QFloors(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QFloors(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QFloors(IDbConnection Connection):base(Connection)
		{
		}
		public QFloors(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Floors Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Floors(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Floors obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Floors obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Floors obj) 
		{
			try
			{
				AddParameter(P_FloorNumber,obj.FloorNumber,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_FloorNumber,obj[P_FloorNumber],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Floors obj,string Id ) 
		{
			try
			{
				AddParameter(P_FloorNumber,obj.FloorNumber,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_FloorNumber,obj[P_FloorNumber],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Floors
	{
		public Floors()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _FloorNumber=string.Empty;
		public string FloorNumber
		{
			get
			{
				 return _FloorNumber;
			}
			set
			{
				  _FloorNumber = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Floors(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QFloors.P_Id))
					Id = obj[QFloors.P_Id].ToString();
				if(obj.Table.Columns.Contains(QFloors.P_FloorNumber))
					FloorNumber = obj[QFloors.P_FloorNumber].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}