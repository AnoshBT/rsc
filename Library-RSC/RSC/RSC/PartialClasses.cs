﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using RSC;
namespace RSC
{
    #region Blocks
    public partial class QBlocks : Query
    {
        public Blocks GetByBlockName(string name)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Blocks where Block_Name = @Block_Name";
                AddParameter(P_Block_Name, name.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Blocks(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    #region Project_Phase
    public partial class QProject_Phase : Query
    {
        public Project_Phase GetByProjectPhase(string name, string city)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Project_Phase where Phase_Name = @Phase_Name AND  Id_Project_City=@Id_Project_City ";
                AddParameter(P_Phase_Name, name.Trim(), false, false);
                AddParameter(P_Id_Project_City, city.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Project_Phase(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    #region Project_City
    public partial class QProject_City : Query
    {
        public Project_City GetByProjectCity(string name)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Project_City where Project_Name = @Project_Name";
                AddParameter(P_Project_Name, name.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Project_City(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    #region Portions
    public partial class QPortions: Query
    {
        public Portions GetByPortionName(string name)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Portions where PortionNumber= @PortionNumber";
                AddParameter(P_PortionNumber, name.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Portions(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    #region Floors
    public partial class QFloors : Query
    {
        public Floors GetByFloorName(string name)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Floors where FloorNumber= @FloorNumber";
                AddParameter(P_FloorNumber, name.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Floors(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    #region Countries
    public partial class QCountries: Query
    {
        public Countries GetByCountry(string name)
        {
            try
            {
                ClearParameters();
                string query = @"select * from   Countries where CountryName= @CountryName";
                AddParameter(P_CountryName, name.Trim(), false, false);

                DataTable dt = GetDataTable(query);

                if (dt != null && dt.Rows.Count > 0)
                    return new Countries(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    #endregion
    /// <summary>
    /// //////////////////////////////////////////////////////////////////////////////////////
    /// </summary>

    //#region QWorkCode
    //public partial class QWorkCode : Query
    //{
    //    public WorkCode GetWrokCodeByID(string wrkcdID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   WorkCode where WorkCodeID = @WorkCodeID ";
    //            AddParameter(P_WorkCodeID, wrkcdID.Trim(), false, false);

    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return new WorkCode(dt.Rows[0]);
    //            return null;




    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion

    //#region QTest
    //public partial class QTest : Query
    //{
    //    public DataTable Get(string phaseId, string StretID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   Test where StreetID = @StreetID and BranID = @BranID";
    //            AddParameter(P_BranID, phaseId.Trim(), false, false);
    //            AddParameter(P_StreetID, StretID.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return dt;
    //            return null;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    public int Update(Test obj, string BranID, string StretID, string houseid)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"Update Test Set HouseID='" + houseid + "' where BranID ='" + BranID + "' and StreetID = '" + StretID + "'";

    //            return ExecuteNonQuery(query);

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion

    //#region QCRTWorkcodeAssignment
    //public partial class QCRTWorkcodeAssignment : Query
    //{
    //    public bool FindRecord(string wrkcdID, string crtid)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   CRTWorkcodeAssignment where Workcodeid = @Workcodeid And CRTid=@CRTid";
    //            AddParameter(P_Workcodeid, wrkcdID.Trim(), false, false);
    //            AddParameter(P_CRTid, crtid.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return true;
    //            else
    //                return false;




    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    public CRTWorkcodeAssignment GetCRTID(string wrkcdID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   CRTWorkcodeAssignment where Workcodeid = @Workcodeid";
    //            AddParameter(P_Workcodeid, wrkcdID.Trim(), false, false);

    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return new CRTWorkcodeAssignment(dt.Rows[0]);
    //            return null;




    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }

    //}
    //#endregion
    //#region QComplaint
    //public partial class QComplaint : Query
    //{
    //    public Complaint GetByComplaintID(string Id)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   Complaint where ComplaintID = @ComplaintID";
    //            AddParameter(P_ComplaintID, Id.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return new Complaint(dt.Rows[0]);
    //            return null;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    public string GetMaxValue()
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select MAX(ComplaintID) from Complaint";
    //            //AddParameter(P_AppTitle, AppTitle.Trim(), false, false);
    //            return ExecuteScalarDefault(query, "0").ToString();

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }

    //    }
    //}
    //#endregion

    #region QTHandlerExpertise
    //public partial class QTHandlerExpertise : Query
    //{
    //    public DataTable GetByComplaintID(string Id)
    //    {
    //        try
    //        {
    //            ClearParameters();

    //            AddParameter(, Id.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return dt;
    //            return null;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }}
    #endregion

    //#region QTC004
    //public partial class QTC004 : Query
    //{
    //    virtual public TC004 GetByNic(string nic)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   TC004 where NIC = @NIC";
    //            AddParameter(P_NIC, nic.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TC004(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    virtual public TC004 GetByPhone(string mobile)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   TC004 where Mobile = @Mobile";
    //            AddParameter(P_Mobile, mobile.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TC004(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    virtual public string GetMaxValue()
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select MAX(TCID) from TC004";
    //            //AddParameter(P_AppTitle, AppTitle.Trim(), false, false);
    //            return ExecuteScalarDefault(query, "0").ToString();

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }

    //    }
    //}
    //#endregion

    //#region QTCDetail004
    //public partial class QTCDetail004 : Query
    //{
    //    virtual public TCDetail004 GetByCustomerID(string id)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   TCDetail004 where TCID = @TCID";
    //            AddParameter(P_TCID, id.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);

    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TCDetail004(dt.Rows[0]);
    //            return null;


    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion

    //    #region QTCM0002
    //    public partial class QTCM0002 : Query
    //    {
    //        virtual public TCM0002 GetByComplaintID(string Id)
    //        {
    //            try
    //            {
    //                ClearParameters();
    //                string query = @"select * from   TCM0002 where ComplaintID = @ComplaintID";
    //                AddParameter(P_ComplaintID, Id.Trim(), false, false);
    //                DataTable dt = GetDataTable(query);

    //                if (dt != null && dt.Rows.Count > 0)
    //                    return new TCM0002(dt.Rows[0]);
    //                return null;


    //            }
    //            catch (Exception ex)
    //            {
    //                throw new Exception(ex.Message);
    //            }
    //            finally
    //            {
    //                ClearParameters();
    //            }
    //        }
    //        public int GetCountOpen()
    //        {

    //            try
    //            {
    //                ClearParameters();
    //                string query = @"select count(TCSID) from   TCM0002 where TCSID = '" + 0 + "'";
    //                //AddParameter(P_ComplaintID, Id.Trim(), false, false);
    //                return int.Parse(ExecuteScalarDefault(query, "0").ToString());


    //            }
    //            catch (Exception ex)
    //            {
    //                throw new Exception(ex.Message);
    //            }
    //            finally
    //            {
    //                ClearParameters();
    //            }
    //        }
    //        public int GetCountInprocess()
    //        {

    //            try
    //            {
    //                ClearParameters();
    //                string query = @"select count(TCSID) from   TCM0002 where TCSID = '" + 2 + "'";
    //                //AddParameter(P_ComplaintID, Id.Trim(), false, false);
    //                return int.Parse(ExecuteScalarDefault(query, "0").ToString());


    //            }
    //            catch (Exception ex)
    //            {
    //                throw new Exception(ex.Message);
    //            }
    //            finally
    //            {
    //                ClearParameters();
    //            }
    //        }
    //        public int GetCountRedirect()
    //        {

    //            try
    //            {
    //                ClearParameters();
    //                string query = @"select count(TCSID) from   TCM0002 where TCSID = '" + 4 + "'";
    //                //AddParameter(P_ComplaintID, Id.Trim(), false, false);
    //                return int.Parse(ExecuteScalarDefault(query, "0").ToString());


    //            }
    //            catch (Exception ex)
    //            {
    //                throw new Exception(ex.Message);
    //            }
    //            finally
    //            {
    //                ClearParameters();
    //            }
    //        }
    //        public string GetMaxValue()
    //        {
    //            try
    //            {
    //                ClearParameters();
    //                string query = @"select ComplaintID from TCM0002 
    //                                where ID = (select MAX(ID) from TCM0002)";
    //                //AddParameter(P_AppTitle, AppTitle.Trim(), false, false);
    //                return ExecuteScalarDefault(query, "0").ToString();

    //            }
    //            catch (Exception ex)
    //            {
    //                throw new Exception(ex.Message);
    //            }
    //            finally
    //            {
    //                ClearParameters();
    //            }

    //        }
    //    }
    //    #endregion

    //#region QTC004
    //public partial class QTC004 : Query
    //{
    //    virtual public TC004 GetByMobile(string mob)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from   TC004 where Mobile = @Mobile";
    //            AddParameter(P_Mobile, mob.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TC004(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }

    //}
    //#endregion

    //#region QTest
    //public partial class QTest : Query
    //{
    //    virtual public Test VerifyByStreetID(string StreetID, string BranchID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from Test where StreetID = @StreetID and BranID=@BranID";
    //            AddParameter(P_StreetID, StreetID.Trim(), false, false);
    //            AddParameter(P_BranID, BranchID.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new Test(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //    virtual public Test VerifyByHouseiD(string StreetID, string BranchID, string HouseID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from Test where StreetID = @StreetID and BranID=@BranID and HouseID = @HouseID";
    //            AddParameter(P_StreetID, StreetID.Trim(), false, false);
    //            AddParameter(P_BranID, BranchID.Trim(), false, false);
    //            AddParameter(P_HouseID, HouseID.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new Test(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion

    //Company
    //#region QTCMP0001
    //public partial class QTCMP0001 : Query
    //{
    //    virtual public TCMP0001 VerifyCmpName(string CmpName)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from TCMP0001 where Name = @Name";
    //            AddParameter(P_Name, CmpName.Trim(), false, false); 
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TCMP0001(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion
    //TCT0003

    //#region QTCT0003
    //public partial class QTCT0003 : Query
    //{
    //    virtual public TCT0003 VerifyCTName(string CtName, string CountID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from TCT0003 where CTName = @CTName and CountID=@CountID";
    //            AddParameter(P_CTName, CtName.Trim(), false, false);
    //            AddParameter(P_CountID, CountID.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TCT0003(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion

    //#region QTBran0004
    //public partial class QTBran0004 : Query
    //{
    //    virtual public TBran0004 VerfiyBranchName(string BranchName, string CTID)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from TBran0004 where Name = @Name and CTID=@CTID";
    //            AddParameter(P_Name, BranchName.Trim(), false, false); 
    //            AddParameter(P_CTID, CTID.Trim(), false, false); 
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TBran0004(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion


    #region QTUS003
    public partial class QTUS003 : Query
    {
        public int SignOut(string SessionID, DateTime logoutTime)
        {
            try
            {
                string query;
                query = @"UPDATE TUS003 SET LogoutTime = GETDATE(), Status = '3' WHERE (Status='1') and (SessionID = @SessionID) and logoutTime<@logoutTime ";
                ClearParameters();
                AddParameter("SessionID", SessionID);
                AddParameter("logoutTime", logoutTime);
                return ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public int SignOut(string SessionID, string UserID)
        {
            try
            {
                string query;
                query = @"UPDATE TUS003 SET LogoutTime = GETDATE(), Status = '3' WHERE (Status='1') and (UserID = @UserID and SessionID=@SessionID)  ";
                ClearParameters();
                AddParameter("UserID", UserID);
                AddParameter("SessionID", SessionID);
                return ExecuteNonQuery(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public int CountSignTU001()
        {
            try
            {
                string query;
                query = @"SELECT  COUNT(*) AS Expr1 FROM TUS003 WHERE (Status = '1')";
                ClearParameters();
                return int.Parse(ExecuteScalarDefault(query, "0").ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public bool Exists(string UserID, string Status)
        {
            try
            {
                string query;
                query = @"select ID From TUS003 WHERE( Status=@Status and UserID = @UserID) ";
                ClearParameters();
                AddParameter("UserID", UserID);
                AddParameter("Status", Status);
                return IsRecordExist(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public bool Exists(string UserID, string IPAddress, string Status)
        {
            try
            {
                string query;
                query = @"select ID From TUS003 WHERE(Status=@Status and UserID = @UserID and IPAddress = @IPAddress ) ";
                ClearParameters();
                AddParameter("UserID", UserID);
                AddParameter("IPAddress", IPAddress);
                AddParameter("Status", Status);
                return IsRecordExist(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public bool Exists(string SessionID, string UserID, string IPAddress, string Status)
        {
            try
            {
                string query;
                query = @"select ID From TUS003 WHERE( Status=@Status and  UserID = @UserID and IPAddress = @IPAddress and SessionID=@SessionID) ";
                ClearParameters();
                AddParameter("Status", Status);
                AddParameter("UserID", UserID);
                AddParameter("IPAddress", IPAddress);
                AddParameter("SessionID", SessionID);
                return IsRecordExist(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public TUS003 Get(string UserID, string Status)
        {
            try
            {
                string query;
                query = @"select * From TUS003 WHERE(UserID = @UserID and Status='1') ";
                ClearParameters();
                AddParameter("UserID", UserID);
                AddParameter("Status", Status);
                DataTable dtu = GetDataTable(query);
                if (dtu != null && dtu.Rows.Count > 0)
                    return new TUS003(dtu.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public bool ExistsIPAddress(string IPAddress, string Status)
        {
            try
            {
                string query;
                query = @"select IPAddress From TUS003 WHERE(IPAddress = @IPAddress and Status='1') ";
                ClearParameters();
                AddParameter("IPAddress", IPAddress);
                AddParameter("Status", Status);
                return IsRecordExist(query);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
        public bool SignIn(HttpRequest Request, string UserID)
        {



            try
            {
                //Add in Profile Table
                string query;

                query = @"INSERT INTO TUS003
                      (SessionID,UserID,LogonTime, LogoutTime, IPAddress, Status)VALUES (@SessionID,@UserID,GETDATE(),GETDATE(),@IPAddress,'1') ";
                ClearParameters();
                AddParameter("UserID", UserID);
                AddParameter("IPAddress", Request.UserHostAddress);
                AddParameter("SessionID", Request.Cookies["ASP.NET_SessionID"].Value);
                int count = ExecuteNonQuery(query);
                if (count > 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                ClearParameters();

            }
        }

    }//QTUS003 
    #endregion
    #region QTU001
    public partial class QTU001 : Query
    {
        virtual public TU001 GetByUserID(string UserID)
        {
            try
            {
                ClearParameters();
                string query = @"select *  From TU001 
								 where UserID = @UserID";
                AddParameter(P_UserID, UserID.Trim(), false, false);
                DataTable dt = GetDataTable(query);
                if (dt != null && dt.Rows.Count > 0)
                    return new TU001(dt.Rows[0]);
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                ClearParameters();
            }
        }
    }//QTUS003 
    #endregion
    //#region QTSer0006
    //public partial class QTSer0006 : Query
    //{
    //    virtual public string GetIDByName(string id)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select ServicesID from TSer0006 where Name = @Name";
    //            AddParameter(P_Name, id.Trim(), false, false);
    //            return ExecuteScalarDefault(query, "0").ToString();

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }

    //    virtual public TSer0006 VerifyServiceName(string srvName)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select * from TSer0006 where Name = @Name";
    //            AddParameter(P_Name, srvName.Trim(), false, false);
    //            //AddParameter(P_ServicesID, sID.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return new TSer0006(dt.Rows[0]);
    //            return null;

    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}

    //#endregion
    //#region QTHandlerComents
    //public partial class QTHandlerComents : Query
    //{
    //    virtual public DataTable GetByHanlderID(string id)
    //    {
    //        try
    //        {
    //            ClearParameters();
    //            string query = @"select *  From THandlerComents where HandlerID = @HandlerID";
    //            AddParameter(P_HandlerID, id.Trim(), false, false);
    //            DataTable dt = GetDataTable(query);
    //            if (dt != null && dt.Rows.Count > 0)
    //                return dt;
    //            return null;
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new Exception(ex.Message);
    //        }
    //        finally
    //        {
    //            ClearParameters();
    //        }
    //    }
    //}
    //#endregion
 

     
}


