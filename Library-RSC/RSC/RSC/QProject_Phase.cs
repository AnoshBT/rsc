using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QProject_Phase:Query
	{

		public const string TABLE_NAME = "Project_Phase";
		public const string P_Id = "Id";
		public const string P_Phase_Name = "Phase_Name";
		public const string P_Id_Project_City = "Id_Project_City";
		public const string SelectColumns=@"Id,Phase_Name,Id_Project_City";
		public const string QSelect=@"Select * From Project_Phase  order by  Phase_Name";
		public const string QSelectById=@"Select * From Project_Phase 
								 where Id = @Id  order by  Phase_Name";
		public const string QInsert=@"Insert Into  Project_Phase (Phase_Name,Id_Project_City) 
								 values(@Phase_Name,@Id_Project_City)";
		public const string QUpdateById=@"Update Project_Phase Set Phase_Name=@Phase_Name,Id_Project_City=@Id_Project_City 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Project_Phase 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Project_Phase 
								 where Id = @Id";

		public QProject_Phase(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QProject_Phase(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QProject_Phase(IDbConnection Connection):base(Connection)
		{
		}
		public QProject_Phase(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Project_Phase Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Project_Phase(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Project_Phase obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Project_Phase obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Project_Phase obj) 
		{
			try
			{
				AddParameter(P_Phase_Name,obj.Phase_Name,true,true);
				AddParameter(P_Id_Project_City,obj.Id_Project_City,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Phase_Name,obj[P_Phase_Name],true,true);
				AddParameter(P_Id_Project_City,obj[P_Id_Project_City],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Project_Phase obj,string Id ) 
		{
			try
			{
				AddParameter(P_Phase_Name,obj.Phase_Name,true,true);
				AddParameter(P_Id_Project_City,obj.Id_Project_City,true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Phase_Name,obj[P_Phase_Name],true,true);
				AddParameter(P_Id_Project_City,obj[P_Id_Project_City],true,true);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectById_Project_City=@"Select * From Project_Phase 
								 where Id_Project_City = @Id_Project_City  order by  Phase_Name";
		public const string QDeleteById_Project_City=@"Delete From Project_Phase 
								 where Id_Project_City = @Id_Project_City";
		virtual	public DataTable GetById_Project_City(string Id_Project_City ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById_Project_City;
				AddParameter(P_Id_Project_City,Id_Project_City,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteById_Project_City( string Id_Project_City ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById_Project_City;
				AddParameter(P_Id_Project_City,Id_Project_City,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  Project_Phase
	{
		public Project_Phase()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Phase_Name=string.Empty;
		public string Phase_Name
		{
			get
			{
				 return _Phase_Name;
			}
			set
			{
				  _Phase_Name = value;
			}
		}
		protected  string _Id_Project_City=string.Empty;
		public string Id_Project_City
		{
			get
			{
				 return _Id_Project_City;
			}
			set
			{
				  _Id_Project_City = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Project_Phase(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QProject_Phase.P_Id))
					Id = obj[QProject_Phase.P_Id].ToString();
				if(obj.Table.Columns.Contains(QProject_Phase.P_Phase_Name))
					Phase_Name = obj[QProject_Phase.P_Phase_Name].ToString();
				if(obj.Table.Columns.Contains(QProject_Phase.P_Id_Project_City))
					Id_Project_City = obj[QProject_Phase.P_Id_Project_City].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}