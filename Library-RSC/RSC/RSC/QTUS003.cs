using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QTUS003:Query
	{

		public const string TABLE_NAME = "TUS003";
		public const string P_ID = "ID";
		public const string P_SessionID = "SessionID";
		public const string P_UserID = "UserID";
		public const string P_IPAddress = "IPAddress";
		public const string P_Status = "Status";
		public const string P_LogonTime = "LogonTime";
		public const string P_LogoutTime = "LogoutTime";
		public const string SelectColumns=@"ID,SessionID,UserID,IPAddress,Status,LogonTime,LogoutTime";
		public const string QSelect=@"Select * From TUS003  order by  SessionID";
		public const string QSelectByID=@"Select * From TUS003 
								 where ID = @ID  order by  SessionID";
		public const string QInsert=@"Insert Into  TUS003 (SessionID,UserID,IPAddress,Status,LogonTime,LogoutTime) 
								 values(@SessionID,@UserID,@IPAddress,@Status,@LogonTime,@LogoutTime)";
		public const string QUpdateByID=@"Update TUS003 Set SessionID=@SessionID,UserID=@UserID,IPAddress=@IPAddress,Status=@Status,
						LogonTime=@LogonTime,LogoutTime=@LogoutTime 
								 where ID = @O_ID";
		public const string QDeleteByID=@"Delete From TUS003 
								 where ID = @ID";
		public const string QExistsByID=@"select ID  From TUS003 
								 where ID = @ID";

		public QTUS003(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QTUS003(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QTUS003(IDbConnection Connection):base(Connection)
		{
		}
		public QTUS003(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public TUS003 Get(string ID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByID;
				AddParameter(P_ID,ID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new TUS003(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(TUS003 obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(TUS003 obj,string ID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				SetParameters(obj,ID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string ID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByID;
				SetParameters(obj,ID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string ID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByID;
				AddParameter(P_ID,ID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string ID ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsByID;
				AddParameter(P_ID,ID,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(TUS003 obj) 
		{
			try
			{
				AddParameter(P_SessionID,obj.SessionID,true,true);
				AddParameter(P_UserID,obj.UserID,true,true);
				AddParameter(P_IPAddress,obj.IPAddress,true,true);
				AddParameter(P_Status,obj.Status,true,true);
				AddParameter(P_LogonTime,obj.LogonTime,true,true);
				AddParameter(P_LogoutTime,obj.LogoutTime,true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_SessionID,obj[P_SessionID],true,true);
				AddParameter(P_UserID,obj[P_UserID],true,true);
				AddParameter(P_IPAddress,obj[P_IPAddress],true,true);
				AddParameter(P_Status,obj[P_Status],true,true);
				AddParameter(P_LogonTime,obj[P_LogonTime],true,true);
				AddParameter(P_LogoutTime,obj[P_LogoutTime],true,true);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(TUS003 obj,string ID ) 
		{
			try
			{
				AddParameter(P_SessionID,obj.SessionID,true,true);
				AddParameter(P_UserID,obj.UserID,true,true);
				AddParameter(P_IPAddress,obj.IPAddress,true,true);
				AddParameter(P_Status,obj.Status,true,true);
				AddParameter(P_LogonTime,obj.LogonTime,true,true);
				AddParameter(P_LogoutTime,obj.LogoutTime,true,true);
				AddParameter("O_ID",ID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string ID ) 
		{
			try
			{
				AddParameter(P_SessionID,obj[P_SessionID],true,true);
				AddParameter(P_UserID,obj[P_UserID],true,true);
				AddParameter(P_IPAddress,obj[P_IPAddress],true,true);
				AddParameter(P_Status,obj[P_Status],true,true);
				AddParameter(P_LogonTime,obj[P_LogonTime],true,true);
				AddParameter(P_LogoutTime,obj[P_LogoutTime],true,true);
				AddParameter("O_ID",ID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectByUserID=@"Select * From TUS003 
								 where UserID = @UserID  order by  SessionID";
		public const string QDeleteByUserID=@"Delete From TUS003 
								 where UserID = @UserID";
		virtual	public DataTable GetByUserID(string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByUserID( string UserID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByUserID;
				AddParameter(P_UserID,UserID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  TUS003
	{
		public TUS003()
		{
		}
		protected  string _ID=string.Empty;
		public string ID
		{
			get
			{
				 return _ID;
			}
			set
			{
				  _ID = value;
			}
		}
		protected  string _SessionID=string.Empty;
		public string SessionID
		{
			get
			{
				 return _SessionID;
			}
			set
			{
				  _SessionID = value;
			}
		}
		protected  string _UserID=string.Empty;
		public string UserID
		{
			get
			{
				 return _UserID;
			}
			set
			{
				  _UserID = value;
			}
		}
		protected  string _IPAddress=string.Empty;
		public string IPAddress
		{
			get
			{
				 return _IPAddress;
			}
			set
			{
				  _IPAddress = value;
			}
		}
		protected  string _Status=string.Empty;
		public string Status
		{
			get
			{
				 return _Status;
			}
			set
			{
				  _Status = value;
			}
		}
		protected  DateTime _LogonTime=new DateTime();
		public DateTime LogonTime
		{
			get
			{
				 return _LogonTime;
			}
			set
			{
				  _LogonTime = value;
			}
		}
		protected  DateTime _LogoutTime=new DateTime();
		public DateTime LogoutTime
		{
			get
			{
				 return _LogoutTime;
			}
			set
			{
				  _LogoutTime = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public TUS003(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QTUS003.P_ID))
					ID = obj[QTUS003.P_ID].ToString();
				if(obj.Table.Columns.Contains(QTUS003.P_SessionID))
					SessionID = obj[QTUS003.P_SessionID].ToString();
				if(obj.Table.Columns.Contains(QTUS003.P_UserID))
					UserID = obj[QTUS003.P_UserID].ToString();
				if(obj.Table.Columns.Contains(QTUS003.P_IPAddress))
					IPAddress = obj[QTUS003.P_IPAddress].ToString();
				if(obj.Table.Columns.Contains(QTUS003.P_Status))
					Status = obj[QTUS003.P_Status].ToString();
				if(obj.Table.Columns.Contains(QTUS003.P_LogonTime) && obj[QTUS003.P_LogonTime].ToString()!="" && obj[QTUS003.P_LogonTime] is DateTime )
					LogonTime =(DateTime)obj[QTUS003.P_LogonTime];
				if(obj.Table.Columns.Contains(QTUS003.P_LogoutTime) && obj[QTUS003.P_LogoutTime].ToString()!="" && obj[QTUS003.P_LogoutTime] is DateTime )
					LogoutTime =(DateTime)obj[QTUS003.P_LogoutTime];
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}