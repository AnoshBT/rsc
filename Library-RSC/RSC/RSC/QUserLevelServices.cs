using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QUserLevelServices:Query
	{

		public const string TABLE_NAME = "UserLevelServices";
		public const string P_ServiceID = "ServiceID";
		public const string P_UserLevelID = "UserLevelID";
		public const string P_isActive = "isActive";
		public const string SelectColumns=@"ServiceID,UserLevelID,isActive";
		public const string QSelect=@"Select * From UserLevelServices  order by  isActive";
		public const string QSelectByServiceID_UserLevelID=@"Select * From UserLevelServices 
								 where ServiceID = @ServiceID And UserLevelID = @UserLevelID  order by  isActive";
		public const string QInsert=@"Insert Into  UserLevelServices (ServiceID,UserLevelID,isActive) 
								 values(@ServiceID,@UserLevelID,@isActive)";
		public const string QUpdateByServiceID_UserLevelID=@"Update UserLevelServices Set ServiceID=@ServiceID,UserLevelID=@UserLevelID,isActive=@isActive 
								 where ServiceID = @O_ServiceID And UserLevelID = @O_UserLevelID";
		public const string QDeleteByServiceID_UserLevelID=@"Delete From UserLevelServices 
								 where ServiceID = @ServiceID And UserLevelID = @UserLevelID";
		public const string QExistsByServiceID_UserLevelID=@"select ServiceID ,UserLevelID  From UserLevelServices 
								 where ServiceID = @ServiceID And UserLevelID = @UserLevelID";

		public QUserLevelServices(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QUserLevelServices(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QUserLevelServices(IDbConnection Connection):base(Connection)
		{
		}
		public QUserLevelServices(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public UserLevelServices Get(string ServiceID ,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByServiceID_UserLevelID;
				AddParameter(P_ServiceID,ServiceID,false,false);
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new UserLevelServices(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(UserLevelServices obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(UserLevelServices obj,string ServiceID ,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByServiceID_UserLevelID;
				SetParameters(obj,ServiceID ,UserLevelID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string ServiceID ,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateByServiceID_UserLevelID;
				SetParameters(obj,ServiceID ,UserLevelID );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string ServiceID ,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByServiceID_UserLevelID;
				AddParameter(P_ServiceID,ServiceID,false,false);
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string ServiceID ,string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsByServiceID_UserLevelID;
				AddParameter(P_ServiceID,ServiceID,false,false);
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(UserLevelServices obj) 
		{
			try
			{
				AddParameter(P_ServiceID,obj.ServiceID,true,false);
				AddParameter(P_UserLevelID,obj.UserLevelID,true,false);
				AddParameter(P_isActive,obj.isActive,true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_ServiceID,obj[P_ServiceID],true,false);
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,false);
				AddParameter(P_isActive,obj[P_isActive],true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(UserLevelServices obj,string ServiceID ,string UserLevelID ) 
		{
			try
			{
				AddParameter(P_ServiceID,obj.ServiceID,true,false);
				AddParameter(P_UserLevelID,obj.UserLevelID,true,false);
				AddParameter(P_isActive,obj.isActive,true,false);
				AddParameter("O_ServiceID",ServiceID,false,false);
				AddParameter("O_UserLevelID",UserLevelID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string ServiceID ,string UserLevelID ) 
		{
			try
			{
				AddParameter(P_ServiceID,obj[P_ServiceID],true,false);
				AddParameter(P_UserLevelID,obj[P_UserLevelID],true,false);
				AddParameter(P_isActive,obj[P_isActive],true,false);
				AddParameter("O_ServiceID",ServiceID,false,false);
				AddParameter("O_UserLevelID",UserLevelID,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		public const string QSelectByUserLevelID=@"Select * From UserLevelServices 
								 where UserLevelID = @UserLevelID  order by  isActive";
		public const string QDeleteByUserLevelID=@"Delete From UserLevelServices 
								 where UserLevelID = @UserLevelID";
		virtual	public DataTable GetByUserLevelID(string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectByUserLevelID;
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int DeleteByUserLevelID( string UserLevelID ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteByUserLevelID;
				AddParameter(P_UserLevelID,UserLevelID,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
	}
	public partial class  UserLevelServices
	{
		public UserLevelServices()
		{
		}
		protected  string _ServiceID=string.Empty;
		public string ServiceID
		{
			get
			{
				 return _ServiceID;
			}
			set
			{
				  _ServiceID = value;
			}
		}
		protected  string _UserLevelID=string.Empty;
		public string UserLevelID
		{
			get
			{
				 return _UserLevelID;
			}
			set
			{
				  _UserLevelID = value;
			}
		}
		protected  string _isActive=string.Empty;
		public string isActive
		{
			get
			{
				 return _isActive;
			}
			set
			{
				  _isActive = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public UserLevelServices(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QUserLevelServices.P_ServiceID))
					ServiceID = obj[QUserLevelServices.P_ServiceID].ToString();
				if(obj.Table.Columns.Contains(QUserLevelServices.P_UserLevelID))
					UserLevelID = obj[QUserLevelServices.P_UserLevelID].ToString();
				if(obj.Table.Columns.Contains(QUserLevelServices.P_isActive))
					isActive = obj[QUserLevelServices.P_isActive].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}