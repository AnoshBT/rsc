using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
namespace RSC
{
	public partial class  QVehicle_Types:Query
	{

		public const string TABLE_NAME = "Vehicle_Types";
		public const string P_Id = "Id";
		public const string P_Veh_Type = "Veh_Type";
		public const string SelectColumns=@"Id,Veh_Type";
		public const string QSelect=@"Select * From Vehicle_Types  order by  Veh_Type";
		public const string QSelectById=@"Select * From Vehicle_Types 
								 where Id = @Id  order by  Veh_Type";
		public const string QInsert=@"Insert Into  Vehicle_Types (Veh_Type) 
								 values(@Veh_Type)";
		public const string QUpdateById=@"Update Vehicle_Types Set Veh_Type=@Veh_Type 
								 where Id = @O_Id";
		public const string QDeleteById=@"Delete From Vehicle_Types 
								 where Id = @Id";
		public const string QExistsById=@"select Id  From Vehicle_Types 
								 where Id = @Id";

		public QVehicle_Types(string ConnectionString):base(ConnectionString,DBProvider.SQLCLIENT)
		{
		}

		public QVehicle_Types(string ConnectionString,string dbProvider):base(ConnectionString,dbProvider)
		{
		}
		public QVehicle_Types(IDbConnection Connection):base(Connection)
		{
		}
		public QVehicle_Types(IDbCommand Command):base(Command)
		{
		}
		virtual	public DataTable Get() 
		{
			try
			{
				ClearParameters();
				string query=QSelect; 
				return GetDataTable(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public Vehicle_Types Get(string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QSelectById;
				AddParameter(P_Id,Id,false,false);
				DataTable dt=GetDataTable(query);
				if(dt!=null && dt.Rows.Count>0) 
					return new Vehicle_Types(dt.Rows[0]); 
				return null;
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Add(Vehicle_Types obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		public int Add(DataRow obj) 
		{
			try
			{
				ClearParameters();
				string query=QInsert;
				SetParameters(obj);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(Vehicle_Types obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Update(DataRow obj,string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QUpdateById;
				SetParameters(obj,Id );
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public int Delete( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QDeleteById;
				AddParameter(P_Id,Id,false,false);
				return ExecuteNonQuery(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public bool Exists( string Id ) 
		{
			try
			{
				ClearParameters();
				string query=QExistsById;
				AddParameter(P_Id,Id,false,false);
				return IsRecordExist(query);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
				ClearParameters();
			}
		}
		virtual	public void SetParameters(Vehicle_Types obj) 
		{
			try
			{
				AddParameter(P_Veh_Type,obj.Veh_Type,true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj) 
		{
			try
			{
				AddParameter(P_Veh_Type,obj[P_Veh_Type],true,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(Vehicle_Types obj,string Id ) 
		{
			try
			{
				AddParameter(P_Veh_Type,obj.Veh_Type,true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
		virtual	public void SetParameters(DataRow obj,string Id ) 
		{
			try
			{
				AddParameter(P_Veh_Type,obj[P_Veh_Type],true,false);
				AddParameter("O_Id",Id,false,false);
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
			finally
			{
			}
		}
	}
	public partial class  Vehicle_Types
	{
		public Vehicle_Types()
		{
		}
		protected  string _Id=string.Empty;
		public string Id
		{
			get
			{
				 return _Id;
			}
			set
			{
				  _Id = value;
			}
		}
		protected  string _Veh_Type=string.Empty;
		public string Veh_Type
		{
			get
			{
				 return _Veh_Type;
			}
			set
			{
				  _Veh_Type = value;
			}
		}
		protected DataRow  _DataRow = null ;
		public DataRow DataRow
		{
			get
			{
				 return _DataRow;
			}
			set
			{
				  _DataRow = value;
			}
		}
		public Vehicle_Types(DataRow obj)
		{
			try
			{
				if(obj==null)
					return;
				DataRow = obj;
				if(obj.Table.Columns.Contains(QVehicle_Types.P_Id))
					Id = obj[QVehicle_Types.P_Id].ToString();
				if(obj.Table.Columns.Contains(QVehicle_Types.P_Veh_Type))
					Veh_Type = obj[QVehicle_Types.P_Veh_Type].ToString();
			}
			catch(Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}